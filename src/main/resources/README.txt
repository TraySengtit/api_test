Date : 27/08/2021

Welcome to Sahakka Admin Dashboard


We have divided 4 parts ( Dashboard, Admin, Business Owner, Freelancer, Category)
1. Dashboard
- Show total of Business Owner
- Show total of Freelancer
- Show total user
- Graph (not yet )
2. Admin
- Show total of Admin
- Show all admin’s information
- Can insert, block, search, update and view admin’s information
3.Freelancer
 We have divided 3 part
  + Freelancer post
   - Show total of freelancer post
   - Show all freelancer's post information
   - Can view, search and block freelancer post
   + Freelancer information
   - Show total of freelancer
   - Show all freelancer information
   - Can view, search and block freelancer
    + Reported post
   - Show total of Freelancer's reported post
   - Show all freelancer's reported post
   - Can view, search and block freelancer's reported post
3. Business owner
There are three parts.
+ Bussiness ower post
  - Show total of business owner post
  - Show all business owner post
  - Can view, search and block business owner post
 + Business owner information
  - Show total of business owner
  - Show all business owner information
  - Can view, search and block business owner information
  + Business owner reported post
   - Show total of owner's reported post (not yet )
   - Show all owner's reported post (not yet )
   - Can view, search and block owner's reported post(not yet )
4. Category
  We have divided 2 parts.
 + Category
   - Show total of categories
   - Show all categories information
   - Can view, search update and delete.
+ Sugesstion Category
   - Show total of categoties sugesstion
   - Show all categories sugesstion information
  - Can view, search, update, delete and add category















I.ADMIN DASHBOARD NOTE
----------------- WHAT WE HAVE DONE BUT NOT CONSIDERED AS COMPLETE-------------
1. For UI
 a.dashboard page (Completed)
 b.Freelancer page (80%)
 b.Business owner page (80%)
 C.Admin page (85%)
 d.Category page (80%)

 2. For Controller
 a.Business owner controller
 +All post
 - Get all (100%)
 - Delete (50%)

------------------------------------------------
II. API NOTES
---------------WHAT WE HAVE DONE AND IT'S OKAY------------

1. Account Rest Controller
    a.ResetPassword
    b.Update Business Owner Account
    c.Update Freelancer Account.

2.Authentication Rest Controller
    a.Login
    b.SignUp For Business Owners
    c.SignUp For Freelancers

3. Service Post Controller
    a.Create Service Post
    b.Update Service Post
    c.Delete Service Post
    d.Review Service Post
    e.Reply Service Post Review

4. Post Rest Controller
    a.Create Project Post
    b.Update Project Post
    c.Delete Project Post
    d.Comment On Project Post


----------------- WHAT WE HAVE DONE BUT NOT CONSIDERED AS COMPLETE-------------
1.Category Rest Controller
    a.Create
    b.Update
    c.Delete
    d.FindById
    e.Get All

2.Post Rest Controller
    a.Find By Id
    b.Get All Post

3.Service Post Rest Controller
    a.Find By Id
    b.Get All.






----------------- A LITTLE NOTE ABOUT DATABASE --------------------

A.Initialize values on shk_role :
    1.FREELANCER
    2.BUSINESS_OWNER

B. Initialize values on shk_categories :
   ex. 1. Computer Programming
       2. Graphic Design
       3. Web Development etc

---------------------------------
THANK YOU SO MUCH TEACHERS. WE ARE GIVING IT ALL OUR BEST.  HAVE A WONDERFUL DAY.