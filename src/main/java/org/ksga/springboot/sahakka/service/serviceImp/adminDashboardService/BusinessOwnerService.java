package org.ksga.springboot.sahakka.service.serviceImp.adminDashboardService;
import org.apache.ibatis.annotations.Param;
import org.ksga.springboot.sahakka.model.dashboardModel.OwnerPost;
import org.ksga.springboot.sahakka.model.post.AllReportedOfPost;
import org.ksga.springboot.sahakka.model.post.AllReportedProjectPost;
import org.ksga.springboot.sahakka.model.post.Post;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.utilities.Paging;

import java.util.List;

public interface BusinessOwnerService {
   List<AllReportedProjectPost> findAllReportedProjectPosts(@Param("search_query") String search_query, @Param("paging") Paging paging);
   long countReportedPost();
   List<AllReportedOfPost> allReportOfProjectPosts(int post_id);
   Boolean deleteReport(int id);
   //All posts owner
   public List<OwnerPost> findAllPosts(String search_query, Paging paging);

   public Boolean deletePost(int post_id);

   public OwnerPost findById(int post_id);

   public Long countPost();


    //All business Owner
   public List<User> findAllBusinessOwner(String search_query, Paging paging);

   public OwnerPost findBusinessownerByid(int user_id);

   public Boolean deleteBusinessOwner(int b_id);
}
