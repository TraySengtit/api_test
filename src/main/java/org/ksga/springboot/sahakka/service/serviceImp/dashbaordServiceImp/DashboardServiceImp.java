package org.ksga.springboot.sahakka.service.serviceImp.dashbaordServiceImp;

import org.ksga.springboot.sahakka.repository.admin_dashboard_repo.DashboardRepo;
import org.ksga.springboot.sahakka.service.serviceImp.adminDashboardService.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DashboardServiceImp implements DashboardService {

    private DashboardRepo dashboardRepo;

    public DashboardServiceImp(DashboardRepo dashboardRepo) {
        this.dashboardRepo = dashboardRepo;
    }

    @Override
    public Long CountFreelancer() {
        return dashboardRepo.CountFreelancer();
    }

    @Override
    public Long CountOwner() {
        return dashboardRepo.CountOwner();
    }
}
