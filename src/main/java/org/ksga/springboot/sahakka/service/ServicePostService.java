package org.ksga.springboot.sahakka.service;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.ksga.springboot.sahakka.dto.servicepostdto.AllServicePostDto;
import org.ksga.springboot.sahakka.dto.servicepostdto.ServicePostDto;
import org.ksga.springboot.sahakka.model.postService.*;
import org.ksga.springboot.sahakka.utilities.Paging;

import java.util.List;

public interface ServicePostService {
  /*  List<ServicePost> getAllReportedServicePost();*/
    String maxServicePostId();
    Boolean createServicePostReport(String email, int id,int service_post_id, String reason,String created_date);
    //Filtering service post
    int countServicePostByUsernameAndCategory(String username,String category_type);
    List<AllServicePostDto> getServicePostByUsernameAndCategory(String username,String category_type, int limit , int offset);

    int countServicePostByCategory(String category_type);
    List<AllServicePostDto> getServicePostByCategory(String category_type , int limit, int offset);
    List <AllServicePostDto> getServicePostByUsername(String username,int limit, int offset);
    int countServicePostOfUsername(String username);




    List<AllServicePostDto> allOwnerServicePost(String email, int limit, int offset,String post_status);
    int countAllOwnerServicePost(String email, String post_status);





    List<AllServicePostDto> allNormalServicePost(int limit, int offset);

    int allAvailableServicePost();


    List<Review> allServicePostReview(int service_post_id);
    ServiceCard findServiceCardById(int id);
    List<String> categoryOfServicePostById(int id);



   boolean createServicePost(int id,  String email,String title , String post_image, String post_status, String post_description);

   List<Category> allCategories();
   boolean addCategoriesToServicePost(int service_post_id,int category_id);
   List<ServicePostCategory> getCategoryFromServicePost(int service_post_id);
   boolean isServicePostCategoryDeleted(String email,int id);
   boolean isCategoryServicePostDeleted(int service_post_id);
   List<Integer> allServicePost();
  // public List<ServicePostDto> allServicePost(Paging paging);
  // public ServicePost findOne(@Param("id") int id);
   int allServicePostOfUser(String email);
   boolean deleteReview(int service_post_id);
   boolean deleteAllReviewReply(int service_post_id, int parent_review_id);
   String  getReviewId(int service_post_id);
    int contReview(int service_post_id);
   boolean updateServicePost(int id, String title, String post_image, String post_status, String post_description);
  // No need to use this one anymore.
   boolean updateCategoryServicePost(int category_id,int id);
   boolean deleteServicePostCategory (int service_post_id);

   public boolean delete(@Param("id") int id);
   String roleOfUser(String email);
   boolean addReviewToServicePost(int id, String email,int service_post_id,String review_text,int number_of_stars ,String reviewDate);
   boolean replyReviewToServicePost(int service_post_id,String email,String review_text,String review_date, int parent_review_id);
   String verifyServicePost(String email,int id);
    String maxReviewId();

   public boolean update(int id, int user_id, String title, String post_image, String post_description, String post_status);
}
