package org.ksga.springboot.sahakka.service;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.ksga.springboot.sahakka.model.BusinessOwner;
import org.ksga.springboot.sahakka.model.Freelancer;

import org.ksga.springboot.sahakka.model.postService.UserServicePost;
import org.ksga.springboot.sahakka.model.user.AuthUser;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.model.user.UserRole;
import org.ksga.springboot.sahakka.utilities.Paging;

import java.util.List;

public interface UserService {

    String maxUserId();
    String maxUserRoleId();
    // Admin related
    Boolean updateAdminPassword(String password , int user_id);
    Boolean deleteAdmin(int user_id);
    String adminEmail(int user_id) ;
    long countSearchedAdmin(String username);
    long countAllAdmin();
    List<User> allAdmins( String search_query,  Paging paging);


    UserServicePost findFreelancerById(int id);
    int verifyTokenOfUser(String email,String reset_password_token);
    Boolean updateForgottenPassword(String email,String passwords,String reset_password_token);
    Boolean setResetTokenPassword(String reset_password_token,String email);


    User findUserByEmail(String email );
    boolean deleteUser(String email);
    String verifyEmail(String email);


    boolean deleteSocialLinkOfUser(String email);

    User findUserById (int user_id) ;
    String providerUser(String email);
    String maxUserSocialMedia();
    boolean addSkills(String email,int id , String social_link);


    AuthUser findUserByUsername(String email);
    // Find UserByEmail
    List<User> allUsers();
    // What information should be response if the it's successfully login
    User login(String email) ;

    String existedEmail(String email);
    boolean addNewUser(int user_id, String username, String email,String password,String provider_type,boolean is_active,String profile_image, String email_address,String telephone, String created_date , String bio,String gender,String address,String nationality);
    boolean updateUser(int user_id,String username, String profile_image,String email_address,String telephone,String bio,String gender ,String address ,String nationality);

    boolean addNewUserAsFreelancer(int freelancer_id,String educational_background,String work_experience,String skills,String languages,String achievement,String national_id);
    boolean addNewUserAsBusinessOwner(int bo_id,String company_name,String company_contact ,String company_location , String business_description,String company_logo);


    boolean addRoleToUser(int id , int user_id, int role_id);

    List<UserRole> allUserRoles();
    Freelancer freelancerUser(int id);
    BusinessOwner businessUser(int id);

    boolean updateFreelancer(int freelancer_id, String educational_background,String work_experience , String skills , String languages ,String achievement ,String national_id);

    int findIdByUserEmail(String email);
    boolean updateBusinessOwner(int bo_id,String company_name,String company_contact, String company_location, String company_logo,String business_description);

    String previousPassword(String email);

    boolean updatePassword(String passwords, String email);

}
