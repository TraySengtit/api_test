package org.ksga.springboot.sahakka.service.serviceImp;

import org.ksga.springboot.sahakka.dto.postDto.AllProjectPostDto;
import org.ksga.springboot.sahakka.dto.postDto.PostDto;
import org.ksga.springboot.sahakka.model.category.Category;
import org.ksga.springboot.sahakka.model.post.Comment;
import org.ksga.springboot.sahakka.model.post.Post;
import org.ksga.springboot.sahakka.model.post.PostDeadline;
import org.ksga.springboot.sahakka.model.post.ProjectPost;
import org.ksga.springboot.sahakka.repository.PostRepository;
import org.ksga.springboot.sahakka.service.PostService;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PostImp implements PostService {
    private PostRepository postRepository;
    ModelMapper mapper = new ModelMapper();
    @Autowired
    public PostImp(PostRepository postRepository) {
        this.postRepository = postRepository;
    }


    @Override
    public int countLikeOfServicePost(int post_id) {
        return postRepository.countLikeOfServicePost(post_id);
    }

    @Override
    public List<PostDeadline> allProjectPostDeadline() {
        return postRepository.allProjectPostDeadline();
    }

    @Override
    public Boolean updatePostStatus(int post_id) {
        return postRepository.updatePostStatus(post_id);
    }

    @Override
    public String maxReportId() {
        return postRepository.maxReportId();
    }

    @Override
    public Boolean createReport(int id, int post_id, String email, String reason,String created_date) {
        return postRepository.createReport(id,post_id,email,reason,created_date);
    }

    @Override
    public List<AllProjectPostDto> findProjectPostByUsernameAndCategory(String username, String category_type, int limit, int offset) {
        return postRepository.findProjectPostByUsernameAndCategory(username,category_type,limit,offset);
    }

    @Override
    public int countPostByUsernameAndCategory(String username, String category_type) {
        return postRepository.countPostByUsernameAndCategory(username,category_type);
    }

    @Override
    public int countProjectPostByCategory(String category_type) {
        return postRepository.countProjectPostByCategory(category_type);
    }

    @Override
    public List<AllProjectPostDto> findProjectByCategory(String category_type, int limit, int offset) {
        return postRepository.findProjectByCategory(category_type,limit,offset);
    }

    @Override
    public int countProjectsByUsername(String username) {
        return postRepository.countProjectsByUsername(username);
    }

    @Override
    public List<AllProjectPostDto> findProjectPostByUsername(String username, int limit, int offset) {
        return postRepository.findProjectPostByUsername(username,limit,offset);
    }

    @Override
    public List<AllProjectPostDto> findOwnerProjectPosts(String email, int limit, int offset,String post_status) {
        return postRepository.findOwnerProjectPosts(email,limit,offset,post_status);
    }

    @Override
    public int countOwnerProjectPosts(String email,String post_status) {
        return postRepository.countOwnerProjectPosts(email,post_status);
    }

    @Override
    public int countNormalProjectPosts() {
        return postRepository.countNormalProjectPosts();
    }

    @Override
    public List<AllProjectPostDto> findAllNormalProjectPosts(int limit , int offset) {
        return postRepository.findAllNormalProjectPosts(limit,offset);
    }

    @Override
    public int countOwnerNormalProjectPosts(String email) {
        return postRepository.countOwnerNormalProjectPosts(email);
    }

    @Override
    public List<ProjectPost> allNormalProjectPosts(int user_id) {
        return postRepository.allNormalProjectPosts(user_id);
    }

    @Override
    public List<ProjectPost> allProjectPosts(int user_id) {
        return postRepository.allProjectPosts(user_id);
    }

    @Override
    public List<Category> categoryOfPost(int post_id) {
        return postRepository.categoryOfPost(post_id);
    }
    @Override
    public List<String> linksUser(int user_id) {
        return postRepository.linksUser(user_id);
    }
    @Override
    public List<Comment> allCommentOfPost(int post_id) {
        return postRepository.allCommentOfPost(post_id);
    }

    @Override
    public Post findPostById(int post_id) {
        return postRepository.findPostById(post_id);
    }

    @Override
    public boolean createProjectPost(String email, int post_id, String title, String description, String post_image, String post_deadline, String created_date, String post_status) {
        return postRepository.createProjectPost(email,post_id,title,description,post_image,post_deadline,created_date,post_status);
    }

    @Override
    public String maxPostId() {
        return postRepository.maxPostId();
    }

    @Override
    public boolean addCategoryToPost(int post_id, int category_id) {
        return postRepository.addCategoryToPost(post_id,category_id);
    }

    @Override
    public boolean updateProject(int post_id, String title, String description, String email, String post_image, String post_deadline, String post_status) {
        return postRepository.updateProject(post_id,title,description,email,post_image,post_deadline,post_status);
    }

    @Override
    public boolean updateCategoryPost(int category_id, int post_id) {
        return postRepository.updateCategoryPost(category_id,post_id);
    }

    @Override
    public String likeOfPost(int post_id) {
        return postRepository.likeOfPost(post_id);
    }

    @Override
    public String verifyPostOwnership(String email, int post_id) {
        return postRepository.verifyPostOwnership(email,post_id);
    }

    @Override
    public boolean deletePostCategory(int post_id) {
        return postRepository.deletePostCategory(post_id);
    }

    @Override
    public int countUserPost(String email) {
        return postRepository.countUserPost(email);
    }

    @Override
    public boolean comment(int comment_id, String email, int post_id, String comment_text,String created_date) {
        return postRepository.comment(comment_id,email,post_id,comment_text,created_date);
    }

    @Override
    public String maxCommentId() {
        return postRepository.maxCommentId();
    }
    @Override
    public boolean replyComment(int comment_id, String email, int post_id, int parent_comment_id, String comment_text) {
        return postRepository.replyComment(comment_id,email,post_id,parent_comment_id,comment_text);
    }
    @Override
    public int countComment(int post_id) {
        return postRepository.countComment(post_id);
    }
    @Override
    public int verifyPostId(int post_id) {
        return postRepository.verifyPostId(post_id);
    }
    @Override
    public String verifyCommentId(int comment_id) {
        return postRepository.verifyCommentId(comment_id);
    }

    @Override
    public String maxLikeId() {
        return postRepository.maxLikeId();
    }

    @Override
    public boolean likePost(String email, int id, int post_id) {
        return postRepository.likePost(email,id,post_id);
    }

    @Override
    public boolean unlikePost(String email, int post_id) {
        return postRepository.unlikePost(email,post_id);
    }

    @Override
    public String reactId(String email, int post_id) {
        return postRepository.reactId(email,post_id);
    }

    @Override
    public int countLikes(int post_id) {
        return postRepository.countLikes(post_id);
    }

    @Override
    public boolean updatePostLike(int likes, int post_id) {
        return postRepository.updatePostLike(likes,post_id);
    }

    @Override
    public boolean deletePostLike(int post_id) {
        return postRepository.deletePostLike(post_id);
    }

    @Override
    public boolean deleteReplies(int post_id) {
        return postRepository.deleteReplies(post_id);
    }

    @Override
    public boolean deleteComments(int post_id) {
        return postRepository.deleteComments(post_id);
    }

    @Override
    public boolean deletePost(int post_id) {
        return postRepository.deletePost(post_id);
    }






    @Override
    public List<PostDto> allPost(Paging paging) {
    paging.setTotalCount(postRepository.countAllRecord());
    List<PostDto> tdoList = new ArrayList<>();
    for(Post s: postRepository.findAll(paging)){
        PostDto dto = mapper.map(s, PostDto.class);
        tdoList.add(dto);
    }
        return tdoList;
    }




}
