package org.ksga.springboot.sahakka.service.serviceImp.dashbaordServiceImp;

import org.ksga.springboot.sahakka.model.dashboardModel.FreelancerPost;
import org.ksga.springboot.sahakka.model.dashboardModel.ReportedServicePost;
import org.ksga.springboot.sahakka.model.postService.AllReportedServicePost;
import org.ksga.springboot.sahakka.model.postService.ServicePost;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.repository.admin_dashboard_repo.FreelancerRepo;
import org.ksga.springboot.sahakka.service.serviceImp.adminDashboardService.FreelancerService;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FreelancerServiceImp implements FreelancerService {

    private FreelancerRepo freelancerRepo;

    @Autowired
    public FreelancerServiceImp(FreelancerRepo freelancerRepo) {
        this.freelancerRepo = freelancerRepo;
    }

    @Override
    public Boolean deleteServiceReport(int id) {
        return freelancerRepo.deleteServiceReport(id);
    }

    @Override
    public List<AllReportedServicePost> allReportOfServicePost(int service_post_id) {
        return freelancerRepo.allReportOfServicePost(service_post_id);
    }
    @Override
    public long countReportedCard() {
        return freelancerRepo.countReportedCard();
    }

    @Override
    public List<ReportedServicePost> getAllReportedServicePost(String search_query, Paging paging) {
        return freelancerRepo.getAllReportedServicePost(search_query,paging);
    }

    @Override
    public List<FreelancerPost> findFreelancerFilterPost(String search_query, Paging paging) {
        paging.setTotalCount(freelancerRepo.countFilterFreelancerPost(search_query));
        return freelancerRepo.findAllFreeLancerPost(search_query, paging);
    }

    @Override
    public FreelancerPost findFreelancerByID(int id) {
        return freelancerRepo.findById(id);
    }

    @Override
    public List<User> findAllFreelancer(String search_query, Paging paging) {
        paging.setTotalCount(freelancerRepo.countAllFreelancer(search_query));
        return freelancerRepo.findAllFreelancerFilter(search_query, paging);
    }

    @Override
    public FreelancerPost findFreelancerById(int user_id) {
        return freelancerRepo.findFreelancerById(user_id);
    }

    @Override
    public Long countPost() {
        return freelancerRepo.countPost();
    }

    @Override
    public Boolean deleteFreelancerById(int f_id) {
        return freelancerRepo.deleteFreelancerById(f_id);
    }
}
