package org.ksga.springboot.sahakka.service;

import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface FileStorageService {
    String saveFile(MultipartFile file);

}
