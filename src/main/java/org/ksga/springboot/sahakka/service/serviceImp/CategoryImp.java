package org.ksga.springboot.sahakka.service.serviceImp;


import org.ksga.springboot.sahakka.model.category.Category;

import org.ksga.springboot.sahakka.repository.CategoryRepository;
import org.ksga.springboot.sahakka.service.CategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryImp implements CategoryService {
    private final CategoryRepository categoryRepository;
    @Autowired
    public CategoryImp(CategoryRepository repository) {

        this.categoryRepository = repository;
    }

    @Override
    public List<String> getAllCategories(Boolean suggested_state, int limit, int offset) {
        return categoryRepository.getAllCategories(suggested_state,limit,offset);
    }

    @Override
    public int countAllCategories(Boolean suggested_state) {
        return categoryRepository.countAllCategories(suggested_state);
    }

    @Override
    public int countCategoriesByType(String category_type) {
        return categoryRepository.countCategoriesByType(category_type);
    }

    @Override
    public boolean suggestedCategoryUpdate(int category_id) {
        return categoryRepository.suggestedCategoryUpdate(category_id);
    }

    @Override
    public boolean suggestedCategoryRemove(int category_id) {
        return categoryRepository.suggestedCategoryRemove(category_id);
    }

    @Override
    public String getSuggestedCategoryById(int category_id) {
        return categoryRepository.getSuggestedCategoryById(category_id);
    }

    @Override
    public String maxCategory() {
        return categoryRepository.maxCategory();
    }

    @Override
    public boolean addNewCategory(int category_id, String category_type) {
        return categoryRepository.addNewCategory(category_id,category_type);
    }

    @Override
    public Category findById(int category_id) {
        return categoryRepository.findById(category_id);
    }

    @Override
    public int countCategories() {
        return categoryRepository.countCategories();
    }

  /*  @Override
    public List<CategoryDto> allCategories(Paging paging) {
        paging.setTotalCount(categoryRepository.countAllRecord());
        List<CategoryDto> tdoList = new ArrayList<>();
        for(Category c: categoryRepository.findAll(paging)){
            CategoryDto dto = mapper.map(c, CategoryDto.class);
            tdoList.add(dto);
        }
        return tdoList;
    }*/

    @Override
    public boolean delete(int category_id) {
        return categoryRepository.delete(category_id);
    }

    @Override
    public boolean update(int category_id, String category_type) {
        return categoryRepository.update(category_id,category_type);
    }



}
