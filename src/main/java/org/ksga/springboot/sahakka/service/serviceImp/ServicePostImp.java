package org.ksga.springboot.sahakka.service.serviceImp;

import org.ksga.springboot.sahakka.dto.servicepostdto.AllServicePostDto;
import org.ksga.springboot.sahakka.dto.servicepostdto.ServicePostDto;
import org.ksga.springboot.sahakka.model.postService.*;
import org.ksga.springboot.sahakka.repository.ServicePostRepository;
import org.ksga.springboot.sahakka.service.ServicePostService;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServicePostImp implements ServicePostService {
    private ServicePostRepository servicePostRepository;


    @Autowired
    public ServicePostImp( ServicePostRepository repository) {
        this.servicePostRepository = repository;
    }

    /*@Override
    public List<ServicePost> getAllReportedServicePost() {
        return servicePostRepository.getAllReportedServicePost();
    }*/

    @Override
    public String maxServicePostId() {
        return servicePostRepository.maxServicePostId();
    }

    @Override
    public Boolean createServicePostReport(String email, int id, int service_post_id,  String reason ,String created_date) {
        return servicePostRepository.createServicePostReport(email,id,service_post_id, reason,created_date);
    }

    @Override
    public int countServicePostByUsernameAndCategory(String username, String category_type) {
        return servicePostRepository.countServicePostByUsernameAndCategory(username,category_type);
    }

    @Override
    public List<AllServicePostDto> getServicePostByUsernameAndCategory(String username, String category_type, int limit, int offset) {
        return servicePostRepository.getServicePostByUsernameAndCategory(username,category_type,limit,offset);
    }

    @Override
    public int countServicePostByCategory(String category_type) {
        return servicePostRepository.countServicePostByCategory(category_type);
    }

    @Override
    public List<AllServicePostDto> getServicePostByCategory(String category_type, int limit, int offset) {
        return servicePostRepository.getServicePostByCategory(category_type,limit,offset);
    }

    @Override
    public List<AllServicePostDto> getServicePostByUsername(String username, int limit, int offset) {
        return servicePostRepository.getServicePostByUsername(username,limit,offset);
    }

    @Override
    public int countServicePostOfUsername(String username) {
        return servicePostRepository. countServicePostOfUsername(username);
    }

    @Override
    public List<AllServicePostDto> allOwnerServicePost(String email, int limit, int offset, String post_status) {
        return servicePostRepository.allOwnerServicePost(email,limit,offset,post_status);
    }

    @Override
    public int countAllOwnerServicePost(String email, String post_status) {
        return servicePostRepository.countAllOwnerServicePost(email,post_status);
    }

    @Override
    public List<AllServicePostDto> allNormalServicePost(int limit , int offset) {
        return servicePostRepository.allNormalServicePost(limit,offset);
    }

    @Override
    public int allAvailableServicePost() {
        return servicePostRepository.allAvailableServicePost();
    }

    @Override
    public List<Review> allServicePostReview(int service_post_id) {
        return servicePostRepository.allServicePostReview(service_post_id);
    }

    @Override
    public ServiceCard findServiceCardById(int id) {
        return servicePostRepository.findServiceCardById(id);
    }

    @Override
    public List<String> categoryOfServicePostById(int id) {
        return servicePostRepository.categoryOfServicePostById(id);
    }

    @Override
    public boolean createServicePost(int id, String email, String title, String post_image, String post_status, String post_description) {
        return servicePostRepository.createServicePost(id,email,title,post_image,post_status,post_description);
    }
    @Override
    public List<Category> allCategories() {
        return servicePostRepository.allCategories();
    }
    @Override
    public boolean addCategoriesToServicePost(int service_post_id, int category_id) {
        return servicePostRepository.addCategoriesToServicePost(service_post_id,category_id);
    }
    @Override
    public List<ServicePostCategory> getCategoryFromServicePost(int service_post_id) {
        return servicePostRepository.getCategoryFromServicePost(service_post_id);
    }
    @Override
    public boolean isServicePostCategoryDeleted(String email, int id) {
        return servicePostRepository.isServicePostCategoryDeleted(email,id);
    }
    @Override
    public boolean isCategoryServicePostDeleted(int service_post_id) {
        return servicePostRepository.isCategoryServicePostDeleted(service_post_id);
    }
    @Override
    public int allServicePostOfUser(String email) {
        return servicePostRepository.allServicePostOfUser(email);
    }

    @Override
    public boolean deleteReview(int service_post_id) {
        return servicePostRepository.deleteReview(service_post_id);
    }

    @Override
    public boolean deleteAllReviewReply(int service_post_id, int parent_review_id) {
        return servicePostRepository.deleteAllReviewReply(service_post_id,parent_review_id) ;
    }
    @Override
    public String getReviewId(int service_post_id) {
        return servicePostRepository.getReviewId(service_post_id);
    }

    @Override
    public int contReview(int service_post_id) {
        return servicePostRepository.contReview(service_post_id);
    }

    @Override
    public boolean updateServicePost(int id, String title, String post_image, String post_status, String post_description) {
        return servicePostRepository.updateServicePost(id,title,post_image,post_status,post_description);
    }

    // No longer need to use this .
    @Override
    public boolean updateCategoryServicePost(int category_id, int id) {
        return servicePostRepository.updateCategoryServicePost(category_id,id);
    }

    @Override
    public boolean deleteServicePostCategory(int service_post_id) {
        return servicePostRepository.deleteServicePostCategory(service_post_id);
    }

    @Override
    public List<Integer> allServicePost() {
        return servicePostRepository.allServicePost();
    }



    //=============================

 /*   @Override
    public List<ServicePostDto> allServicePost(Paging paging) {
            paging.setTotalCount(servicePostRepository.countAllRecord());
            List<ServicePostDto> tdoList = new ArrayList<>();

            for(ServicePost s: servicePostRepository.findAll(paging)){
                    ServicePostDto dto = mapper.map(s, ServicePostDto.class);
                    tdoList.add(dto);
            }
      return tdoList;
    }*/





    @Override
    public boolean delete(int id) {
        return servicePostRepository.delete(id);
    }

    @Override
    public String roleOfUser(String email) {
        return servicePostRepository.roleOfUser(email);
    }

    @Override
    public boolean addReviewToServicePost(int id, String email, int service_post_id, String review_text, int number_of_stars , String review_date) {
        return servicePostRepository.addReviewToServicePost(id, email,service_post_id,review_text,number_of_stars, review_date);
    }

    @Override
    public boolean replyReviewToServicePost(int service_post_id, String email, String review_text,String review_date, int parent_review_id) {
        return servicePostRepository.replyReviewToServicePost(service_post_id,email,review_text,review_date, parent_review_id);
    }

    @Override
    public String verifyServicePost(String email, int id) {
        return servicePostRepository.verifyServicePost(email, id);
    }

    @Override
    public String maxReviewId() {
        return servicePostRepository.maxReviewId();
    }


    @Override
    public boolean update(int id, int user_id, String title, String post_image, String post_description, String post_status) {
        return servicePostRepository.update(id,user_id,title,post_image,post_description,post_status);
    }


}
