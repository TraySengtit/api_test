package org.ksga.springboot.sahakka.service.serviceImp.dashbaordServiceImp;

import org.ksga.springboot.sahakka.model.category.Category;
import org.ksga.springboot.sahakka.repository.admin_dashboard_repo.CategoryRepo;
import org.ksga.springboot.sahakka.service.serviceImp.adminDashboardService.CategoryService;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {

    private CategoryRepo categoryRepo;

    @Autowired
    public CategoryServiceImp(CategoryRepo categoryRepo) {
        this.categoryRepo = categoryRepo;
    }

    @Override
    public boolean update(int category_id, String category_type) {
        return categoryRepo.update(category_id,category_type);
    }

    @Override
    public List<Category> findAllCategoryFilter(String search_query, Paging paging) {
        paging.setTotalCount(categoryRepo.countAllCategoryFilter(search_query));
        return categoryRepo.findAllCategory(search_query, paging);
    }

    @Override
    public List<Category> findAllSuggestCategoryFilter(String search_query, Paging paging) {
        paging.setTotalCount(categoryRepo.countAllCategorySuggestFilter(search_query));
        return categoryRepo.findAllCategorySuggestion(search_query, paging);
    }

    @Override
    public Long CountAllCategory() {
        return categoryRepo.CountAllCategory();
    }


    @Override
    public Long CountAllSuggest() {
        return categoryRepo.CountAllCategorySuggest();
    }

    @Override
    public Boolean addCategory(Category category) {
        return categoryRepo.addCategory(category);
    }

    @Override
    public Category findOneCategoryById(int category_id) {
        return categoryRepo.findOneCategoryByid(category_id);
    }

    @Override
    public Boolean updateCategoryById(Category category) {
        return categoryRepo.addCategory(category);
    }

    @Override
    public Boolean deleteCategoryById(int category_id) {
        return categoryRepo.deleteCategoryById(category_id);
    }

    @Override
    public Boolean deleteCategorySuggestById(int category_id) {
        return categoryRepo.deleteCategorySuggestById(category_id);
    }
}
