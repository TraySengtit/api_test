package org.ksga.springboot.sahakka.service;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.ksga.springboot.sahakka.dto.postDto.AllProjectPostDto;
import org.ksga.springboot.sahakka.dto.postDto.PostDto;
import org.ksga.springboot.sahakka.model.category.Category;
import org.ksga.springboot.sahakka.model.post.Comment;
import org.ksga.springboot.sahakka.model.post.Post;
import org.ksga.springboot.sahakka.model.post.PostDeadline;
import org.ksga.springboot.sahakka.model.post.ProjectPost;
import org.ksga.springboot.sahakka.utilities.Paging;

import java.util.Date;
import java.util.List;

public interface PostService {

   int countLikeOfServicePost(int post_id);
   List<PostDeadline> allProjectPostDeadline();
   Boolean updatePostStatus(int post_id);
   String maxReportId();
   Boolean createReport(int id,int post_id,String email,String reason,String created_date);


   List<AllProjectPostDto>findProjectPostByUsernameAndCategory(String username, String category_type , int limit, int offset);
   int countPostByUsernameAndCategory(String username,String category_type);

   int countProjectPostByCategory(String category_type);
   List<AllProjectPostDto> findProjectByCategory(String category_type , int limit, int offset);

   // Filtering :
   int countProjectsByUsername(String username);
   List <AllProjectPostDto> findProjectPostByUsername(String username ,int limit, int offset);

   //Find owner of the all post
   List<AllProjectPostDto> findOwnerProjectPosts(String email, int limit , int offset ,String post_status);
   int countOwnerProjectPosts(String email,String post_status);


   //Find all post
   int countNormalProjectPosts();
   List<AllProjectPostDto> findAllNormalProjectPosts(int limit , int offset);

   // Find Profile
   int countOwnerNormalProjectPosts(String email);

   List<ProjectPost> allNormalProjectPosts(int user_id);
   List<ProjectPost> allProjectPosts(int user_id);

   List<Category> categoryOfPost(int post_id);
   List<String> linksUser(int user_id);
   List<Comment> allCommentOfPost(int post_id);
   Post findPostById(int post_id);

   boolean createProjectPost(String email, int post_id, String title , String description, String post_image, String post_deadline, String created_date,String post_status);
   String maxPostId();
   boolean addCategoryToPost(int post_id, int category_id);
   boolean updateProject(int post_id, String title, String description, String email,String post_image,String post_deadline , String post_status);
   boolean updateCategoryPost(int category_id,int post_id);
   String likeOfPost(int post_id );
   String verifyPostOwnership(String email,int post_id);
   boolean deletePostCategory(int post_id);
   int countUserPost(String email);
   boolean comment(int comment_id,String email,int post_id,String comment_text,String created_date);
   String maxCommentId();
   boolean replyComment(int comment_id, String email , int post_id, int parent_comment_id,String comment_text);
   int countComment(int post_id);
   int verifyPostId(int post_id);
   String verifyCommentId(int comment_id);
   String maxLikeId();
   boolean likePost(String email,int id, int post_id);
   boolean unlikePost(String email, int post_id);
   String reactId(String email,  int post_id);
   int countLikes(int post_id);
   boolean updatePostLike(int likes, int post_id);
   boolean deletePostLike(int post_id);
   boolean deleteReplies(int post_id);
   boolean deleteComments(int post_id);



   public List<PostDto> allPost(Paging paging);

   public boolean deletePost(  int post_id);

//   public boolean delete(@Param("id") int id);
//
//   public boolean insert(ServicePostRequest postRequest);
//   public boolean update(int id, int user_id, String title, String post_image, String post_description, String post_status);
}
