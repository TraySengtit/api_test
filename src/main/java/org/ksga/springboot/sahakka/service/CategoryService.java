package org.ksga.springboot.sahakka.service;

import org.apache.ibatis.annotations.Param;
import org.ksga.springboot.sahakka.dto.categoryDto.CategoryDto;
import org.ksga.springboot.sahakka.dto.postDto.PostDto;
import org.ksga.springboot.sahakka.model.category.Category;
import org.ksga.springboot.sahakka.model.post.Post;
import org.ksga.springboot.sahakka.payload.request.CategoryRequest.CategoryRequest;
import org.ksga.springboot.sahakka.payload.request.ServicePostRequest.ServicePostRequest;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


public interface CategoryService {
   List<String> getAllCategories(Boolean suggested_state , int limit , int offset);
   int countAllCategories (Boolean suggested_state);
   int countCategoriesByType(String category_type);
   boolean suggestedCategoryUpdate(int category_id);
   boolean suggestedCategoryRemove(int category_id);
   String getSuggestedCategoryById(int category_id);
   String maxCategory();
   boolean addNewCategory(int category_id, String category_type);
   public Category findById( int category_id);
   int countCategories();
   public boolean delete(int category_id);

 //  public List<CategoryDto> allCategories(Paging paging);


   public boolean update(@Param("category_id") int category_id, String category_type);


}
