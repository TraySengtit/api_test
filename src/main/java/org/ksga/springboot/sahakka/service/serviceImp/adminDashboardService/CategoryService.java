package org.ksga.springboot.sahakka.service.serviceImp.adminDashboardService;

import org.ksga.springboot.sahakka.model.category.Category;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.springframework.stereotype.Controller;

import java.util.List;

public interface CategoryService {
    public boolean update(int category_id, String category_type);
    public List<Category> findAllCategoryFilter(String search_query, Paging paging);

    public List<Category> findAllSuggestCategoryFilter(String search_query, Paging paging);

    public Long CountAllCategory();


    public Long CountAllSuggest();

    public Boolean addCategory(Category category);

    public Category findOneCategoryById(int category_id);

    public Boolean updateCategoryById(Category category);

    public Boolean deleteCategoryById(int category_id);

    public Boolean deleteCategorySuggestById(int category_id);


}
