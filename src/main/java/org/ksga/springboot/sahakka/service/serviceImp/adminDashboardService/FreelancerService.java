package org.ksga.springboot.sahakka.service.serviceImp.adminDashboardService;

import org.ksga.springboot.sahakka.model.dashboardModel.FreelancerPost;
import org.ksga.springboot.sahakka.model.dashboardModel.ReportedServicePost;
import org.ksga.springboot.sahakka.model.postService.AllReportedServicePost;
import org.ksga.springboot.sahakka.model.postService.ServicePost;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.utilities.Paging;

import java.util.List;

public interface FreelancerService {
    //Freelancer Posts
    Boolean deleteServiceReport(int id);
    List<AllReportedServicePost> allReportOfServicePost(int service_post_id);

    long countReportedCard();
    List<ReportedServicePost> getAllReportedServicePost(String search_query, Paging paging);
    public List<FreelancerPost> findFreelancerFilterPost(String search_query, Paging paging);

    public FreelancerPost findFreelancerByID(int id);

    //Freelancer
    public List<User> findAllFreelancer(String search_query, Paging paging);

    public FreelancerPost findFreelancerById(int user_id);

    public Long countPost();

    public Boolean deleteFreelancerById(int f_id);

}
