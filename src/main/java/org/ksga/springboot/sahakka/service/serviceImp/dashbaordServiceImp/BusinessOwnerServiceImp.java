package org.ksga.springboot.sahakka.service.serviceImp.dashbaordServiceImp;

import org.ksga.springboot.sahakka.model.dashboardModel.OwnerPost;
import org.ksga.springboot.sahakka.model.post.AllReportedOfPost;
import org.ksga.springboot.sahakka.model.post.AllReportedProjectPost;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.repository.admin_dashboard_repo.BusinessOwnerRepo;
import org.ksga.springboot.sahakka.service.serviceImp.adminDashboardService.BusinessOwnerService;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BusinessOwnerServiceImp implements BusinessOwnerService {

    private BusinessOwnerRepo businessOwnerRepo;

    @Autowired
    public BusinessOwnerServiceImp(BusinessOwnerRepo businessOwnerRepo) {
        this.businessOwnerRepo = businessOwnerRepo;
    }

    @Override
    public List<AllReportedProjectPost> findAllReportedProjectPosts(String search_query, Paging paging) {
        return businessOwnerRepo.findAllReportedProjectPosts(search_query,paging);
    }

    @Override
    public long countReportedPost() {
        return businessOwnerRepo.countReportedPost();
    }

    @Override
    public List<AllReportedOfPost> allReportOfProjectPosts(int post_id) {
        return businessOwnerRepo.allReportOfProjectPosts(post_id);
    }

    @Override
    public Boolean deleteReport(int id) {
        return businessOwnerRepo.deleteReport(id);
    }

    //All posts business Owner
    @Override
    public List<OwnerPost> findAllPosts(String search_query, Paging paging) {
        paging.setTotalCount(businessOwnerRepo.countAllPost(search_query));
        return businessOwnerRepo.findAllPosts(search_query, paging);
    }

    @Override
    public Boolean deletePost(int post_id) {
        return businessOwnerRepo.deletePost(post_id);
    }

    @Override
    public OwnerPost findById(int post_id) {
        return businessOwnerRepo.findById(post_id);
    }

    @Override
    public Long countPost() {
        return businessOwnerRepo.countPost();
    }

    //All business owner
    @Override
    public List<User> findAllBusinessOwner(String search_query, Paging paging) {
        paging.setTotalCount(businessOwnerRepo.countAllBusinessOwner(search_query));
        return businessOwnerRepo.findAllBusinessOwner(search_query, paging);
    }

    @Override
    public OwnerPost findBusinessownerByid(int user_id) {
        return businessOwnerRepo.findBusinessOwner(user_id);
    }

    @Override
    public Boolean deleteBusinessOwner(int b_id) {
        return businessOwnerRepo.deleteBusinessOwner(b_id);
    }
}
