package org.ksga.springboot.sahakka.service.serviceImp;


import org.ksga.springboot.sahakka.model.BusinessOwner;
import org.ksga.springboot.sahakka.model.Freelancer;

import org.ksga.springboot.sahakka.model.postService.UserServicePost;
import org.ksga.springboot.sahakka.model.user.AuthUser;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.model.user.UserRole;
import org.ksga.springboot.sahakka.repository.UserRepository;
import org.ksga.springboot.sahakka.service.UserService;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImp implements UserService {

UserRepository userRepository;

@Autowired
    public UserServiceImp(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @Override
    public String maxUserId() {
        return userRepository.maxUserId();
    }

    @Override
    public String maxUserRoleId() {
        return userRepository.maxUserRoleId();
    }

    @Override
    public Boolean updateAdminPassword(String password, int user_id) {
        return userRepository.updateAdminPassword(password,user_id);
    }

    @Override
    public Boolean deleteAdmin(int user_id) {
        return userRepository.deleteAdmin(user_id);
    }

    @Override
    public String adminEmail(int user_id) {
        return userRepository.adminEmail(user_id);
    }

    @Override
    public long countSearchedAdmin(String username) {
        return userRepository.countSearchedAdmin(username);
    }

    @Override
    public long countAllAdmin() {
        return userRepository.countAllAdmin();
    }

    @Override
    public List<User> allAdmins(String search_query, Paging paging) {
        return userRepository.allAdmins(search_query,paging);
    }

    @Override
    public UserServicePost findFreelancerById(int id) {
        return userRepository.findFreelancerById(id);
    }

    @Override
    public int verifyTokenOfUser(String email, String reset_password_token) {
        return userRepository.verifyTokenOfUser(email,reset_password_token);
    }

    @Override
    public Boolean updateForgottenPassword(String email, String passwords, String reset_password_token) {
        return userRepository.updateForgottenPassword(email,passwords,reset_password_token);
    }

    @Override
    public Boolean setResetTokenPassword(String reset_password_token, String email) {
        return userRepository.setResetTokenPassword(reset_password_token,email);
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    @Override
    public boolean deleteUser(String email) {
        return userRepository.deleteUser(email);
    }

    @Override
    public String verifyEmail(String email) {
        return userRepository.verifyEmail(email);
    }

    @Override
    public boolean deleteSocialLinkOfUser(String email) {
        return userRepository.deleteSocialLinkOfUser(email);
    }

    @Override
    public User findUserById(int user_id) {
        return userRepository.findUserById(user_id);
    }

    @Override
    public String providerUser(String email) {
        return userRepository.providerUser(email);
    }

    @Override
    public String maxUserSocialMedia() {
        return userRepository.maxUserSocialMedia();
    }

    @Override
    public boolean addSkills(String email, int id,  String social_link) {
        return userRepository.addSkills(email,id, social_link);
    }

    @Override
    public AuthUser findUserByUsername(String email) {
        return userRepository.findUserByUsername(email);
    }


    @Override
    public List<User> allUsers() {
        return userRepository.allUser();
    }

    @Override
    public User login(String email) {
        return userRepository.login(email);
    }

    @Override
    public String existedEmail(String email) {
        return userRepository.existedEmail(email);
    }

    @Override
    public boolean addNewUser(int user_id, String username, String email, String password, String provider_type, boolean is_active, String profile_image, String email_address, String telephone, String created_date, String bio, String gender, String address, String nationality) {
        return userRepository.addNewUser(user_id,username,email,password,provider_type,is_active,profile_image,email_address,telephone,created_date,bio,gender,address,nationality);
    }

    @Override
    public boolean updateUser(int user_id, String username, String profile_image, String email_address, String telephone, String bio, String gender, String address, String nationality) {
        return userRepository.updateUser(user_id,username,profile_image,email_address,telephone,bio,gender,address,nationality);
    }


    @Override
    public boolean addNewUserAsFreelancer(int freelancer_id, String educational_background, String work_experience, String skills, String languages, String achievement ,String national_id) {
        return userRepository.addNewUserAsFreelancer(freelancer_id,educational_background,work_experience,skills,languages,achievement,national_id);
    }
    @Override
    public boolean addNewUserAsBusinessOwner(int bo_id, String company_name, String company_contact, String company_location, String business_description, String company_logo) {
        return userRepository.addNewUserAsBusinessOwner(bo_id,company_name,company_contact,company_location, business_description,company_logo);
    }
    @Override
    public boolean addRoleToUser(int id, int user_id, int role_id) {
        return userRepository.addRoleToUser(id,user_id,role_id);
    }

    @Override
    public List<UserRole> allUserRoles() {
        return userRepository.allUserRoles();
    }
    @Override
    public Freelancer freelancerUser(int id) {
        return userRepository.freelancerUser(id);
    }
    @Override
    public BusinessOwner businessUser(int id) {
        return userRepository.businessUser(id);
    }



    @Override
    public boolean updateFreelancer(int freelancer_id, String educational_background, String work_experience, String skills, String languages, String achievement ,String national_id) {
        return userRepository.updateFreelancer(freelancer_id,educational_background,work_experience,skills,languages,achievement,national_id);
    }

    @Override
    public int findIdByUserEmail(String email) {
        return userRepository.findIdByUserEmail(email);
    }

    @Override
    public boolean updateBusinessOwner(int bo_id, String company_name, String company_contact, String company_location, String company_logo, String business_description) {
        return userRepository.updateBusinessOwner(bo_id,company_name,company_contact,company_location,company_logo,business_description);
    }

    @Override
    public String previousPassword(String email) {
        return userRepository.previousPassword(email);
    }

    @Override
    public boolean updatePassword(String passwords, String email) {
        return userRepository.updatePassword(passwords,email);
    }

}
