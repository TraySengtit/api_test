package org.ksga.springboot.sahakka.repository;

import org.apache.ibatis.annotations.*;
import org.ksga.springboot.sahakka.dto.postDto.UserPost;
import org.ksga.springboot.sahakka.dto.servicepostdto.AllServicePostDto;
import org.ksga.springboot.sahakka.dto.servicepostdto.ReviewServicePostDto;
import org.ksga.springboot.sahakka.model.postService.*;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository

public interface ServicePostRepository {


    // Get all reported service post
  /*  @Select("select distinct(ssp.*) from shk_service_posts ssp inner join shk_report_service_post srsp on ssp.id = srsp.service_post_id")


    @Results(
        value={
                @Result(property = "id", column = "id"),
                @Result(property = "user_id", column = "user_id"),
                @Result(property = "post_image",column = "post_image"),
                @Result(property = "title",column = "title"),
                @Result(property = "post_description",column = "post_description"),
                @Result(property = "user",column = "user_id",one = @One(select = "userOfServicePost"))

        }

        )
    List<ServicePost> getAllReportedServicePost();*/



    @Select("select  max(id) from shk_report_service_post")
    String maxServicePostId();
    @Insert("insert into shk_report_service_post(id, service_post_id, user_id, reason,created_date) values (#{id},#{service_post_id},(select user_id from shk_users where email=#{email} ),#{reason},#{created_date})")
    Boolean createServicePostReport(String email, int id,int service_post_id, String reason,String created_date);

    // get owner service post
    @Select("select count(*) from shk_service_posts where post_status=#{post_status} and user_id = (select user_id from shk_users where email=#{email}) ")
    int countAllOwnerServicePost(String email, String post_status);
    //  id, user_id,post_image,title
    @Select("select * from shk_service_posts where post_status =#{post_status} and user_id = (select user_id from shk_users where email=#{email}) limit #{limit} offset #{offset}")
    @Results(
            value = {
                    @Result(property = "id", column = "id"),
                    @Result(property = "user_id", column = "user_id"),
                    @Result(property = "post_image",column = "post_image"),
                    @Result(property = "title",column = "title"),
                    @Result(property = "post_description",column = "post_description"),
                    @Result(property = "reviews",column = "id" , one = @One(select = "postReview")),
                    @Result(property = "user",column = "user_id",one = @One(select = "userOfServicePost")),
                    @Result(property = "category",column = "id",many = @Many(select = "categoryOfServicePost"))
            }
    )

    List<AllServicePostDto> allOwnerServicePost(String email, int limit, int offset,String post_status);

    @Select("select sc.category_type from shk_service_post_category sspc inner join shk_categories sc on sc.category_id = sspc.category_id    where service_post_id=#{id}")
    List<String >categoryOfServicePost(int id);

     //Filtering Service Post
    @Select("select count(*) from shk_service_posts ssp inner join shk_service_post_category sspc on ssp.id = sspc.service_post_id inner join shk_categories sc on sc.category_id = sspc.category_id" +
            " inner join shk_users su on ssp.user_id = su.user_id" +
            " where   UPPER(sc.category_type )  like upper('%'|| #{category_type} || '%') and UPPER(su.username)  like upper('%'|| #{username} || '%')   and ssp.post_status='normal'")
    int countServicePostByUsernameAndCategory(String username,String category_type);
    @Select("select ssp.* from shk_service_posts ssp inner join shk_service_post_category sspc on ssp.id = sspc.service_post_id inner join shk_categories sc on sc.category_id = sspc.category_id\n" +
            " inner join shk_users su on ssp.user_id = su.user_id\n" +
            " where   UPPER(sc.category_type )  like upper('%'|| #{category_type} || '%') and UPPER(su.username)  like upper('%'|| #{username} || '%')   and ssp.post_status='normal' limit #{limit} offset #{offset}")
    @Results(
            value = {
                    @Result(property = "id", column = "id"),
                    @Result(property = "user_id", column = "user_id"),
                    @Result(property = "post_image",column = "post_image"),
                    @Result(property = "title",column = "title"),
                    @Result(property = "reviews",column = "id" , one = @One(select = "postReview")),
                    @Result(property = "user",column = "user_id",one = @One(select = "userOfServicePost")),
                    @Result(property = "category",column = "id",many = @Many(select = "categoryOfServicePost"))
            }
    )
    List<AllServicePostDto> getServicePostByUsernameAndCategory(String username,String category_type, int limit , int offset);

    @Select("select count(*) from shk_service_posts ssp inner join shk_users su on ssp.user_id = su.user_id where post_status ='normal' and UPPER(su.username)  like upper('%'|| #{username} || '%')")
    int countServicePostOfUsername(String username);

    @Select("select ssp.* from shk_service_posts ssp inner join shk_users su on ssp.user_id = su.user_id where post_status ='normal' and  UPPER(su.username )  like upper('%'|| #{username} || '%') limit #{limit} offset #{offset}")
    @Results(
            value = {
                    @Result(property = "id", column = "id"),
                    @Result(property = "user_id", column = "user_id"),
                    @Result(property = "post_image",column = "post_image"),
                    @Result(property = "title",column = "title"),
                    @Result(property = "reviews",column = "id" , one = @One(select = "postReview")),
                    @Result(property = "user",column = "user_id",one = @One(select = "userOfServicePost")),
                    @Result(property = "category",column = "id",many = @Many(select = "categoryOfServicePost"))
            }
    )
    List <AllServicePostDto> getServicePostByUsername(String username,int limit, int offset);

    @Select("select count(*) from shk_service_posts ssp inner join shk_service_post_category sspc on ssp.id = sspc.service_post_id inner join shk_categories sc on sc.category_id = sspc.category_id" +
            " where UPPER(sc.category_type )  like upper('%'|| #{category_type} || '%') and ssp.post_status='normal'")
    int countServicePostByCategory(String category_type);
    @Select("select ssp.* from shk_service_posts ssp inner join shk_service_post_category sspc on ssp.id = sspc.service_post_id inner join shk_categories sc on sc.category_id = sspc.category_id\n" +
            " where UPPER(sc.category_type )  like upper('%'|| #{category_type} || '%') and ssp.post_status='normal' limit #{limit} offset #{offset}")
    @Results(
            value = {
                    @Result(property = "id", column = "id"),
                    @Result(property = "user_id", column = "user_id"),
                    @Result(property = "post_image",column = "post_image"),
                    @Result(property = "title",column = "title"),
                    @Result(property = "reviews",column = "id" , one = @One(select = "postReview")),
                    @Result(property = "user",column = "user_id",one = @One(select = "userOfServicePost")),
                    @Result(property = "category",column = "id",many = @Many(select = "categoryOfServicePost"))
            }
    )
    List<AllServicePostDto> getServicePostByCategory(String category_type , int limit, int offset);


    // Get all the service post
    @Select("select count(*) from shk_service_posts where post_status='normal'")
    int allAvailableServicePost();

  //  id, user_id,post_image,title
    @Select("select * from shk_service_posts where post_status ='normal' limit #{limit} offset #{offset}")
    @Results(
            value = {

                    @Result(property = "id", column = "id"),
                    @Result(property = "user_id", column = "user_id"),
                    @Result(property = "post_image",column = "post_image"),
                    @Result(property = "title",column = "title"),
                    @Result(property = "reviews",column = "id" , one = @One(select = "postReview")),
                    @Result(property = "user",column = "user_id",one = @One(select = "userOfServicePost")),
                    @Result(property = "category",column = "id",many = @Many(select = "categoryOfServicePost"))

            }
    )

    List<AllServicePostDto> allNormalServicePost(int limit, int offset);

    @Select("select count(*) from shk_reviews where service_post_id =#{service_post_id} and parent_review_id is null")
    String postReview(int service_post_id);

    @Select("select su.user_id,su.username,su.profile_image ,sf.skills from shk_users su inner join shk_freelancers sf on su.user_id = sf.freelancer_id where su.user_id =#{user_id}")
    ServiceCardUser userOfServicePost(int user_id);

    @Select("select sr.id,sr.service_post_id,sr.review_text,sr.review_date,sr.number_of_stars,su.user_id,su.username ,su.profile_image from shk_reviews sr inner join shk_users su on sr.user_id = su.user_id where sr.service_post_id=#{service_post_id}\n")
    List<Review> allServicePostReview(int service_post_id);


    //Find the service post by id
    @Select("select * from shk_service_posts where id=#{id}")
    ServiceCard findServiceCardById(int id);

    @Select("select category_type from shk_service_post_category sspc\n" +
            "inner join shk_categories sc on sspc.category_id = sc.category_id where sspc.service_post_id=#{service_post_id};")
    List<String> categoryOfServicePostById(int service_post_id);




    @Delete("delete from shk_service_posts where shk_service_posts.id = #{id}")
    public Boolean delete(@Param("id")int id);

 // Create New Service Post :
    @Select("select id from shk_service_posts")
 List<Integer> allServicePost();

  @Insert("insert into shk_service_posts( id, user_id, title, post_image, post_status, post_description)\n" +
          "VALUES (#{id},(select  user_id from shk_users where email=#{email}),#{title},#{post_image},#{post_status},#{post_description})\n")
  boolean createServicePost(int id,String email,String title , String post_image, String post_status, String post_description);

  @Select("select  * from shk_categories where suggested_state=false")
  List<Category> allCategories();

  @Insert("insert into shk_service_post_category ( service_post_id, category_id) VALUES (#{service_post_id},#{category_id})")
  boolean addCategoriesToServicePost(int service_post_id,int category_id);

  @Select("select * from shk_service_post_category where service_post_id=#{service_post_id};")
  List<ServicePostCategory> getCategoryFromServicePost(int service_post_id);

  @Delete("delete from shk_service_post_category where service_post_id =#{service_post_id};")
  boolean isCategoryServicePostDeleted(int service_post_id);

  // Id is the service post
    @Delete("delete from shk_service_posts where user_id=(select  user_id from shk_users where email=#{email}) AND id=#{id};")
    boolean isServicePostCategoryDeleted(String email,int id);

    // Check the role the user
    @Select("select sr.role from shk_user_role as sur inner join shk_roles as sr\n" +
          "on sur.role_id = sr.role_id where user_id = (select user_id from shk_users where email=#{email})\n")
    String roleOfUser(String email);



    //Adding Review
    @Insert("insert into shk_reviews  (id,  service_post_id, user_id, review_text, number_of_stars , review_date) values\n" +
            "(#{id}, #{service_post_id},(select user_id from shk_users where email=#{email}),#{review_text},#{number_of_stars},#{review_date});")
    boolean addReviewToServicePost(int id, String email,int service_post_id,String review_text,int number_of_stars,String review_date);

    @Select("select  max(id) from shk_reviews")
    String maxReviewId();
   //Replying Review
    @Insert("insert into shk_reviews (service_post_id,user_id,review_text,review_date, parent_review_id) values\n" +
            "(#{service_post_id},(select user_id from shk_users where email=#{email}),#{review_text},#{review_date}, #{parent_review_id})")
    boolean replyReviewToServicePost(int service_post_id,String email,String review_text,String review_date, int parent_review_id);
    @Select("select user_id from shk_service_posts where user_id=(select user_id from shk_users where email=#{email}) and id=#{id}\n")
    String verifyServicePost( String email,int id);



    @Select("select count(*) from shk_reviews where service_post_id =#{service_post_id} and parent_review_id is null")
    int contReview(int service_post_id);

    @Select("select  count(*) from shk_service_posts where user_id=(select user_id from shk_users where email=#{email})\n")
    int allServicePostOfUser(String email);
    // To delete the review
    @Select("select id  from shk_reviews where parent_review_id is null and service_post_id =#{service_post_id}")
    String  getReviewId(int service_post_id);
    @Delete("delete from shk_reviews where service_post_id=#{service_post_id} and parent_review_id=#{parent_review_id}")
    boolean deleteAllReviewReply(int service_post_id, int parent_review_id);
    @Delete("delete from shk_reviews where parent_review_id is null and service_post_id =#{service_post_id}")
    boolean deleteReview(int service_post_id);
    @Update("update shk_service_posts\n" +
            "set  title=#{title},post_image=#{post_image},post_status=#{post_status},post_description=#{post_description}\n" +
            "where id=#{id};")
    boolean updateServicePost(int id, String title, String post_image, String post_status, String post_description);
    @Update("update shk_service_post_category \n" +
            "set category_id=#{category_id}\n" +
            "where id=#{id}")
    boolean updateCategoryServicePost(int category_id,int id);
    @Delete("delete from shk_service_post_category where service_post_id=#{service_post_id}")
    boolean deleteServicePostCategory (int service_post_id);

    @Select("select count(id) from shk_service_posts")
    public int countAllRecord();







    @Update("update shk_service_posts set " +
            "id= #{id}," +
            "user_id= #{user_id}," +
            "title= #{title}, " +
            "post_image= #{post_image}," +
            "post_description= #{post_description}," +
            "post_status= #{post_status} where id = #{id}"
            )
    public boolean update (int id, int user_id, String title, String post_image, String post_description, String post_status);


}


