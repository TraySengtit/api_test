package org.ksga.springboot.sahakka.repository.admin_dashboard_repo;

import org.apache.ibatis.annotations.*;
import org.ksga.springboot.sahakka.model.category.Category;
import org.ksga.springboot.sahakka.repository.admin_dashboard_repo.dashboardProvider.DashboardProvider;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepo {

    //Get all Category
//    @Select("Select * from shk_categories where suggested_state = 'false' order by shk_categories.category_id asc")
//    public List<Category> FindAllCategory();




    //Get all Category with filter
    @SelectProvider(method = "findAllCategory", type = DashboardProvider.class)
    public List<Category> findAllCategory(@Param("search_query") String search_query, @Param("paging") Paging paging);

    //Count all Category with filter
    @SelectProvider(method = "countAllCategory", type = DashboardProvider.class)
    public int countAllCategoryFilter(@Param("search_query") String search_query);

    //Count all Category
    @Select("Select count(*) from shk_categories where suggested_state = 'false'")
    public Long CountAllCategory();

    //Count all Suggestion Category
    @Select("Select count(*) from shk_categories where suggested_state = 'true'")
    public Long CountAllCategorySuggest();

    //Insert Category
    @Insert("INSERT INTO shk_categories (category_type, suggested_state) VALUES (#{category_type}, 'false')")
    public Boolean addCategory(Category category);

    //Find one category by Id
    @Select("SELECT category_type from shk_categories where suggested_state = 'false' and category_id = #{category_id}")
    public Category findOneCategoryByid( int category_id);

    //Update category
    @Update("UPDATE shk_categories SET category_type = #{category_type} where category_id = #{category_id}")
    public Boolean updateCategory(Category category);

    //Get all suggest Category and filter
    @SelectProvider(method = "findAllCategorySuggestion", type = DashboardProvider.class)
    public List<Category> findAllCategorySuggestion(@Param("search_query") String search_query, @Param("paging") Paging paging);

    //count Category and filter
    @SelectProvider(method = "countAllCategorySuggest", type = DashboardProvider.class)
    public int countAllCategorySuggestFilter(@Param("search_query") String search_query);

    //Delete category
    @Delete("DELETE from shk_categories where category_id = #{category_id} and suggested_state = 'false'")
    public Boolean deleteCategoryById(int category_id);

    //Detele category suggestion
    @Delete("DELETE from shk_categories where category_id = #{category_id} and suggested_state = 'true'")
    public Boolean deleteCategorySuggestById(int category_id);

    @Update("update shk_categories set " +
            " category_type = #{category_type}" +
            " where category_id=#{category_id} and suggested_state is false")
    boolean update(int category_id, String category_type);
}
