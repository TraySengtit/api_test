package org.ksga.springboot.sahakka.repository.admin_dashboard_repo;

import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface DashboardRepo {

    //Count Freelancer
    @Select("Select count(*) from shk_users INNER JOIN shk_user_role sur on shk_users.user_id = sur.user_id where role_id = 1")
    public Long CountFreelancer();

    //Count Owner
    @Select("Select count(*) from shk_users INNER JOIN shk_user_role sur on shk_users.user_id = sur.user_id where role_id = 2")
    public Long CountOwner();

}
