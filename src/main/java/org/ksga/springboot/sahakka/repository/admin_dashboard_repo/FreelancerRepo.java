package org.ksga.springboot.sahakka.repository.admin_dashboard_repo;

import org.apache.ibatis.annotations.*;
import org.ksga.springboot.sahakka.model.dashboardModel.FreelancerPost;
import org.ksga.springboot.sahakka.model.dashboardModel.OwnerPost;
import org.ksga.springboot.sahakka.model.dashboardModel.ReportedServicePost;
import org.ksga.springboot.sahakka.model.postService.AllReportedServicePost;
import org.ksga.springboot.sahakka.model.postService.ServiceCardUser;
import org.ksga.springboot.sahakka.model.postService.ServicePost;
import org.ksga.springboot.sahakka.model.user.FindFreelancer;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.repository.admin_dashboard_repo.dashboardProvider.DashboardProvider;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FreelancerRepo {

  //All Report of the Service post :

  @Delete("delete from shk_report_service_post where id =#{id}")
  Boolean deleteServiceReport(int id);

  @Select("SELECT  * from shk_report_service_post where service_post_id = #{service_post_id}")
  @Results(
          value={
                  @Result(property = "id", column = "id"),
                  @Result(property = "service_post_id", column = "service_post_id"),
                  @Result(property = "user_id",column = "user_id"),
                  @Result(property = "reason",column = "reason"),
                  @Result(property = "created_date",column = "created_date"),
                  @Result(property = "user",column = "user_id",one = @One(select = "userOfServiceCard"))

          }

  )
  List<AllReportedServicePost> allReportOfServicePost(int service_post_id);
  @Select("select * from shk_users   where user_id =#{user_id}")
  User userOfServiceCard(int user_id);




    // Get all report service post

    @Select("select count(distinct(ssp.*))  from shk_service_posts ssp inner join shk_report_service_post srsp on ssp.id = srsp.service_post_id\n")
     long countReportedCard();

  @SelectProvider(method = "allReportedServicePost", type = DashboardProvider.class)

    /*@Select(" SELECT distinct(ssp.*) FROM shk_service_posts ssp INNER JOIN shk_report_service_post srsp on ssp.id = srsp.service_post_id\n" +
            " ORDER BY ssp.id asc LIMIT 10 OFFSET 0")*/
    @Results(
            value={
                    @Result(property = "id", column = "id"),
                    @Result(property = "user_id", column = "user_id"),
                    @Result(property = "post_image",column = "post_image"),
                    @Result(property = "title",column = "title"),
                    @Result(property = "post_description",column = "post_description"),
                    @Result(property = "user",column = "user_id",one = @One(select = "userOfServicePost"))

            }

    )
  /*su.user_id,su.username,su.profile_image ,sf.skills*/
    List<ReportedServicePost> getAllReportedServicePost(@Param("search_query")  String search_query , @Param("paging")  Paging paging);
    @Select("select * from shk_users su inner join shk_freelancers sf on su.user_id = sf.freelancer_id where su.user_id =#{user_id}")
    FindFreelancer userOfServicePost(int user_id);



    //Select all freelancer post and filter
    @SelectProvider(method = "findAllFreelancer", type = DashboardProvider.class)
    public List<FreelancerPost> findAllFreeLancerPost(@Param("search_query") String search_query, @Param("paging") Paging paging);

    //Count All freelancer with filter
    @SelectProvider(method = "countFreelancerFilterPost", type = DashboardProvider.class)
    public int countFilterFreelancerPost(@Param("search_query") String search_query);

    //Find Freelancer Post by Id
    @Select("Select ssp.id, ssp.title, ssp.post_image, ssp.post_description, sf.work_experience, sf.skills,\n" +
            "sf.languages, sf.educational_background, us.username, us.profile_image, us.email, us.telephone, us.created_date \n" +
            "from shk_users us\n" +
            "inner join shk_freelancers sf on us.user_id = sf.freelancer_id\n" +
            "inner join shk_service_posts ssp on us.user_id = ssp.user_id\n" +
            "where ssp.id = #{id}")
    public FreelancerPost findById(int id);

    //Select all Freelancer and
    @SelectProvider(method = "findAllFreelancerFilter", type = DashboardProvider.class)
    public List<User> findAllFreelancerFilter(@Param("search_query") String search_query, @Param("paging") Paging paging);

    //Count all freelancer and filter
    @SelectProvider(method = "countAllFreelancer", type = DashboardProvider.class)
    public int countAllFreelancer(@Param("search_query") String search_query);

    //find freelancer by id
    @Select("SELECT su.user_id, su.username, su.profile_image, su.telephone, su.email, su.created_date, su.nationality,\n" +
            "su.address, sf.educational_background, sf.languages, sf.skills, sf.work_experience, sf.achievement\n" +
            "from shk_users su\n" +
            "inner join shk_user_role sur on su.user_id = sur.user_id\n" +
            "inner join shk_freelancers sf on su.user_id = sf.freelancer_id\n" +
            "where sur.role_id = 1 and su.user_id = #{user_id}")
    public FreelancerPost findFreelancerById(int user_id);

    //Count post
    @Select("Select count(*) total from shk_service_posts where post_status= 'normal'")
    public Long countPost();

    //Delete Freelancer by Id
    @Delete("DELETE from shk_users su where su.user_id = #{f_id}")
    public Boolean deleteFreelancerById(int f_id);

    //Delete Freelancer post
    @Delete("DELETE from shk_service_posts ssp where ssp.id = #{fp_id}")
    public  Boolean deleteFreelancerPost(int fp_id);

}
