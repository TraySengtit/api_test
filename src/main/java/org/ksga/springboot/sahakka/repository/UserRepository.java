package org.ksga.springboot.sahakka.repository;


import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.*;
import org.ksga.springboot.sahakka.model.BusinessOwner;
import org.ksga.springboot.sahakka.model.Freelancer;

import org.ksga.springboot.sahakka.model.dashboardModel.FreelancerPost;
import org.ksga.springboot.sahakka.model.postService.UserServicePost;
import org.ksga.springboot.sahakka.model.user.AuthRole;
import org.ksga.springboot.sahakka.model.user.AuthUser;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.model.user.UserRole;
import org.ksga.springboot.sahakka.repository.admin_dashboard_repo.dashboardProvider.DashboardProvider;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.springframework.stereotype.Repository;

import javax.swing.*;
import java.util.List;
import java.util.Set;

@Repository
public interface UserRepository {
    // Count admin

    @Select("select max(user_id) from shk_users")
    String maxUserId();

    @Select("select max(id) from shk_user_role")
    String maxUserRoleId();

    @Update("update shk_users set passwords=#{password} where user_id =#{user_id} ")
    Boolean updateAdminPassword(String password , int user_id);

    @Delete("delete from shk_users su where user_id=#{user_id}")
    Boolean deleteAdmin(int user_id);
    // Verify user by id

    @Select("select email from shk_users where user_id =#{user_id}")
    String adminEmail(int user_id) ;

    @Select("select count(*) from shk_users su inner join shk_user_role sur on su.user_id = sur.user_id\n" +
            "inner join shk_roles sr on sur.role_id = sr.role_id where role='ADMIN' and  UPPER(su.username )  like upper('%'|| #{username} || '%') ")
    long countSearchedAdmin(String username);

    @Select("select count(*) from shk_users su inner join shk_user_role sur on su.user_id = sur.user_id\n" +
            "inner join shk_roles sr on sur.role_id = sr.role_id where role='ADMIN' ")
    long countAllAdmin();

    @SelectProvider(method = "findAllAdmins", type = DashboardProvider.class)
    List<User> allAdmins(@Param("search_query") String search_query, @Param("paging") Paging paging);

    @Select("select * from shk_users su inner join shk_freelancers sf on su.user_id = sf.freelancer_id where user_id=#{user_id}")
    @Results(value = {
            @Result(property = "user_id",column = "user_id"),
            @Result(property = "username" ,column ="username"),
            @Result(property = "gender",column = "gender"),
            @Result(property = "profile_image",column = "profile_image"),
            @Result(property = "telephone" ,column = "telephone"),
            @Result(property = "location",column = "address"),
            @Result(property = "official_email",column = "email_address")
            ,@Result(property = "nationality",column = "nationality")
            ,@Result(property = "skills" , column = "skills"),
            @Result(property = "social_media", column = "user_id" ,one = @One(select ="linksUser") )
    })
    UserServicePost findFreelancerById(int user_id);
    @Select("select social_link from shk_user_social_media where user_id =#{user_id}")
    List<String> linksUser(int user_id);


  @Select("select count(*) from shk_users  where reset_password_token=#{reset_password_token} and email=#{email}")
   int verifyTokenOfUser(String email,String reset_password_token);
    //reset forgotten password

    @Update("update shk_users set passwords=#{passwords} where reset_password_token=#{reset_password_token} and email=#{email}")
    Boolean updateForgottenPassword(String email,String passwords,String reset_password_token);
    // reset password

    @Update("update shk_users set  reset_password_token=#{reset_password_token} where email=#{email}")
    Boolean setResetTokenPassword(String reset_password_token,String email);

    @Select("select email from shk_users where email=#{email}")
    String verifyEmail(String email);

    @Delete("delete from shk_users where email =#{email}")
    boolean deleteUser(String email);

    @Delete("delete  from shk_user_social_media where user_id=(select  user_id from shk_users where email=#{email})")
    boolean deleteSocialLinkOfUser(String email);

    @Select("select * from shk_users where user_id =#{user_id}")
    User findUserById (int user_id) ;

    @Select("select max(id) from shk_user_social_media;")
    String maxUserSocialMedia();

    @Insert("insert into shk_user_social_media (id, user_id, social_link) VALUES (#{id},(select user_id from shk_users where email=#{email}),#{social_link})")
    boolean addSkills(String email,int id  ,String social_link);

    @Select("select provider_type from shk_users where email=#{email}")
    String providerUser(String email);

    //Login
    @Select("select * from shk_users where email=#{email}")
    public User login(String email);


    // load User By the user name;
    @Select("select * from shk_users as ut where ut.email=#{email}")
    @Results(
         {
                 @Result(property = "password",column = "passwords"),
                 @Result(property = "roles" ,column ="user_id" , many = @Many(select = "findAllRoleByUserId"))

            }
    )
    AuthUser findUserByUsername(String email);
    @Select("select  * from shk_user_role sur inner join shk_roles sr on sur.role_id = sr.role_id where sur.user_id=#{user_id}\n")
    Set<AuthRole> findAllRoleByUserId(int user_id);


    @Select("select * from shk_users as ut where ut.email=#{email}")
    User findUserByEmail(String email );





    @Select("select * from shk_users")
    List<User> allUser();

    // Verify Email
    @Select("select email from shk_users where email =#{email}")
    String existedEmail (String email);


    // Add New User
    @Insert("INSERT INTO shk_users(\n" +
            " user_id,\n" +
            " username, email,\n" +
            "passwords, provider_type, is_active,\n" +
            "profile_image,\n" +
            " email_address,\n" +
            "  telephone,\n" +
            "   created_date,\n" +
            "  bio, gender,address,nationality\n" +
            ") values" +
            " (#{user_id} , #{username} ,#{email} ,#{password} ,#{provider_type} , #{is_active}\n" +
            ",#{profile_image} ,#{email_address}, #{telephone}, #{created_date}, #{bio}, #{gender} ,#{address},#{nationality})\n")
    boolean addNewUser(int user_id, String username, String email,String password,String provider_type,boolean is_active,String profile_image, String email_address,String telephone, String created_date , String bio,String gender,String address,String nationality);


    // For FreelancerUser
    @Insert("insert into shk_freelancers\n" +
            "( freelancer_id, educational_background, work_experience, skills, languages, achievement,national_id) values\n" +
            "    (#{freelancer_id},#{educational_background},#{work_experience},#{skills},#{languages},#{achievement},#{national_id})\n")
    boolean addNewUserAsFreelancer(int freelancer_id,String educational_background,String work_experience,String skills,String languages,String achievement,String national_id);

    @Insert("insert into shk_business_owners(bo_id, company_name, company_contact, company_location, business_description, company_logo) \n" +
            "values (#{bo_id},#{company_name},#{company_contact},#{company_location},#{business_description},#{company_logo})")
    boolean addNewUserAsBusinessOwner(int bo_id,String company_name,String company_contact ,String company_location , String business_description,String company_logo);

    //Add role to user
    @Insert("insert into  shk_user_role(id, user_id, role_id) VALUES\n" +
            "(#{id},#{user_id},#{role_id})")
    boolean addRoleToUser(int id , int user_id, int role_id);

    //Get All UserRole

    @Select("select * from shk_user_role")
    List<UserRole> allUserRoles();

    // Check for the existence of the freelancer user


    @Select("select  * from shk_freelancers where freelancer_id =#{id}")
    Freelancer freelancerUser(int id);
    @Select("select  * from shk_business_owners where bo_id=#{id}")
    BusinessOwner businessUser(int id);

//-------------
  @Update("update shk_users set username=#{username},profile_image=#{profile_image},email_address=#{email_address},telephone=#{telephone},bio=#{bio},gender=#{gender}, address=#{address},nationality=#{nationality} where user_id=#{user_id}")
  boolean updateUser(int user_id,String username, String profile_image,String email_address,String telephone,String bio,String gender ,String address ,String nationality);

//------------
    @Update("update shk_freelancers set  educational_background=#{educational_background},  work_experience=#{work_experience}, skills=#{skills},languages=#{languages},achievement=#{achievement},national_id=#{national_id}\n" +
            "where freelancer_id=#{freelancer_id}")
    boolean updateFreelancer(int freelancer_id, String educational_background,String work_experience , String skills , String languages ,String achievement ,String national_id);

    @Update("update shk_business_owners set company_name=#{company_name},company_contact=#{company_contact},company_location=#{company_location}, company_logo=#{company_logo} ,business_description=#{business_description} where bo_id=#{bo_id}")
    boolean updateBusinessOwner(int bo_id,String company_name,String company_contact, String company_location, String company_logo,String business_description);

    @Select("select user_id from shk_users where email=#{email}")
    int findIdByUserEmail(String email);

    @Select("select passwords from shk_users where email=#{email}")
    String previousPassword(String email);
    @Update("update shk_users set passwords=#{passwords} where email=#{email}")
    boolean updatePassword(String passwords, String email);
}

