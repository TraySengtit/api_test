package org.ksga.springboot.sahakka.repository;

import org.apache.ibatis.annotations.*;
import org.ksga.springboot.sahakka.model.category.Category;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {




    @Select("select  count(*) from shk_categories where suggested_state=#{suggested_state};")
    int countAllCategories (Boolean suggested_state);

    @Select("select category_type from  shk_categories where suggested_state=#{suggested_state} limit #{limit} offset #{offset}")
    List<String> getAllCategories(Boolean suggested_state , int limit , int offset);


    @Select("select category_type  from shk_categories where suggested_state=true and category_id=#{category_id} ;")
    String getSuggestedCategoryById(int category_id);

    @Update("update shk_categories set suggested_state = false where category_id=#{category_id}")
    boolean suggestedCategoryUpdate(int category_id);

    @Delete("delete from shk_categories where category_id=#{category_id} and suggested_state=true")
    boolean suggestedCategoryRemove(int category_id);

    @Select("select max(category_id) from shk_categories")
    String maxCategory();

    @Select("select  count(*) from shk_categories")
    int countCategories();
    @Select("select  count(*) from shk_categories where suggested_state=false and category_type=#{category_type}")
    int countCategoriesByType(String category_type);

    @Insert("insert into shk_categories (category_id, category_type, suggested_state) VALUES (#{category_id},#{category_type},false)")
    boolean addNewCategory(int category_id, String category_type);


    @Select("select category_id,category_type from shk_categories where category_id=#{category_id} and suggested_state is false")
    public Category findById( int category_id);

    @Delete("delete  from shk_categories where category_id=#{category_id} AND suggested_state=false")
    public boolean delete(int category_id);

    @Update("update shk_categories set " +
            " category_type = #{category_type}" +
            " where category_id=#{category_id} and suggested_state is false")
    public boolean update(int category_id, String category_type);


}
