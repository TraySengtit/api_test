package org.ksga.springboot.sahakka.repository.admin_dashboard_repo.dashboardProvider;

import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.jdbc.SQL;
import org.ksga.springboot.sahakka.utilities.Paging;

import java.util.Locale;

public class DashboardProvider {


    // Find
 //   @Select("select distinct(sp.*) from shk_posts sp inner join shk_report_post srp on sp.post_id = srp.post_id")

    public String allReportedProjectPost(@Param("search_query") String search_query, @Param("paging") Paging paging){

        return  new SQL(){
            {
                SELECT("distinct(sp.*)") ; FROM("shk_posts sp") ; INNER_JOIN("shk_report_post srp on sp.post_id = srp.post_id");
                if (search_query!=null)
                    WHERE("UPPER(sp.title )  like upper('%'|| #{search_query} || '%')");

                ORDER_BY("sp.post_id asc LIMIT #{paging.limit} OFFSET #{paging.offset}");
            }
        }.toString();
    }


    //Find all the reported Service card
    public String allReportedServicePost(@Param("search_query") String search_query, @Param("paging") Paging paging){

        return new SQL(){

            {
            SELECT("distinct(ssp.*)"); FROM("shk_service_posts ssp");
            INNER_JOIN("shk_report_service_post srsp on ssp.id = srsp.service_post_id");
            if (search_query!=null)
                WHERE("UPPER(ssp.title )  like upper('%'|| #{search_query} || '%')");

       ORDER_BY("ssp.id asc LIMIT #{paging.limit} OFFSET #{paging.offset}");
            }
        }.toString();
    }



    public String findAllAdmins(@Param("search_query") String search_query,@Param("paging") Paging paging){

        return new SQL(){
            {
            SELECT("su.*");FROM("shk_users su "); INNER_JOIN("  shk_user_role sur on su.user_id = sur.user_id")
            ; INNER_JOIN("  shk_roles sr on sur.role_id = sr.role_id") ;
            if (search_query !=null)
            WHERE("role='ADMIN' and  UPPER(su.username )  like upper('%'|| #{search_query} || '%')   ") ;
            else WHERE("role='ADMIN'");


            ORDER_BY("su.user_id asc LIMIT #{paging.limit} OFFSET #{paging.offset}");
            }
        }.toString();
    }

    //FindAllOwnerPost
   public String findAllPosts(@Param("search_query") String search_query, @Param("paging") Paging paging){
     return new SQL(){{
         SELECT("sp.post_id, sp.title, sp.description, sp.post_image, sp.likes, sp.created_date," +
                 " su.username, su.profile_image, sbo.company_name, sbo.company_contact, sbo.company_location, sbo.company_logo");
         FROM("shk_posts sp");
         INNER_JOIN("shk_users su on su.user_id = sp.user_id");
         INNER_JOIN("shk_business_owners sbo on su.user_id = sbo.bo_id");
         if(search_query != null)
         WHERE("(sp.description LIKE '%' || #{search_query} || '%' or sp.title LIKE '%' || #{search_query} || '%') and (sp.post_status = 'normal' or sp.post_status = 'NORMAL')");
         else
         WHERE("sp.post_status = 'normal' or sp.post_status = 'NORMAL'");
         ORDER_BY("sp.post_id asc LIMIT #{paging.limit} OFFSET #{paging.offset}");
       }}.toString();
   }

   //CountOwnerPost
   public String countAllPost(@Param("search_query") String search_query){
       return new SQL(){{
           SELECT("count(*)");
           FROM("shk_posts");
           if(search_query != null)
               WHERE("(description LIKE '%' || #{search_query} || '%' or title LIKE '%' || #{search_query} || '%') and (post_status = 'normal' or post_status = 'NORMAL')");
           else
               WHERE("post_status = 'normal' or post_status = 'NORMAL'");
       }}.toString();
   }

   //FindAllBusinessOwner
    public String findAllBusinessOwner(@Param("search_query") String search_query, @Param("paging") Paging paging){
       return new SQL(){{
           SELECT("su.user_id ,su.username, su.gender ,su.email, su.profile_image, su.telephone,\n" +
                           "su.created_date, su.address, su.nationality");
           FROM("shk_users su");
           INNER_JOIN("shk_user_role sur on su.user_id = sur.user_id");
           if(search_query != null)
               WHERE("su.username LIKE '%' || #{search_query} || '%' and sur.role_id = 2");
           else
               WHERE("sur.role_id = 2");
           ORDER_BY("su.user_id asc LIMIT #{paging.limit} OFFSET #{paging.offset}");
       }}.toString();
    }

    //CountBusinessOwner
    public String countAllBusinessOwner(@Param("search_query") String search_query){
        return new SQL(){{
            SELECT("count(*)");
            FROM("shk_users su");
            INNER_JOIN("shk_user_role sur on su.user_id = sur.user_id");
            if(search_query != null)
                WHERE("su.username LIKE '%' || #{search_query} || '%' and sur.role_id = 2");
            else
                WHERE("sur.role_id = 2");
        }}.toString();
    }

    //FindAllFreelancer and filter
    public String findAllFreelancer(@Param("search_query") String search_query, @Param("paging") Paging paging){
       return new SQL(){{
           SELECT("ssp.id, ssp.title, ssp.post_image, ssp.post_description, sf.work_experience, sf.skills," +
                   " sf.languages, sf.educational_background, us.username, us.profile_image, us.user_id");
           FROM("shk_users us");
           INNER_JOIN("shk_freelancers sf on us.user_id = sf.freelancer_id");
           INNER_JOIN("shk_service_posts ssp on us.user_id = ssp.user_id");
           if(search_query != null)
               WHERE("(ssp.post_description LIKE '%' || #{search_query} || '%' or ssp.title LIKE '%' || #{search_query} || '%') and (ssp.post_status = 'normal' or ssp.post_status = 'NORMAL')");
           else
               WHERE("ssp.post_status = 'normal' or ssp.post_status = 'NORMAL'");
           ORDER_BY("ssp.id asc LIMIT #{paging.limit} OFFSET #{paging.offset}");
       }}.toString();
    }

    //Count Freelancer post with filter
    public String countFreelancerFilterPost(@Param("search_query") String search_query){
       return new SQL(){{
           SELECT("count(*)");
           FROM("shk_service_posts ssp");
           if(search_query != null)
               WHERE("(ssp.post_description LIKE '%' || #{search_query} || '%' or ssp.title LIKE '%' || #{search_query} || '%') and (ssp.post_status = 'normal' or ssp.post_status = 'NORMAL')");
           else
               WHERE("ssp.post_status = 'normal' or ssp.post_status = 'NORMAL'");
       }}.toString();
    }

    //Find all freelancer
    public String findAllFreelancerFilter(@Param("search_query") String search_query, @Param("paging") Paging paging){
       return new SQL(){{
           SELECT("su.user_id ,su.username, su.gender ,su.email, su.profile_image," +
                   "su.telephone, su.created_date, su.address, su.nationality");
           FROM("shk_users su");
           INNER_JOIN("shk_user_role sur on su.user_id = sur.user_id");
           if(search_query != null)
               WHERE("su.username LIKE '%' || #{search_query} || '%' and sur.role_id = 1");
           else
               WHERE("sur.role_id = 1");
           ORDER_BY("su.user_id asc LIMIT #{paging.limit} OFFSET #{paging.offset}");
       }}.toString();
    }

    //Count Freelancer with filter
    public String countAllFreelancer(@Param("search_query") String search_query){
        return new SQL(){{
            SELECT("count(*)");
            FROM("shk_users su");
            INNER_JOIN("shk_user_role sur on su.user_id = sur.user_id");
            if(search_query != null)
                WHERE("su.username LIKE '%' || #{search_query} || '%' and sur.role_id = 1");
            else
                WHERE("sur.role_id = 1");
        }}.toString();
    }

    //Find all Category and filter
    public String findAllCategory(@Param("search_query") String search_query, @Param("paging") Paging paging){
        return new SQL(){{
            SELECT("*");
            FROM("shk_categories sc");
            if(search_query != null)
                WHERE("sc.category_type LIKE '%' || #{search_query} || '%' and sc.suggested_state = 'false'");
            else
                WHERE("sc.suggested_state = 'false'");
            ORDER_BY("sc.category_id asc LIMIT #{paging.limit} OFFSET #{paging.offset}");
        }}.toString();
    }

    //Count all category with filter
    public String countAllCategory(@Param("search_query") String search_query){
       return new SQL(){{
           SELECT("count(*)");
           FROM("shk_categories sc");
           if(search_query != null)
           WHERE("sc.category_type LIKE '%' || #{search_query} || '%' and sc.suggested_state = 'false'");
            else
           WHERE("sc.suggested_state = 'false'");
       }}.toString();
    }

    public String findAllCategorySuggestion(@Param("search_query") String search_query, @Param("paging") Paging paging){
       return new SQL(){{
           SELECT("*");
           FROM("shk_categories sc");
           if(search_query != null)
               WHERE("sc.category_type LIKE '%' || #{search_query} || '%' and sc.suggested_state = 'true'");
           else
               WHERE("sc.suggested_state = 'true'");
           ORDER_BY("sc.category_id asc LIMIT #{paging.limit} OFFSET #{paging.offset}");
       }}.toString();
    }

    public String countAllCategorySuggest(@Param("search_query") String search_query){
        return new SQL(){{
            SELECT("count(*)");
            FROM("shk_categories sc");
            if(search_query != null)
                WHERE("sc.category_type LIKE '%' || #{search_query} || '%' and sc.suggested_state = 'true'");
            else
                WHERE("sc.suggested_state = 'true'");
        }}.toString();
    }
}

