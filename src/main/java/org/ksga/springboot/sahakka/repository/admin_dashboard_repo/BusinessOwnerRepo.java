package org.ksga.springboot.sahakka.repository.admin_dashboard_repo;

import org.apache.ibatis.annotations.*;
import org.ksga.springboot.sahakka.model.dashboardModel.OwnerPost;
import org.ksga.springboot.sahakka.model.post.AllReportedOfPost;
import org.ksga.springboot.sahakka.model.post.AllReportedProjectPost;
import org.ksga.springboot.sahakka.model.post.Post;
import org.ksga.springboot.sahakka.model.postService.AllReportedServicePost;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.repository.admin_dashboard_repo.dashboardProvider.DashboardProvider;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BusinessOwnerRepo {


    @Delete("delete from shk_report_post where id=#{id}")
    Boolean deleteReport(int id);
    @Select("select  * from shk_report_post where post_id = #{post_id}")
    @Results(
            value={
                    @Result(property = "id", column = "id"),
                    @Result(property = "post_id", column = "post_id"),
                    @Result(property = "user_id",column = "user_id"),
                    @Result(property = "reason",column = "reason"),
                    @Result(property = "created_date",column = "created_date"),
                    @Result(property = "user",column = "user_id",one = @One(select = "userOfProjectPost"))

            }
    )
    List<AllReportedOfPost> allReportOfProjectPosts(int post_id);

  /*  @Select("SELECT  * from shk_report_service_post where service_post_id = #{service_post_id}")
    @Results(
            value={
                    @Result(property = "id", column = "id"),
                    @Result(property = "service_post_id", column = "service_post_id"),
                    @Result(property = "user_id",column = "user_id"),
                    @Result(property = "reason",column = "reason"),
                    @Result(property = "created_date",column = "created_date"),
                    @Result(property = "user",column = "user_id",one = @One(select = "userOfProjectPost"))

            }
    )
    List<AllReportedServicePost> allReportOfServicePost(int service_post_id);*/


    /// Find all reported Project Post

    @Select("select count(distinct(sp.*)) from shk_posts sp inner join shk_report_post srp on sp.post_id = srp.post_id")
    long countReportedPost();

    @SelectProvider(method = "allReportedProjectPost", type = DashboardProvider.class)
    @Results(
            value={
                    @Result(property = "post_id", column = "post_id"),
                    @Result(property = "user_id", column = "user_id"),
                    @Result(property = "title",column = "title"),
                    @Result(property = "likes",column = "likes"),
                    @Result(property = "description",column = "description"),
                    @Result(property = "created_date",column = "created_date"),
                    @Result(property = "user",column = "user_id",one = @One(select = "userOfProjectPost"))

            }

    )

    List<AllReportedProjectPost> findAllReportedProjectPosts(@Param("search_query") String search_query, @Param("paging") Paging paging);

    @Select("select * from shk_users   where user_id =#{user_id}")
    User userOfProjectPost(int user_id);








    //Find All post and filter
    @SelectProvider(method = "findAllPosts", type = DashboardProvider.class)
    public List<OwnerPost> findAllPosts(@Param("search_query") String search_query, @Param("paging") Paging paging);

    //Delete Post
    @Delete("DELETE from shk_posts sp where sp.post_id = #{post_id} ")
    public boolean deletePost(int post_id);

    //Find by id post
    @Select("SELECT sp.post_id, sp.title, sp.description, sp.post_image, sp.likes, sp.created_date, su.username, su.telephone, su.email, su.profile_image, sbo.company_name\n" +
            ", sbo.company_contact, sbo.company_location, sbo.company_logo, sp.post_status\n" +
            "from shk_posts sp\n" +
            "inner join shk_users su on su.user_id = sp.user_id\n" +
            "inner join shk_business_owners sbo on su.user_id = sbo.bo_id\n" +
            "where sp.post_id = #{post_id}")
    public OwnerPost findById(int post_id);

    //Count all posts
    @Select("Select count(*) total from shk_posts where post_status= 'normal'")
    public Long countPost();

    //Count Post by filter
    @SelectProvider(method = "countAllPost", type = DashboardProvider.class)
    public int countAllPost(@Param("search_query") String search_query);

    //Get all Business owners and filter
    @SelectProvider(method = "findAllBusinessOwner", type = DashboardProvider.class)
    public List<User> findAllBusinessOwner(@Param("search_query") String search_query, @Param("paging") Paging paging);

    //Find business owner
    @Select("SELECT su.user_id, su.username, su.profile_image, su.telephone, su.email, su.created_date, su.address, su.nationality\n" +
            ",sbo.company_location, sbo.company_name, sbo.company_contact\n" +
            "from shk_users su\n" +
            "inner join shk_user_role sur on su.user_id = sur.user_id\n" +
            "inner join shk_business_owners sbo on su.user_id = sbo.bo_id\n" +
            "where sur.role_id = 2 and su.user_id = #{user_id}")
    public OwnerPost findBusinessOwner(int user_id);

    //Count all filter business Owner
    @SelectProvider(method = "countAllBusinessOwner", type = DashboardProvider.class)
    public int countAllBusinessOwner(@Param("search_query") String search_query);


    //Delete business owner
    @Delete("DELETE from shk_users su where su.user_id = #{b_id}")
    public Boolean deleteBusinessOwner(int b_id);

}
