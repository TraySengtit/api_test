package org.ksga.springboot.sahakka.repository;


import io.swagger.annotations.SwaggerDefinition;
import org.apache.ibatis.annotations.*;
import org.ksga.springboot.sahakka.dto.postDto.AllProjectPostDto;
import org.ksga.springboot.sahakka.dto.postDto.UserPost;
import org.ksga.springboot.sahakka.model.category.Category;
import org.ksga.springboot.sahakka.model.post.Comment;
import org.ksga.springboot.sahakka.model.post.Post;
import org.ksga.springboot.sahakka.model.post.PostDeadline;
import org.ksga.springboot.sahakka.model.post.ProjectPost;
import org.ksga.springboot.sahakka.model.postService.ServiceCardUser;

import org.ksga.springboot.sahakka.utilities.Paging;

import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
@Repository

public interface PostRepository {

    @Select("select count(*) from shk_user_like_post where post_id =#{post_id}")
    int countLikeOfServicePost(int post_id);

    // Automatically Pending the extended deadline post
    @Select("select post_id, post_deadline   from shk_posts where post_status ='normal' ;")
    List<PostDeadline> allProjectPostDeadline();

    @Update("update shk_posts set post_status='pending' where post_id=#{post_id}")
    Boolean updatePostStatus(int post_id);

    //Report
    @Insert("insert into  shk_report_post (id, post_id, user_id, reason,created_date) VALUES (#{id},#{post_id},(select user_id from shk_users where email=#{email}),#{reason},#{created_date})")
    Boolean createReport(int id,int post_id,String email,String reason,String created_date);
    @Select("select max(id) from shk_report_post;")
    String maxReportId();


    // Filtering
     @Select("select  count(*) from shk_posts sp inner join shk_post_category spc on sp.post_id = spc.post_id inner join shk_categories sc on sc.category_id = spc.category_id  inner join shk_users su on sp.user_id = su.user_id where   UPPER(sc.category_type )  like upper('%'|| #{category_type} || '%') and UPPER(su.username)  like upper('%'|| #{username} || '%')  and sp.post_status='normal' ")
     int countPostByUsernameAndCategory(String username,String category_type);

    @Select("select  sp.* from shk_posts sp inner join shk_post_category spc on sp.post_id = spc.post_id inner join shk_categories sc on sc.category_id = spc.category_id\n" +
            " inner join shk_users su on sp.user_id = su.user_id where   UPPER(sc.category_type )  like upper('%'|| #{category_type} || '%') and UPPER(su.username)  like upper('%'|| #{username} || '%')  and sp.post_status='normal'  limit #{limit} offset #{offset}")
    @Results(

            value = {
                    @Result(property = "post_id",column = "post_id"),
                    @Result( property = "categories" ,column = "post_id",many = @Many(select = "postCategories")),
                    @Result(property = "user" ,column = "user_id",one = @One(select = "getAuthor"))
            }
    )

    List<AllProjectPostDto>findProjectPostByUsernameAndCategory(String username, String category_type , int limit, int offset);

     @Select("select  count(*) from shk_posts sp inner join shk_post_category spc on sp.post_id = spc.post_id inner join shk_categories sc on sc.category_id = spc.category_id\n" +
             "where UPPER(sc.category_type )  like upper('%'|| #{category_type} || '%') and sp.post_status='normal'")
     int countProjectPostByCategory(String category_type);
     @Select("select  sp.*,sc.category_type from shk_posts sp inner join shk_post_category spc on sp.post_id = spc.post_id inner join shk_categories sc on sc.category_id = spc.category_id\n" +
             "where    UPPER(sc.category_type )  like upper('%'|| #{category_type} || '%')   and sp.post_status='normal' limit #{limit} offset #{offset} ")
     @Results(

             value = {
                     @Result(property = "post_id",column = "post_id"),
                     @Result( property = "categories" ,column = "post_id",many = @Many(select = "postCategories")),
                     @Result(property = "user" ,column = "user_id",one = @One(select = "getAuthor"))
             }
     )
     List<AllProjectPostDto> findProjectByCategory(String category_type , int limit, int offset);


    @Select("select  count(*) from shk_posts sp inner join shk_users su on sp.user_id = su.user_id where UPPER(su.username)  like upper('%'|| #{username} || '%') and sp.post_status='normal'")
    int countProjectsByUsername(String username);
    @Select("select  sp.* from shk_posts sp inner join shk_users su on sp.user_id = su.user_id where UPPER(su.username)  like upper('%'|| #{username} || '%') and sp.post_status='normal' limit #{limit} offset #{offset}\n")
    @Results(

            value = {
                    @Result(property = "post_id",column = "post_id"),
                    @Result( property = "categories" ,column = "post_id",many = @Many(select = "postCategories")),
                    @Result(property = "user" ,column = "user_id",one = @One(select = "getAuthor"))
            }
    )
    List <AllProjectPostDto> findProjectPostByUsername(String username ,int limit, int offset);

 // normal project
 @Select("select * from shk_posts where post_status=#{post_status} and user_id =(select user_id from shk_users where email=#{email}) limit #{limit} offset #{offset}")
 @Results(

         value = {
                 @Result(property = "post_id",column = "post_id"),
                 @Result( property = "categories" ,column = "post_id",many = @Many(select = "postCategories")),
                 @Result(property = "user" ,column = "user_id",one = @One(select = "getAuthor"))
         }
 )
 List<AllProjectPostDto> findOwnerProjectPosts(String email, int limit , int offset , String post_status);

 @Select("select count(*) from shk_posts where post_status=#{post_status}  and user_id=(select user_id from shk_users where email =#{email})")
 int countOwnerProjectPosts(String email,String post_status);



 // Find all the project post of all user

 @Select("select count(*) from shk_posts where post_status='normal'")
 int countNormalProjectPosts();

    @Select("select * from shk_posts where post_status='normal' limit #{limit} offset #{offset}")
    @Results(

            value = {
                    @Result(property = "post_id",column = "post_id"),
                    @Result( property = "categories" ,column = "post_id",many = @Many(select = "postCategories")),
                    @Result(property = "user" ,column = "user_id",one = @One(select = "getAuthor"))
            }
    )

    List<AllProjectPostDto> findAllNormalProjectPosts(int limit , int offset );

    @Select("select user_id,username,profile_image ,bio  from shk_users where user_id =#{user_id}")
    @Results(
            value = {
                    @Result(property = "user_id",column = "user_id"),
                    @Result(property = "social_media" , column = "user_id" , many = @Many(select = "linksUser"))
            }

    )
    UserPost getAuthor(int user_id);

   /* @Select("select sc.category_type from shk_post_category spc inner join shk_categories sc on sc.category_id = spc.category_id where spc.post_id =#{post_id}")
    List<String> projectCategory(int post_id);

*/
   // find Post
    @Select("Select  * from shk_posts where post_id =#{post_id}")
    Post findPostById(int post_id);
    @Select("select * from shk_comments where post_id =#{post_id}") //user_id, comment_id, comment_text,created_date
    @Results(

            value = {
                    @Result(property = "comment_id" , column = "comment_id"),
                    @Result(property = "comment_text",column = "comment_text"),
                    @Result(property = "created_date",column = "created_date"),
                    @Result(property = "user",column ="user_id",one = @One(select ="commentUser"))
            }

    )
    List<Comment> allCommentOfPost(int post_id);

    @Select("select  user_id, username, profile_image from shk_users  where user_id=#{user_id}")
    @Results(
            value = {
                    @Result(property = "user_id",column = "user_id"),
                    @Result(property = "username",column = "username"),
                    @Result(property = "profile_image",column = "profile_image")
            }
    )
    ServiceCardUser commentUser(int user_id);

    @Select("select social_link from shk_user_social_media where user_id =#{user_id}")
    List<String> linksUser(int user_id);
    @Select("select sc.category_id,sc.category_type from shk_categories sc inner join shk_post_category spc\n" +
            "on sc.category_id = spc.category_id where spc.post_id =#{post_id}")
    List<Category> categoryOfPost(int post_id);

// Find the Business Owner Profile
@Select("select count(*) from shk_posts where post_status='normal' and user_id=(select user_id from shk_users where email=#{email})")
int countOwnerNormalProjectPosts(String email);


    @Select("select sp.post_id, sp.title,sp.description,sp.post_image,sp.likes,sp.created_date,sp.post_status,su.username,su.user_id,su.username,su.profile_image from shk_posts sp inner join shk_users su\n" +
            "on sp.user_id = su.user_id where sp.user_id=#{user_id} and sp.post_status='normal'")

    @Results(
            value = {
                    @Result(property = "post_id", column = "post_id"),
                    @Result(property = "categories", column = "post_id", many = @Many(select = "postCategories"))
            }
    )
   List<ProjectPost> allNormalProjectPosts(int user_id);

    @Select("select sp.post_id, sp.title,sp.description,sp.post_image,sp.likes,sp.created_date,sp.post_status,su.username,su.user_id,su.username,su.profile_image from shk_posts sp inner join shk_users su\n" +
            "on sp.user_id = su.user_id where sp.user_id=#{user_id}")

    @Results(
            value = {
                    @Result(property = "post_id", column = "post_id"),
                    @Result(property = "categories", column = "post_id", many = @Many(select = "postCategories"))
            }
    )
    List<ProjectPost> allProjectPosts(int user_id);

    @Select("select sc.category_type from shk_post_category spc inner join shk_categories sc on sc.category_id = spc.category_id where post_id=#{post_id}")
   List<String> postCategories (int post_id);




    @Insert("insert into shk_posts (post_id, title, description, user_id, post_image,  post_deadline, created_date, post_status)\n" +
            " VALUES (#{post_id},#{title},#{description},(select  user_id from shk_users where email=#{email}),#{post_image},#{post_deadline},#{created_date},#{post_status})\n")
    boolean createProjectPost(String email, int post_id, String title , String description, String post_image, String post_deadline, String created_date,String post_status);

    @Select("select max(post_id)  from shk_posts")
    String maxPostId();

    @Insert("insert into shk_post_category ( post_id, category_id) VALUES\n" +
            "(#{post_id},#{category_id});")
    boolean addCategoryToPost(int post_id, int category_id);

    @Update("update shk_posts set\n" +
            "title =#{title},description=#{description},user_id=(select user_id from shk_users where email=#{email}),post_image=#{post_image},post_deadline=#{post_deadline}, post_status=#{post_status}\n" +
            "where post_id=#{post_id}")
    boolean updateProject(int post_id, String title, String description, String email,String post_image,String post_deadline , String post_status);

    @Update("update shk_post_category set category_id=#{category_id} where post_id=#{post_id}")
    boolean updateCategoryPost(int category_id,int post_id);

    @Select("select likes from shk_posts where post_id =#{post_id}")
    String likeOfPost(int post_id );

    @Select("select user_id from shk_posts where user_id =(select user_id from shk_users where email=#{email}) AND post_id=#{post_id}\n")
    String verifyPostOwnership(String email,int post_id);

    @Delete("delete from shk_post_category where post_id=#{post_id}")
    boolean deletePostCategory(int post_id);

    @Delete("delete from shk_posts where post_id=#{post_id}")
    public boolean deletePost(int post_id);

    @Select("select count(*) from shk_posts where user_id = (select user_id from shk_users where email=#{email})")
    int countUserPost(String email);

    @Insert("insert into shk_comments (comment_id, user_id, post_id,   comment_text,created_date)\n" +
        " VALUES (#{comment_id},(select user_id from shk_users where email=#{email}),#{post_id},#{comment_text},#{created_date})")

    boolean comment(int comment_id,String email,int post_id,String comment_text,String created_date);

    @Select("select max(comment_id) from shk_comments;")
    String maxCommentId();

    @Insert("insert into shk_comments (comment_id, user_id, post_id, parent_comment_id, comment_text) VALUES\n" +
            "(#{comment_id},(select  user_id from shk_users where email=#{email}),#{post_id},#{parent_comment_id},#{comment_text})")
    boolean replyComment(int comment_id, String email , int post_id, int parent_comment_id,String comment_text);

    @Select("select count(*) from shk_posts where post_id =#{post_id}")
    int verifyPostId(int post_id);

    @Select("select count(*) from shk_comments where post_id=#{post_id};")
    int countComment(int post_id);

    @Select("select comment_id from shk_comments where comment_id =#{comment_id}")
    String verifyCommentId(int comment_id);

    @Select("select max(id) from shk_user_like_post;")
    String maxLikeId();
    // like on the post
    @Insert("  insert into shk_user_like_post (id, post_id,user_id) VALUES (#{id},#{post_id},(select user_id from shk_users  where email=#{email}))\n")
    boolean likePost(String email,int id, int post_id);
    @Insert("delete from shk_user_like_post where id=#{id} and user_id=(select user_id from shk_users where email=#{email})")
    boolean unlikePost(String email, int id);

    @Select("select id from shk_user_like_post where user_id=(select user_id from shk_users where email=#{email}) and post_id=#{post_id}")
    String reactId(String email,  int post_id);
    @Select("select count(*) from shk_user_like_post where post_id =#{post_id}")
    int countLikes(int post_id);
    @Update("update  shk_posts set likes=#{likes} where post_id=#{post_id}")
    boolean updatePostLike(int likes, int post_id);

    @Delete("delete from shk_user_like_post where post_id=#{post_id}")
    boolean deletePostLike(int post_id);

    @Delete("delete from shk_comments where post_id=#{post_id} and parent_comment_id is not null;")
    boolean deleteReplies(int post_id);

    @Delete("delete from shk_comments where post_id=#{post_id} and parent_comment_id is null;")
    boolean deleteComments(int post_id);







    @Select("select * from shk_posts")
    public List<Post>findAll(@Param("paging") Paging paging);



    @Select("select count(post_id) from shk_service_posts")
    public int countAllRecord();




    @Delete("delete from shk_posts where shk_posts.post_id = #{post_id}")
    public boolean delete(@Param("post_id")int post_id);

    @Update("update shk_posts set " +
            " post_id = #{post_id}," +
            " title = #{title}," +
            " description = #{description}," +
            " user_id = #{user_id}," +
            " post_image = #{post_image}," +
            " likes = #{likes}," +
            " comment_id = #{comment_id}," +
            " post_deadline = #{post_deadline}," +
            " created_date = #{created_date}," +
            " post_status = #{post_status} where shk_posts.post_id = #{post_id}")
    public boolean update(int post_id,
                          String title,
                          String description,
                          int user_id,
                          String post_image,
                          int likes,
                          int comment_id,
                          Date post_deadline,
                          Date created_date,
                          boolean post_status);

}
