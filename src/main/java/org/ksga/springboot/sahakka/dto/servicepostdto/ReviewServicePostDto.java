package org.ksga.springboot.sahakka.dto.servicepostdto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor

public class ReviewServicePostDto {

//ReviewResponse
    private int servicePostId;
    private String reviewText;
    private int numberOfStars;
    private String postedDate;
 //ReviewBy
    private int reviewId;
    private  int  reviewByUserId;
    private String reviewByUser; //username
    private String reviewUserProfile;





}
