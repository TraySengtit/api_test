package org.ksga.springboot.sahakka.dto.postDto;

import lombok.*;
import lombok.experimental.Accessors;
import org.ksga.springboot.sahakka.model.user.User;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class PostDto {

    private int postId;
    private String title;
    private String description;
    private String postImage;
    private int likes;
    private String  postDeadline;
    private String createDate;
    private String postStatus;
    List<String> categories;

    //
    private int userId;
    private String author;
    private String profileImage;



}
