package org.ksga.springboot.sahakka.dto.userdto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class BusinessOwnerUpdateResponse {

    private int user_id;
    private String username;

    private String profile_image;
    private String email_address;
    private String  telephone;

    //  private String created_date;
    private String bio;
    private String gender;
    private String address ;
    private String nationality ;

    List<String> social_media;

    private String company_name;
    private String company_contact;
    private String company_location ;
    private String company_description ;
    private String company_logo;
}
