package org.ksga.springboot.sahakka.dto.userdto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.ksga.springboot.sahakka.dto.postDto.AllProjectPostDto;
import org.ksga.springboot.sahakka.dto.postDto.FindPostDto;
import org.ksga.springboot.sahakka.dto.postDto.PostDto;

import org.ksga.springboot.sahakka.model.FindBusinessOwner;
import org.ksga.springboot.sahakka.model.post.Post;
import org.ksga.springboot.sahakka.model.post.ProjectPost;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class FindBusinessOwnerDto {
    boolean ownerProfile;
    List<AllProjectPostDto> projectPost;
    FindBusinessOwner user;
}
//List<FindPostDto> projectPosts;