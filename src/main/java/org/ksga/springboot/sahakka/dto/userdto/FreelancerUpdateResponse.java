package org.ksga.springboot.sahakka.dto.userdto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class FreelancerUpdateResponse {

    private  int user_id;
    private String username;
    private String profile_image;
    private String email_address;
    private String  telephone;

    //  private String created_date;
    private String bio;
    private String gender;
    private String address ;
    private String nationality ;

    List<String> social_media;



    private String education_background;
    private String work_experience;
    private String skills ;
    private String languages;
    private String achievement;
    private String national_id;
}
