package org.ksga.springboot.sahakka.dto.freelancerDto;

import lombok.*;
import lombok.experimental.Accessors;
import org.ksga.springboot.sahakka.model.user.User;

@Getter
@Setter
@ToString
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class freelancerDto {
    private int freelancer_id;
    private String educational_background;
    private String work_experience;
    private String skills;
    private String languages;
    private User user;

}

