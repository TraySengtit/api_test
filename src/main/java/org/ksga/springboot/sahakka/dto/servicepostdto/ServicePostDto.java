package org.ksga.springboot.sahakka.dto.servicepostdto;

import lombok.*;
import lombok.experimental.Accessors;
import org.ksga.springboot.sahakka.model.user.User;

import java.util.List;

@Getter
@Setter
@ToString
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor

public class ServicePostDto {

    private int id;
    private String title;
    private String post_image;
    private String post_status;
    private String post_description;
    private String author;
    List<String> categories ;


}

