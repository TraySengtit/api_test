package org.ksga.springboot.sahakka.dto.userdto;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class UserSignUpDto {

    int id;
    String username;
    String email;
    String profile_image;


}
