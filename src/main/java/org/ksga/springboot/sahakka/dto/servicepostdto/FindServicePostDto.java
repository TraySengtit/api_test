package org.ksga.springboot.sahakka.dto.servicepostdto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.ksga.springboot.sahakka.dto.postDto.UserPost;
import org.ksga.springboot.sahakka.model.postService.Category;
import org.ksga.springboot.sahakka.model.postService.Review;
import org.ksga.springboot.sahakka.model.postService.UserServicePost;
import org.ksga.springboot.sahakka.model.user.User;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class FindServicePostDto {

    private int id;
    private String title;
    private String post_image;
    private String post_status;
    private String post_description;
    private boolean service_post_owner;
    private int number_of_review;
    List<String> service_post_categories;
    private UserServicePost user;
    List<Review> servicePostReview;

}
