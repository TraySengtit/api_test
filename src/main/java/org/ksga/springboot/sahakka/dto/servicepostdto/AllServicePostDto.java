package org.ksga.springboot.sahakka.dto.servicepostdto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.ksga.springboot.sahakka.model.postService.ServiceCardUser;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AllServicePostDto {

    int id;
    int user_id;
    String post_image;
    String title;
    String post_description;
    String reviews ;
    ServiceCardUser user;
    List<String> category;

}
