package org.ksga.springboot.sahakka.dto.postDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserPost {
    int user_id;
    String username;
    String bio;
    String profile_image;
    List<String> social_media;

}
