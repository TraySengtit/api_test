package org.ksga.springboot.sahakka.dto.categoryDto;

import lombok.*;
import lombok.experimental.Accessors;
import org.ksga.springboot.sahakka.model.user.User;

import java.util.Date;

@Getter
@Setter
@ToString
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class CategoryDto {

    private int category_id;
    private String category_type;

}
