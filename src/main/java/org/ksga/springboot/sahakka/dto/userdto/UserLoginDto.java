package org.ksga.springboot.sahakka.dto.userdto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class UserLoginDto {

    String token;
    int id;
    String username;
    String email;
    String profile_image;
    String role;
    String bio;
    String skills ;

}
