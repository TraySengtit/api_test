package org.ksga.springboot.sahakka.dto.postDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.ksga.springboot.sahakka.model.post.Comment;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class FindPostDto {
    private boolean postOwner;
    private int postId;
    private String title;
    private String description;
    private String postImage;
    private String likes;
    private String  postDeadline;
    private String createDate;
    private String postStatus;

    List<String> categories;

    UserPost user;
    List<Comment> comments;

}
