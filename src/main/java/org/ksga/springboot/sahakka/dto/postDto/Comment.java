package org.ksga.springboot.sahakka.dto.postDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class Comment {

private String comment;
private String createdDate;
private UserPost user;



}
