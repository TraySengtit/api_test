package org.ksga.springboot.sahakka.dto.freelancerDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.ksga.springboot.sahakka.dto.postDto.AllProjectPostDto;
import org.ksga.springboot.sahakka.dto.servicepostdto.AllServicePostDto;
import org.ksga.springboot.sahakka.model.FindBusinessOwner;
import org.ksga.springboot.sahakka.model.user.FindFreelancer;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class FindFreelancerDto {

    boolean ownerProfile;
    List<AllServicePostDto> servicePost;
    FindFreelancer user;

}
