package org.ksga.springboot.sahakka.dto.postDto;

import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter

public class AllProjectPostDto {

    private int post_id;
    private String title;
    private String description;
    private String post_image;
    private int likes;
    private String  post_deadline;
    private String created_date;
    private String post_status;
    List<String> categories;

    UserPost user;

}
