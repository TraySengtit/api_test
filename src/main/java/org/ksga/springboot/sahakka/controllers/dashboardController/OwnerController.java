package org.ksga.springboot.sahakka.controllers.dashboardController;

import org.ksga.springboot.sahakka.model.BusinessOwner;
import org.ksga.springboot.sahakka.model.FindBusinessOwner;
import org.ksga.springboot.sahakka.model.dashboardModel.OwnerPost;
import org.ksga.springboot.sahakka.model.dashboardModel.ViewBusinessOwner;
import org.ksga.springboot.sahakka.model.post.AllReportedOfPost;
import org.ksga.springboot.sahakka.model.post.AllReportedProjectPost;
import org.ksga.springboot.sahakka.model.post.Post;
import org.ksga.springboot.sahakka.model.postService.AllReportedServicePost;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.security.UserDetailsImp;
import org.ksga.springboot.sahakka.service.UserService;
import org.ksga.springboot.sahakka.service.serviceImp.adminDashboardService.BusinessOwnerService;
import org.ksga.springboot.sahakka.service.serviceImp.adminDashboardService.DashboardService;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("/owner")
public class OwnerController {

    @Autowired
    private BusinessOwnerService businessOwnerService;

    @Autowired
    private DashboardService dashboardService;

    @Autowired
    private UserService userService ;

    //Get all posts
    @GetMapping("/allPost")
    public String allOwnerPost(@AuthenticationPrincipal UserDetailsImp user,  Model model, String search_query, Paging paging){
        long count = businessOwnerService.countPost();
        List<OwnerPost> posts = businessOwnerService.findAllPosts(search_query, paging);

        if(user !=null){
            String currentUser = user.getUsername();
            if (!user.getUsername().equalsIgnoreCase("anonymousUser") )
            {
                User loginUser = userService.findUserByEmail(currentUser);
                model.addAttribute("loginUser",loginUser);
            }
        }

        model.addAttribute("message",model.asMap().get("message"));
        model.addAttribute("countPost", count);
        model.addAttribute("posts", posts);
        model.addAttribute("search_query", search_query);
        return "business_owner/allOwnerPosts";
    }

//     Delete All posts
    @GetMapping("/{post_id}/delete")
    public String deletePost(@AuthenticationPrincipal UserDetailsImp user, String password,  @PathVariable int post_id ,RedirectAttributes redirectAttributes){
        System.out.println("Here is the post id : "+post_id);

        String message = "";
       if (user!=null){
           String currentUser = user.getUsername();
           if (!currentUser.equals("anonymousUser")){
               BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
               if (encoder.matches(password,user.getPassword())){
                   boolean post =businessOwnerService.deletePost(post_id);
                   System.out.println("Successfully removed the project post");
                    message = "Successfully removed the project post! ";


               }else {
                   System.out.println("Wrong credential. Failed to remove.");
                   message ="Wrong Credential!. Failed to remove the post.";
               }
               redirectAttributes.addFlashAttribute("message",message);
               return "redirect:/owner/allPost";


           }else{

               System.out.println("Authentication Failed.Try login again.");
               message = "Authentication Failed. Try login again. ";
               redirectAttributes.addFlashAttribute("message",message);
               return "redirect:/logout";
           }


       }else{
           System.out.println("Authentication Failed.Try login again.");
           message = "Authentication Failed. Try login again. ";
           redirectAttributes.addFlashAttribute("message",message);
           return "redirect:/logout";
       }


    }

//    view post
    @GetMapping("/{post_id}/view_post")
    public String viewOwnerPost(@AuthenticationPrincipal UserDetailsImp user ,  Model model, @PathVariable int post_id) {
        OwnerPost ownerPost  = businessOwnerService.findById(post_id);
        User findUser = userService.findUserByEmail(ownerPost.getEmail());
        if (findUser!=null)
                    ownerPost.setUser_id(findUser.getUser_id());

        if (user !=null){
            String currentUser = user.getUsername();
            if (!user.getUsername().equalsIgnoreCase("anonymousUser") )
            {
                User loginUser = userService.findUserByEmail(currentUser);
                model.addAttribute("loginUser",loginUser);
            }
        }


        model.addAttribute("ownerPost", ownerPost);
        return "business_owner/viewProjectPost";
    }

    //All busniness Owner
    @GetMapping("/allOwner")
    public String allOwnerController(@AuthenticationPrincipal UserDetailsImp user,  Model model, String search_query, Paging paging){
        Long countOwner = dashboardService.CountOwner();
        List<User> allOwners = businessOwnerService.findAllBusinessOwner(search_query, paging);
        if (user !=null){
            String currentUser = user.getUsername();
            if (!user.getUsername().equalsIgnoreCase("anonymousUser") )
            {
                User loginUser = userService.findUserByEmail(currentUser);
                model.addAttribute("loginUser",loginUser);
            }
        }

        model.addAttribute("message",model.asMap().get("message"));
        model.addAttribute("countOwner", countOwner);
        model.addAttribute("allOwners", allOwners);
        model.addAttribute("search_query", search_query);
        return "business_owner/allBusinessOwner";
    }

    @GetMapping("/{b_id}/delete_owner")
    public String deleteOwner(@AuthenticationPrincipal UserDetailsImp user, @PathVariable int b_id, String password , RedirectAttributes redirectAttributes){

        String message = "";
        if (user!=null){

            String currentUser = user.getUsername();
            if (!currentUser.equalsIgnoreCase("anonymousUser") )
            {
                BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
                if (encoder.matches(password,user.getPassword())){
                    Boolean deleteOwner = businessOwnerService.deleteBusinessOwner(b_id);

                    if (deleteOwner){
                        System.out.println("This user has been deleted successfully!");
                        message="This user has been deleted successfully!";
                        redirectAttributes.addFlashAttribute("message", message);
                        return "redirect:/owner/allOwner";
                    }else
                    {
                        message="Failed to deactivate this account.";
                        System.out.println("failed to deactivate this account.");
                        redirectAttributes.addFlashAttribute("message", message);
                        return "redirect:/owner/allOwner";
                    }

                }else {

                    message="Wrong password! Cannot deactivate this account";
                    System.out.println("Wrong password ! Cannot Deactivate this user.");
                    redirectAttributes.addFlashAttribute("message", message);
                    return "redirect:/owner/allOwner";
                }


            }else {
                System.out.println("Logout since the currentUser is not valid");
                message="Authentication Failed!";
                redirectAttributes.addFlashAttribute("message", message);
                return "redirect:/logout";

            }



        }else  return "redirect:/logout";

    }

    //View business owner
    @GetMapping("/{user_id}/view")
    public String viewBusinessOwner(@AuthenticationPrincipal UserDetailsImp user,   Model model, @PathVariable int user_id){
        //OwnerPost businessOwner = businessOwnerService.findBusinessownerByid(user_id);

        ViewBusinessOwner businessOwner = new ViewBusinessOwner();

        User buUser = userService.findUserById(user_id);
        BusinessOwner boUser= userService.businessUser(buUser.getUser_id());
        businessOwner.setObject(buUser,boUser);



         businessOwner.toString();

        if (user !=null){
            String currentUser = user.getUsername();
            if (!user.getUsername().equalsIgnoreCase("anonymousUser") )
            {
                User loginUser = userService.findUserByEmail(currentUser);
                model.addAttribute("loginUser",loginUser);
            }
        }

        model.addAttribute("businessOwner", businessOwner);
        return "business_owner/viewBusinessOwnerProfile";
    }


    @GetMapping("/allReport")
    public String allReportedProjectPost(@AuthenticationPrincipal UserDetailsImp user,Model model ,String search_query,Paging paging ){

     //   List<OwnerPost> posts = businessOwnerService.findAllPosts(search_query, paging);

        List<AllReportedProjectPost> allReportedProjectPosts=businessOwnerService.findAllReportedProjectPosts(search_query,paging);
      int count = (int) businessOwnerService.countReportedPost();
        paging.setTotalCount(count);

        /// Check the value of the post id :

        if(user !=null){
            String currentUser = user.getUsername();
            if (!user.getUsername().equalsIgnoreCase("anonymousUser") )
            {
                User loginUser = userService.findUserByEmail(currentUser);
                model.addAttribute("loginUser",loginUser);
            }
        }

        model.addAttribute("message",model.asMap().get("message"));
        model.addAttribute("countPost", count);
        model.addAttribute("posts", allReportedProjectPosts);
        model.addAttribute("search_query", search_query);

        return "/business_owner/allReportedProjectPost";
    }


    @GetMapping("/{id}/viewReportedPost")
    public String viewReportedProjectPost(@AuthenticationPrincipal UserDetailsImp user,   @PathVariable int id,Model model ){
        OwnerPost ownerPost  = businessOwnerService.findById(id);
        User findUser = userService.findUserByEmail(ownerPost.getEmail());
        if (findUser!=null)
            ownerPost.setUser_id(findUser.getUser_id());
        List<AllReportedOfPost> reportsOfPost = businessOwnerService.allReportOfProjectPosts(id);

        if (user !=null){
            String currentUser = user.getUsername();
            if (!user.getUsername().equalsIgnoreCase("anonymousUser") )
            {
                User loginUser = userService.findUserByEmail(currentUser);
                model.addAttribute("loginUser",loginUser);
            }
        }
        model.addAttribute("message",model.asMap().get("message"));
        model.addAttribute("reports",reportsOfPost);
        model.addAttribute("ownerPost", ownerPost);
        return "business_owner/viewReportedProject";


    }


    @GetMapping("/{id}/deleteReport")
    public String deleteReport(@AuthenticationPrincipal UserDetailsImp user,String password,String postId,  @PathVariable int id,RedirectAttributes redirectAttributes ){

        String message = "";

        if (user!=null){
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder( );
            if (encoder.matches(password, user.getPassword())){
                // Perform deletion
                Boolean    isReportDeleted =businessOwnerService.deleteReport(id);
               message=  isReportDeleted? "Report has been deleted successfully!" : "Failed to delete the report. Exception occurs.";
            }else {
                message="Wrong Credential!.Failed to delete the report.";
            }
            redirectAttributes.addFlashAttribute("message",message);
            return "redirect:/owner/"+postId+"/viewReportedPost" ;

        }else {

            return "redirect:/logout";
        }
    }


}
