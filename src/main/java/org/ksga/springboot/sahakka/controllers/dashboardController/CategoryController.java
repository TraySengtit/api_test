package org.ksga.springboot.sahakka.controllers.dashboardController;

import org.ksga.springboot.sahakka.model.category.Category;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.security.UserDetailsImp;
import org.ksga.springboot.sahakka.service.UserService;
import org.ksga.springboot.sahakka.service.serviceImp.adminDashboardService.CategoryService;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.parameters.P;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService  categoryService;

    @Autowired
    org.ksga.springboot.sahakka.service.CategoryService categoryServices;

    // added code
    @Autowired
    private UserService userService;
    //Get all category
    @GetMapping("/allCategory")
    public String allCategory(@AuthenticationPrincipal UserDetailsImp user, Model model, String search_query, Paging paging){
        List <Category> category= categoryService.findAllCategoryFilter(search_query, paging);
        Long countCategory = categoryService.CountAllCategory();

        // Added code to get the user login
        if (user !=null){
            String currentUser = user.getUsername();
            if (!user.getUsername().equalsIgnoreCase("anonymousUser") )
            {
                User loginUser = userService.findUserByEmail(currentUser);
                model.addAttribute("loginUser",loginUser);
            }
        }

        model.addAttribute("message",model.asMap().get("message"));
        model.addAttribute("category", category);
        model.addAttribute("search_query", search_query);
        model.addAttribute("countCategory", countCategory);
//        model.addAttribute("category1", new Category());
//        model.addAttribute("findOneCategory", categoryService.findOneCategoryById(category_id));
        return "categories/allCategory";
    }

    @GetMapping("/postCategory")
    public String PostCategory( @AuthenticationPrincipal UserDetailsImp user, String category,String password,RedirectAttributes redirectAttributes){
        /*categoryService.addCategory(category);*/
String message = "";
if (user!=null){
    User author = userService.findUserByEmail(user.getUsername());
    if (author==null) return "redirect:/logout";
    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    if (encoder.matches(password,author.getPasswords())){

        int countExistedCategories = categoryServices.countCategoriesByType(category);
        if (countExistedCategories>0){

            message ="'"+ " "+category + " ' is already exist.Cannot create duplicate value ";
            redirectAttributes.addFlashAttribute("message",message);
            return "redirect:/category/allCategory";
        }

        int availableId =0;
        String maxCateID = categoryServices.maxCategory();
        if (maxCateID!=null) availableId = Integer.parseInt(maxCateID);

        Boolean newCategoryCreated = categoryServices.addNewCategory(availableId+1 , category);


        // check to see if the the category is already existed
        message=category + " has been successfully added ";


    }else {
        message="Wrong Credential! Failed to add new category";

    }
    redirectAttributes.addFlashAttribute("message",message);
    return "redirect:/category/allCategory";
}else {

    System.out.println("Authentication Failed. Try login again.");
    message = "Authentication Failed. Try to login again";
    redirectAttributes.addFlashAttribute("message",message);
    return "redirect:/logout";
}


    }

  @GetMapping("/{id}/updateCategory")
    public String updateCategoryById(@AuthenticationPrincipal UserDetailsImp user, String category,   @PathVariable int id , String password, RedirectAttributes redirectAttributes)
    {  String message = "";

        if (user!=null){
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder( );
            if (encoder.matches(password,user.getPassword())){
                // Perform update
                Boolean isCategoryUpdated =categoryService.update(id,category);
                message= isCategoryUpdated?  "Update the category successfully!" : "Failed to update the category.SQL Exception";
            }else {

                message="Wrong Credential! Failed to update this category.";

            }
            redirectAttributes.addFlashAttribute("message",message);
            return "redirect:/category/allCategory";
        }else {

            System.out.println("Authentication Error !. Try Login in again.");
            message = "Authentication Failed ! Try to login again.";
            return "redirect:/logout";
        }


    }

    //Get all Suggest category
    @GetMapping("/allSuggestion")
    public String allSuggestion(@AuthenticationPrincipal UserDetailsImp user,  Model model, String search_query, Paging paging){


        List<Category> categorySuggest = categoryService.findAllSuggestCategoryFilter(search_query, paging);
        Long countCategorySuggest = categoryService.CountAllSuggest();

       /* if  (categorySuggest.size()==0){
                 Category testing = new Category();

        testing.setCategory_type("Nothing to show");

         categorySuggest.add(testing);

        }*/

        System.out.println("Here is the size of the category suggestion: "+categorySuggest.size());
        if (user !=null){
            String currentUser = user.getUsername();
            if (!user.getUsername().equalsIgnoreCase("anonymousUser") )
            {
                User loginUser = userService.findUserByEmail(currentUser);
                model.addAttribute("loginUser",loginUser);
            }
        }

        model.addAttribute("message",model.asMap().get("message"));
        model.addAttribute("categorySuggest", categorySuggest);
        model.addAttribute("countCategorySuggest", countCategorySuggest);
        return "categories/allSuggestion";
    }




    //Delete category by id
    @GetMapping("/{category_id}/deleteCategory")
    public String deleteCategoryById(@AuthenticationPrincipal UserDetailsImp user, String password , RedirectAttributes redirectAttributes, @PathVariable int category_id){

       String message = "";
       if (user!=null){
           BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
           if (encoder.matches(password,user.getPassword())){
               Boolean deleteCategory = categoryService.deleteCategoryById(category_id);

               if (deleteCategory)
               {
                   System.out.println("Delete the category successfully!");
                   message="Delete the category successfully!";
               }else {

                   System.out.println("Failed to delete the category. SQL Exception");
                   message="Failed to delete the category. SQL Exception.";
               }
            redirectAttributes.addFlashAttribute("message",message);
             return "redirect:/category/allCategory";

           }else
           {
               System.out.println("Wrong Credential! Failed to delete the category");
               message="Wrong Credential! Failed to delete the category.";
               redirectAttributes.addFlashAttribute("message",message);
               return "redirect:/category/allCategory";

           }


       }else {
           System.out.println("Authentication Failed. Try to login again.");
           message="Authentication Failed! Try log in again.";
           redirectAttributes.addFlashAttribute("message",message) ;
           return "redirect:/logout";
       }
    }

    @GetMapping("/{category_id}/delete_suggestion")
    public String deleteCategorySuggestById(@AuthenticationPrincipal UserDetailsImp user,String password, RedirectAttributes redirectAttributes,   @PathVariable int category_id){
        String message = "";
        if (user!=null){
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder( );

            if (encoder.matches(password,user.getPassword())){
                Boolean deleteCategorySuggestion = categoryService.deleteCategorySuggestById(category_id);
                if (deleteCategorySuggestion)
                    message="You have successfully removed this suggestion!";
               else message= "Failed to removed this suggestion.Exception occurs.";
                    redirectAttributes.addFlashAttribute("message",message);

                return "redirect:/category/allSuggestion";
            }else {

                message = "Wrong Credential! Failed to remove this suggestion.";
                redirectAttributes.addFlashAttribute("message",message) ;

                return "redirect:/category/allSuggestion";
            }


        }else {
            message ="Authentication Failed. Try login again.";
            redirectAttributes.addFlashAttribute("message",message);
            return  "redirect:/logout";

        }

    }

     //Add suggested category
    @GetMapping("/{id}/addSuggestedCategory")
     public String addSuggestedCategory(@AuthenticationPrincipal UserDetailsImp user, @PathVariable int id, String password,String category,RedirectAttributes redirectAttributes)
     {
         String message = "";
         if (user!=null){
             BCryptPasswordEncoder encoder = new BCryptPasswordEncoder( );
             if (encoder.matches(password,user.getPassword())){
                 int countExistedCategory = categoryServices.countCategoriesByType(category);
                 if (countExistedCategory>0){
                     message ="' "+category+" 'is already exist.Cannot add duplicate category.";
                     redirectAttributes.addFlashAttribute("message",message);
                     return "redirect:/category/allSuggestion";
                 }
                 boolean isSuggestionAdded = categoryServices.suggestedCategoryUpdate(id);
                message = isSuggestionAdded? "Successfully added "+ "' "+category+ " ' to the category": "Failed to added this suggestion. Exception occurs";
                redirectAttributes.addFlashAttribute("message",message);
                 return "redirect:/category/allSuggestion" ;
                 //return "redirect:/category/allCategory";
             }else {
                 message = "Wrong Credential! Failed to add this suggestion.";
                 redirectAttributes.addFlashAttribute("message",message);
                 return "redirect:/category/allSuggestion" ;
             }

         }else {
               message="Authentication Failed! Try to login again.";
               redirectAttributes.addFlashAttribute("message",message);
               return "redirect:/logout";
         }
     }








}
