package org.ksga.springboot.sahakka.controllers.dashboardController;

import org.ksga.springboot.sahakka.model.dashboardModel.FreelancerPost;
import org.ksga.springboot.sahakka.model.dashboardModel.OwnerPost;
import org.ksga.springboot.sahakka.model.dashboardModel.ReportedServicePost;
import org.ksga.springboot.sahakka.model.postService.AllReportedServicePost;
import org.ksga.springboot.sahakka.model.postService.ServicePost;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.security.UserDetailsImp;
import org.ksga.springboot.sahakka.service.ServicePostService;
import org.ksga.springboot.sahakka.service.UserService;
import org.ksga.springboot.sahakka.service.serviceImp.adminDashboardService.DashboardService;
import org.ksga.springboot.sahakka.service.serviceImp.adminDashboardService.FreelancerService;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
@RequestMapping("/freelancer")
public class FreelancerController {

    @Autowired
    private FreelancerService freelancerService;
    @Autowired
    private DashboardService dashboardService;

    @Autowired
    ServicePostService servicePost;

    @Autowired
    UserService userService;


    @GetMapping("/allReportedServicePosts")
    public String allReportedCard(@AuthenticationPrincipal UserDetailsImp user, Model model,String search_query,Paging paging){

        List<ReportedServicePost> allReportedPosts= freelancerService.getAllReportedServicePost(search_query,paging);
        int countReportedPost = (int) freelancerService.countReportedCard();
         paging.setTotalCount(countReportedPost);



        if(user!=null){
            String currentUser = user.getUsername();
            if (!user.getUsername().equalsIgnoreCase("anonymousUser") )
            {
                User loginUser = userService.findUserByEmail(currentUser);
                model.addAttribute("loginUser",loginUser);
            }
        }

        model.addAttribute("message",model.asMap().get("message"));
        model.addAttribute("allReportedPosts", allReportedPosts);
        model.addAttribute("countFreelancer", countReportedPost);

        return "freelancer/allReportedService";
    }

    //Show all Freelancer info.
    @GetMapping("/allLancer")
    public String freelancerDashboardController(@AuthenticationPrincipal UserDetailsImp user, Model model, String search_query, Paging paging){
        List <User> freelancer = freelancerService.findAllFreelancer(search_query, paging);
        Long countFreelancer = dashboardService.CountFreelancer();

        // Added code to get the user login
        if (user !=null){
            String currentUser = user.getUsername();
            if (!user.getUsername().equalsIgnoreCase("anonymousUser") )
            {
                User loginUser = userService.findUserByEmail(currentUser);
                model.addAttribute("loginUser",loginUser);
            }
        }

        model.addAttribute("message",model.asMap().get("message"));
        model.addAttribute("allFreelancer", freelancer);
        model.addAttribute("countFreelancer", countFreelancer);

        return "freelancer/allFreelancers";
    }

    //view freelancer onfo.
    @GetMapping("/{user_id}/view")
    public String viewFreelancerProfile(@AuthenticationPrincipal UserDetailsImp user, Model model, @PathVariable int user_id){
        FreelancerPost freelancerPost = freelancerService.findFreelancerById(user_id);
        if (user!= null){

            String currentUser = user.getUsername();
            if (!currentUser.equals("anonymousUser")){
                User loginUser = userService.findUserByEmail(currentUser);
                model.addAttribute("loginUser",loginUser);

            }else return "redirect:/logout";
        }else return "redirect:/logout";

        model.addAttribute("freelancer", freelancerPost);
        return "freelancer/viewFreelancerProfile";
    }

    //Block Freelancer
    @GetMapping("/{id}/delete_freelancer")
    public String deleteFreelancerById (@AuthenticationPrincipal UserDetailsImp user, String password, @PathVariable int  id , RedirectAttributes redirectAttributes){
        String message = "";
        if (user!=null){
            String currentUser = user.getUsername();
            if (!currentUser.equals("anonymousUser")){
                BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
                if (encoder.matches(password,user.getPassword()))
                {
                   /* System.out.println("Here is the value of the password : "+ password);
                    System.out.println( "Here is the value of the id: " + id);*/
                    Boolean deleteFreelancer =  freelancerService.deleteFreelancerById(id);
                    System.out.println("Successfully deleted the freelancer.");
                    message="Successfully deleted the freelancer.";
                    redirectAttributes.addFlashAttribute("message",message);
                    return "redirect:/freelancer/allLancer";

                }else {

                    System.out.println("Wrong password!.Failed to delete the freelancer account.");
                    message="Wrong credential!. Failed to detete the freelancer account.";
                    redirectAttributes.addFlashAttribute("message",message);

                    return  "redirect:/freelancer/allLancer";
                }


            }else

            {
                System.out.println("Authentication Failed! Try login again.");
                message = "Authentication Failed! Try login again" ;
                redirectAttributes.addFlashAttribute("message",message);
                return "redirect:/logout";}

        }else {
            System.out.println("Authentication Failed! Try login again.");
            message = "Authentication Failed! Try login again" ;
            redirectAttributes.addFlashAttribute("message",message);
            return "redirect:/logout";
        }


    }

    @GetMapping("/{id}/deleteServicePost")
    public String deleteServicePost(@AuthenticationPrincipal UserDetailsImp user, String password,@PathVariable int id, RedirectAttributes redirectAttributes){

        String message = "";
        if (user!=null){

            if (!user.getUsername().equals("anonymousUser")){
                BCryptPasswordEncoder encoder = new BCryptPasswordEncoder( );
                if (encoder.matches(password,user.getPassword())){
                    //perform delete
                 Boolean isServicePostDeleted = servicePost.delete(id);

               message =  isServicePostDeleted? "Successfully delete the service post.": "Failed to delete the service post. Exception occurs";
               redirectAttributes.addFlashAttribute("message",message);

             //  return new RedirectView("allPost");
          //    return  "";
                      return "redirect:/freelancer/allPost";

                }else {
                    System.out.println("Wrong Credential ");
                    message="Wrong Credential! Failed to delete the service post.";
                    redirectAttributes.addFlashAttribute("message",message);
                   return  "redirect:/freelancer/allPost";

                }

            }else {
                System.out.println("Authentication Failed. Try login again.");
                message="Authentication Failed. Try login again.";
                redirectAttributes.addFlashAttribute("message",message);
                return "redirect:/logout" ;
            }
        }else return "redirect:/logout";


    }
    //All freelancer posts
    @GetMapping("/allPost")
    public String allServicePosts(@AuthenticationPrincipal UserDetailsImp user,  Model model, String search_query, Paging paging){
        List <FreelancerPost> freelancerPosts = freelancerService.findFreelancerFilterPost(search_query, paging);
        Long countPost = freelancerService.countPost();
       // Added code to get the user login
        if(user !=null){
            String currentUser = user.getUsername();
            if (!user.getUsername().equalsIgnoreCase("anonymousUser") )
            {
                User loginUser = userService.findUserByEmail(currentUser);
                model.addAttribute("loginUser",loginUser);
            }
        }

        model.addAttribute("message",model.asMap().get("message"));
        model.addAttribute("freelancerPosts",freelancerPosts);
        model.addAttribute("search_query", search_query);
        model.addAttribute("countPost", countPost);
        return "freelancer/lancerPost";
    }

    //View Freelancer Posts
    @GetMapping("/{id}/view_post")
    public String viewFreelancerPost(@AuthenticationPrincipal UserDetailsImp user,  Model model, @PathVariable int id  ) {

        FreelancerPost freelancerPosts  = freelancerService.findFreelancerByID(id);
        if (user !=null){
            String currentUser = user.getUsername();
            if (!user.getUsername().equalsIgnoreCase("anonymousUser") )
            {
                User loginUser = userService.findUserByEmail(currentUser);
                model.addAttribute("loginUser",loginUser);
            }
        }
        int ownerId = userService.findIdByUserEmail(freelancerPosts.getEmail());
        freelancerPosts.setUser_id(ownerId);

        model.addAttribute("freelancerPosts", freelancerPosts);
        return "freelancer/viewFreelancerPost";
    }

    @GetMapping("{id}/viewReportedServicePost")
    public String viewReportedServicePost(@AuthenticationPrincipal UserDetailsImp user, Model model,@PathVariable int id ){
        FreelancerPost freelancerPosts  = freelancerService.findFreelancerByID(id);
        int ownerId = userService.findIdByUserEmail(freelancerPosts.getEmail());
        freelancerPosts.setUser_id(ownerId);
        // Find reported reason
        List<AllReportedServicePost> reportOfServicePost = freelancerService.allReportOfServicePost(id);

        /*System.out.println("Here is the value of username : "+reportOfServicePost.get(1).getUser().getUsername());*/

        if (user!=null){
            User authorUser = userService.findUserByEmail(user.getUsername());
            model.addAttribute("loginUser",authorUser);
        }
        model.addAttribute("message",model.asMap().get("message"));
        model.addAttribute("freelancerPosts", freelancerPosts);
        model.addAttribute("reports",reportOfServicePost);

        return "freelancer/viewReportServicePost";

    }

    @GetMapping("{id}/deleteReports")
    public String deleteReport(@AuthenticationPrincipal UserDetailsImp user,String password, @PathVariable int id, RedirectAttributes redirectAttributes ,int servicePostId){
        String message ="";

        if (user!=null){
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            if (encoder.matches(password,user.getPassword())){
                Boolean isReportDeleted = freelancerService.deleteServiceReport(id);
                message= isReportDeleted? "Report has been deleted successfully!": "Failed to delete this report.Exception occurs." ;

            }else {
                message="Wrong Credential.Failed to remove the report.";
            }


            redirectAttributes.addFlashAttribute("message",message);
            return "redirect:/freelancer/"+servicePostId +"/viewReportedServicePost";
        }else {
            message="Authentication Failed.Try to login again.";
            redirectAttributes.addFlashAttribute("message",message);
            return "redirect:/logout";
        }

    }

}
