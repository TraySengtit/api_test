package org.ksga.springboot.sahakka.controllers.restcontroller;

import org.ksga.springboot.sahakka.dto.userdto.UserLoginDto;
import org.ksga.springboot.sahakka.dto.userdto.UserSignUpDto;
import org.ksga.springboot.sahakka.model.BusinessOwner;
import org.ksga.springboot.sahakka.model.Freelancer;

import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.model.user.UserRole;
import org.ksga.springboot.sahakka.payload.request.userRequest.BusinessOwnerSignUpRequest;
import org.ksga.springboot.sahakka.payload.request.userRequest.FreelancerSignUpRequest;
import org.ksga.springboot.sahakka.payload.request.userRequest.UserLoginRequest;
import org.ksga.springboot.sahakka.payload.response.Response;

import org.ksga.springboot.sahakka.security.jwt.JwtUtils;
import org.ksga.springboot.sahakka.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController

@RequestMapping("api/v1/auth")
public class AuthRestController {
    public UserService userService;

    @Autowired
    public AuthenticationManager authenticationManager;
    @Autowired
    public AuthRestController (UserService userService){
        this.userService = userService;
   }
    enum Provider{
        FACEBOOK,GOOGLE,NORMAL
    }
    // SignUp first Version
    @PostMapping("/signup/freelancer")
    public Response<UserSignUpDto> freelancerSignUp(@RequestBody FreelancerSignUpRequest request ,@RequestParam(defaultValue = "NORMAL") Provider providerType){

        try{
            boolean emailCondition = false;

            // Check to see if the email is already in used
            if( request != null){
                // 1. check to see if the email is existed
                // 2. get the available id from the database
                // 3.create freelancer user

                String existedEmail = userService.existedEmail(request.getEmail());

                System.out.println(existedEmail);
                    if (existedEmail==null){
                            // Should I use another try catch
                        try{

                            //Find available ID :
                            List<User> allUser = userService.allUsers();
                                User userMaxId = new User();

                                try{
                                    userMaxId = allUser.stream()
                                            .max(Comparator.comparing(User::getUser_id))
                                            .orElseThrow(NoSuchElementException::new);
                                }catch (Exception ex){
                                    System.out.println("My Algorithms Sucks : " + ex.getMessage());
                                }
                                int id = userMaxId.getUser_id()+1;
                            //System.out.println("Available Id for user is : "+ id);

                            Date date = Calendar.getInstance().getTime();
                            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            String currentDate = dateFormat.format(date);


                            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
                            //googlePa$$w0rd
                            if (providerType.equals(Provider.GOOGLE)){
                                request.setPasswords("googlePa$$w0rd");
                            }else if (providerType.equals(Provider.FACEBOOK)){
                                request.setPasswords("facebookPa$$w0rd");
                            }else {
                                request.setPasswords(request.getPasswords());
                            }
                            boolean isUserCreated = userService.addNewUser(
                                    id,
                                    request.getUsername(),
                                    request.getEmail(),
                                    encoder.encode(request.getPasswords()),
                                    providerType.toString(),
                                    false,
                                    request.getProfile_image(),
                                    request.getEmail_address(),
                                    request.getTelephone(),

                                     currentDate,
                                    request.getBio(),
                                    request.getGender()
                                    ,request.getAddress(),request.getNationality()
                            );
                            // adding skills into the table

                           // System.out.println("------ All social Media ");
                            List<String> allLinks = request.getSocial_media();
                            allLinks.stream()
                                    .map(String::valueOf)
                                    .forEach(System.out::println);

                            //add skill to the table

                            String maxUserSocialMedia = userService.maxUserSocialMedia();
                            int availableUserSocialMedia=0 ;
                            if (maxUserSocialMedia!=null) availableUserSocialMedia=Integer.valueOf(maxUserSocialMedia);

                            if (allLinks.size()>0)
                            for(int i=0; i<allLinks.size(); i++){

                            userService.addSkills(request.getEmail(),availableUserSocialMedia+i+1, allLinks.get(i));

                            }
                      boolean isFreelancerCreated =false ,isRoleToUser= false;
                            if(isUserCreated){


                                isFreelancerCreated = userService.addNewUserAsFreelancer(
                                                id,
                                                request.getEducation_background(),
                                                request.getWork_experience(),
                                                request.getSkills(),
                                                request.getLanguages(),
                                                request.getAchievement(),
                                           request.getNational_id()
                                        );
                                        //Get the available id from the table user role
                                        UserRole userRoleMaxId= new UserRole();
                                        List<UserRole> allUserRoles = userService.allUserRoles();
                                        try{
                                            userRoleMaxId = allUserRoles.stream()
                                                    .max(Comparator.comparing(UserRole::getId))
                                                    .orElseThrow(NoSuchElementException::new);
                                        }catch (Exception ex){
                                            System.out.println("My Algorithms (User Role) Sucks : " + ex.getMessage());
                                        }
                                        int userRoleId = userRoleMaxId.getId() + 1;

                                        // 1 = Freelancer and 2 = Business Owner
                                        // Adding Role For User
                                         isRoleToUser = userService.addRoleToUser(userRoleId,id,1);
                                         //isRoleToUser , isFreelancerCreated
                            }
                        if(isUserCreated && isRoleToUser && isFreelancerCreated){
                            // Response back to the user
                            UserSignUpDto response = new UserSignUpDto();
                            response.setId(id);
                            response.setUsername(request.getUsername());
                            response.setEmail(request.getEmail());
                            response.setProfile_image(request.getProfile_image());

                            return Response.<UserSignUpDto>ok().setPayload(response);
                        }else {
                            return Response.<UserSignUpDto>exception().setError("SignUp Failed ... Exception occurs.");
                        }
                        }catch (Exception ex){
                            System.out.println("Get AllUser SQL Exception : ");
                            return Response.<UserSignUpDto>exception().setError(ex.getMessage());
                        }
                    }
                    else
                    {
                     return Response.<UserSignUpDto>badRequest().setError("This is email is already exist. Try using different one ....");
                    }
            } else
            return Response.<UserSignUpDto>badRequest().setError("Field Cannot be empty...");
     }catch (Exception ex){

            System.out.println("SQL Exception Occur: ");
            System.out.println(ex.getMessage());

            return Response.<UserSignUpDto>badRequest().setError("Failed to SignUp .... ");
        }
    }
    @PostMapping( "/signup/businessOwner")
    public Response<UserSignUpDto> businessOwnerSignUp (@RequestBody BusinessOwnerSignUpRequest request ,@RequestParam(defaultValue = "NORMAL") Provider providerType){

        try{
            boolean emailCondition = false;

            // Check to see if the email is already in used
            if( request != null){
                String existedEmail = userService.existedEmail(request.getEmail());
                System.out.println(existedEmail);
                if (existedEmail==null){
                    // Should I use another try catch
                    try{
                        //Find available ID :
                        List<User> allUsers = userService.allUsers();
                        User userMax = new User();
                        try{
                            userMax= allUsers.stream()
                                    .max(Comparator.comparing(User::getUser_id))
                                    .orElseThrow(NoSuchElementException::new);
                        }catch (Exception ex){
                            System.out.println("SQL Get available ID for User Exception : " + ex.getMessage());
                        }
                        int availableId = userMax.getUser_id()+1;


                        Date date = Calendar.getInstance().getTime();
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        String currentDate = dateFormat.format(date);

                        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//googlePa$$w0rd

                        if (providerType.equals(Provider.GOOGLE)){
                            request.setPasswords("googlePa$$w0rd");

                        }else if (providerType.equals(Provider.FACEBOOK)){
                            request.setPasswords("facebookPa$$w0rd");

                        }else {

                            request.setPasswords(request.getPasswords());
                        }

         boolean isUserCreated =   userService.addNewUser(
                               availableId,
                                request.getUsername(),
                                request.getEmail(),
                                encoder.encode(request.getPasswords()) ,
                                providerType.toString(),
                                false,
                                request.getProfile_image(),
                                request.getEmail_address(),
                                request.getTelephone(),
                                currentDate,
                                request.getBio(),
                                request.getGender(),
                                request.getAddress(),
                                request.getNationality()
                        );
                        boolean isBusinessCreated =false ,isRoleToUser= false;
                        if(isUserCreated){

                            isBusinessCreated = userService.addNewUserAsBusinessOwner(
                                    availableId,
                                    request.getCompany_name(),
                                    request.getCompany_contact(),
                                    request.getCompany_location(),
                                    request.getCompany_description(),
                                    request.getCompany_logo()

                            );

                            //add link to the table
                            List<String> allLinks = request.getSocial_media();
                            String maxUserSocialMedia = userService.maxUserSocialMedia();
                            int availableUserSocialMedia=0 ;
                            if (maxUserSocialMedia!=null) availableUserSocialMedia=Integer.valueOf(maxUserSocialMedia);

                            if (allLinks.size()>0)
                                for(int i=0; i<allLinks.size(); i++){

                                    userService.addSkills(request.getEmail(),availableUserSocialMedia+i+1, allLinks.get(i));

                                }

                           //Get the available id from the table user role
                            UserRole userRoleMax= new UserRole();
                            List<UserRole> allUserRoles = userService.allUserRoles();
                            try{
                                userRoleMax = allUserRoles.stream()
                                        .max(Comparator.comparing(UserRole::getId))
                                        .orElseThrow(NoSuchElementException::new);
                            }catch (Exception ex){
                                System.out.println("UserMaxRole Exception  : " + ex.getMessage());
                            }
                            int userRoleId = userRoleMax.getId() + 1;

                            // 1 = Freelancer and 2 = Business Owner
                            // Adding Role For User
                            isRoleToUser = userService.addRoleToUser(userRoleId,availableId,2);

                            //isRoleToUser , isFreelancerCreated


                        }

                        if(isUserCreated && isBusinessCreated &&isRoleToUser){
                            // Response back to the user
                            UserSignUpDto response = new UserSignUpDto();
                            response.setId(availableId);
                            response.setUsername(request.getUsername());
                            response.setEmail(request.getEmail());
                            response.setProfile_image(request.getProfile_image());

                            return Response.<UserSignUpDto>ok().setPayload(response);
                        }else {
                            return Response.<UserSignUpDto>exception().setError("SignUp Failed ... Exception occurs.");
                        }
                    }catch (Exception ex){
                        System.out.println("Get AllUser SQL Exception : ");
                        return Response.<UserSignUpDto>exception().setError("SignUp Failed. Exception Occurs.");
                    }
                }
                else
                { return Response.<UserSignUpDto>badRequest().setError("Your email is already in used.Try using different one.");
                }
            } else
                return Response.<UserSignUpDto>badRequest().setError("Field Cannot be empty...");
        }catch (Exception ex){
            System.out.println("SQL Exception Occur: "+ex.getMessage());
            return Response.<UserSignUpDto>badRequest().setError("SQL Exception Occur. Failed to SignUp  . ");
        }



    }
    @PostMapping("/login")
    public Response<UserLoginDto> login(@RequestBody UserLoginRequest request){
         try{
             String userProvider= userService.providerUser(request.getEmail());
            if (userProvider==null || userProvider.equalsIgnoreCase(Provider.FACEBOOK.toString()) ||userProvider.equalsIgnoreCase(Provider.GOOGLE.toString()))
                return Response.<UserLoginDto>badRequest().setError("Failed to login.Wrong credential.");

            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                    request.getEmail(),
                    request.getPassword()
            );
            UserLoginDto response = new UserLoginDto();
            try{
                Authentication authentication = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
                // Generate Token
                JwtUtils jwtUtils = new JwtUtils();
                String jwtToken = jwtUtils.generateJwtToken(authentication);
                response.setToken(jwtToken);

                User matchedUser = userService.login(request.getEmail().trim());

                if (matchedUser!=null){

                    String skills = null ;
                    try{
                        Freelancer freelancer  = userService.freelancerUser(matchedUser.getUser_id());
                        // We are going to add stuff later in the response class.
                        System.out.println(freelancer.getSkills());
                        response.setRole("Freelancer");
                        skills= freelancer.getSkills();
                    }catch (Exception exc){
                        System.out.println(" SQL Exception Occur : " + exc.getMessage());
                        BusinessOwner businessOwner = userService.businessUser(matchedUser.getUser_id());
                        response.setRole("Business Owner");
                    }
                    response.setEmail(matchedUser.getEmail());
                    response.setId(matchedUser.getUser_id());
                    response.setProfile_image(matchedUser.getProfile_image());
                    response.setBio(matchedUser.getBio());
                    response.setSkills(skills);
                    response.setUsername(matchedUser.getUsername());
                    return  Response.<UserLoginDto>ok().setPayload(response);
                }else{
                    return Response.<UserLoginDto>badRequest().setError("Failed to login. Wrong Credential ...");
                }
            }catch (Exception ex ){
                System.out.println("Exception Happens: " + ex.getMessage());
                return Response.<UserLoginDto>exception().setError("Failed to login. Wrong Credential ...");
            }
        }catch (Exception sqlException){
            System.out.println("SQL Exception : " +sqlException.getMessage());
            return Response.<UserLoginDto>exception().setError("Failed to login. Exception occurs.");
        }
    }
    @PostMapping("/loginWithProvider")
    public  Response<UserLoginDto> loginWithProvider( @RequestParam String email){



         try{
             String userProviderType = userService.providerUser(email);
             if (userProviderType==null || userProviderType.equalsIgnoreCase(Provider.NORMAL.toString()))
                 return Response.<UserLoginDto>badRequest().setError("This account doesn't exist.");
             else if ( userProviderType.equalsIgnoreCase(Provider.GOOGLE.toString())){
                 UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                          email,
                          "googlePa$$w0rd"
                 );
                 try{
                     Authentication authentication = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
                     UserLoginDto response = new UserLoginDto();
                     // Generate Token
                     JwtUtils jwtUtils = new JwtUtils();
                     String jwtToken = jwtUtils.generateJwtToken(authentication);
                     response.setToken(jwtToken);
                     User matchedUser = userService.login(email.trim());

                   try{
                       Freelancer freelancer  = userService.freelancerUser(matchedUser.getUser_id());
                             // We are going to add stuff later in the response class.
                             response.setRole("Freelancer");

                     }catch (Exception ex){
                       System.out.println(" SQL Exception Occur : " + ex.getMessage());
                             BusinessOwner businessOwner = userService.businessUser(matchedUser.getUser_id());
                             response.setRole("Business Owner");
                         }
                   response.setEmail(matchedUser.getEmail());
                         response.setId(matchedUser.getUser_id());
                         response.setProfile_image(matchedUser.getProfile_image());
                         response.setUsername(matchedUser.getUsername());

                         return  Response.<UserLoginDto>ok().setPayload(response);
                     }catch (Exception authManager){

                     System.out.println("Authentication manager Exception: "+authManager.getMessage());
                     return Response.<UserLoginDto>exception().setError("Exception Occurs.Failed to login with google account.");
                 }



             }else if (userProviderType.equalsIgnoreCase(Provider.FACEBOOK.toString())) {
                 UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                         email,
                         "facebookPa$$w0rd"
                 );


                 try{
                     UserLoginDto response = new UserLoginDto();
                     Authentication authentication = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
                     // Generate Token
                     JwtUtils jwtUtils = new JwtUtils();
                     String jwtToken = jwtUtils.generateJwtToken(authentication);
                     response.setToken(jwtToken);

                     User matchedUser = userService.login(email.trim());

                     try{
                         Freelancer freelancer  = userService.freelancerUser(matchedUser.getUser_id());
                         // We are going to add stuff later in the response class.
                         response.setRole("Freelancer");


                     }catch (Exception ex){

                         System.out.println(" SQL Exception Occur : " + ex.getMessage());
                         BusinessOwner businessOwner = userService.businessUser(matchedUser.getUser_id());
                         response.setRole("Business Owner");
                     }

                     response.setEmail(matchedUser.getEmail());
                     response.setId(matchedUser.getUser_id());
                     response.setProfile_image(matchedUser.getProfile_image());
                     response.setUsername(matchedUser.getUsername());

                     return  Response.<UserLoginDto>ok().setPayload(response);
                 }catch (Exception authManager){

                     System.out.println("Authentication manager Exception: "+authManager.getMessage());
                     return Response.<UserLoginDto>exception().setError("Exception Occurs.Failed to login with google account.");
                 }
                 //return Response.<UserLoginDto>ok().setError("So you log in with facebook account");
             }
             else return Response.<UserLoginDto>badRequest().setError("This account doesn't exist.");
        }catch (Exception sqlException){
            System.out.println("SQL Exception: "+sqlException.getMessage());
            return Response.<UserLoginDto>exception().setError("Failed to login . Exception occurs.");
        }

    }
}
