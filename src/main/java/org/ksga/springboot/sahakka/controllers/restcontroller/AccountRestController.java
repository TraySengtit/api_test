package org.ksga.springboot.sahakka.controllers.restcontroller;

import net.bytebuddy.utility.RandomString;
import org.ksga.springboot.sahakka.dto.accountdto.AccountUpdateDto;
import org.ksga.springboot.sahakka.dto.freelancerDto.FindFreelancerDto;
import org.ksga.springboot.sahakka.dto.postDto.AllProjectPostDto;
import org.ksga.springboot.sahakka.dto.postDto.FindPostDto;
import org.ksga.springboot.sahakka.dto.postDto.PostDto;
import org.ksga.springboot.sahakka.dto.servicepostdto.AllServicePostDto;
import org.ksga.springboot.sahakka.dto.userdto.BusinessOwnerUpdateResponse;
import org.ksga.springboot.sahakka.dto.userdto.FindBusinessOwnerDto;
import org.ksga.springboot.sahakka.dto.userdto.FreelancerUpdateResponse;
import org.ksga.springboot.sahakka.model.BusinessOwner;
import org.ksga.springboot.sahakka.model.FindBusinessOwner;
import org.ksga.springboot.sahakka.model.Freelancer;
import org.ksga.springboot.sahakka.model.post.Post;
import org.ksga.springboot.sahakka.model.post.ProjectPost;
import org.ksga.springboot.sahakka.model.user.FindFreelancer;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.payload.request.accountrequest.BusinessOwnerUpdateRequest;
import org.ksga.springboot.sahakka.payload.request.accountrequest.FreelancerUpdateRequest;
import org.ksga.springboot.sahakka.payload.request.accountrequest.ResetPasswordRequest;
import org.ksga.springboot.sahakka.payload.response.Response;
import org.ksga.springboot.sahakka.security.UserDetailsImp;
import org.ksga.springboot.sahakka.service.PostService;
import org.ksga.springboot.sahakka.service.ServicePostService;
import org.ksga.springboot.sahakka.service.UserService;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

@RestController
@RequestMapping("api/v1/account")
public class AccountRestController {

  public   UserService userService;
  public ServicePostService servicePostService;
  public PostService postService;

  @Autowired
  private JavaMailSender emailSender;




  @Autowired
    AccountRestController(UserService userService , ServicePostService servicePostService , PostService postService ) {


      this.userService = userService;
    this.servicePostService = servicePostService;
    this.postService = postService;
  }


 enum Roles{
        FREELANCER,BUSINESS_OWNER,ADMIN
    }
  @PutMapping("/update/freelancer")
  public Response<FreelancerUpdateResponse> updateFreelancerAccount(@AuthenticationPrincipal UserDetailsImp user, @RequestBody FreelancerUpdateRequest request){
      try{
              String currentUser = user.getUsername();
              if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();
              try {
                  String roleOfUser = servicePostService.roleOfUser(currentUser);
                  if (roleOfUser.equalsIgnoreCase(Roles.FREELANCER.toString())) {
                      int id = userService.findIdByUserEmail(user.getUsername());

                      boolean isUserUpdated = userService.updateUser(id, request.getUsername(), request.getProfile_image(), request.getEmail_address()
                              , request.getTelephone(),  request.getBio(), request.getGender(), request.getAddress(),request.getNationality());


                      // Delete the remaining social media in the table
                      boolean isDeleteSocialLinks = userService.deleteSocialLinkOfUser(currentUser);
                      // adding the social media in
                      List<String> allLinks = request.getSocial_media();
                      String maxUserSocialMedia = userService.maxUserSocialMedia();
                      int availableUserSocialMedia=0 ;
                      if (maxUserSocialMedia!=null) availableUserSocialMedia=Integer.valueOf(maxUserSocialMedia);

                      if (allLinks.size()>0)
                          for(int i=0; i<allLinks.size(); i++){
                              userService.addSkills(currentUser,availableUserSocialMedia+i+1, allLinks.get(i));
                          }

                      //Update Freelancer
                      boolean isFreelancerUpdated = userService.updateFreelancer(id, request.getEducation_background(), request.getWork_experience(), request.getSkills(), request.getLanguages(), request.getAchievement(),request.getNational_id());


                      if (isUserUpdated && isFreelancerUpdated) {
                          /* AccountUpdateDto response = new AccountUpdateDto();
                          response.setMessage("You have successfully updated your profile...");*/
                          FreelancerUpdateResponse response = new FreelancerUpdateResponse();

                          response.setUser_id(id);
                          response.setUsername(request.getUsername());
                          response.setProfile_image(request.getProfile_image());
                          response.setEmail_address(request.getEmail_address());
                          response.setTelephone(request.getTelephone());
                          response.setBio(request.getBio());
                          response.setGender(request.getGender());
                          response.setNationality(request.getNationality());
                          response.setSkills(request.getSkills());
                          response.setGender(request.getGender());
                          response.setAchievement(request.getAchievement());

                          response.setSocial_media(request.getSocial_media());
                          response.setWork_experience(request.getWork_experience());
                          response.setLanguages(request.getLanguages());
                          response.setEducation_background(request.getEducation_background());
                          response.setNational_id(request.getNational_id());

                          return Response.<FreelancerUpdateResponse>updateSucess().setPayload(response).setError("Update the profile information successfully.");
                      } else
                          return Response.<FreelancerUpdateResponse>exception().setError("Failed Failed to update the profile, exception occur..");
                  } else
                      return Response.<FreelancerUpdateResponse>badRequest().setError(currentUser + " is not a freelancer. Only freelancer can perform this action.");
              }catch (Exception sqlException){
                  System.out.println("SQL Exception : "+sqlException.getMessage());
                  return Response.<FreelancerUpdateResponse>exception().setError("SQL Exception.Failed to update the profile information ");
              }
          }catch (Exception authException){
               System.out.println("Authentication Error:" + authException.getMessage());
              return  Response.<FreelancerUpdateResponse>badRequest().setError("Authentication Error. Authentication is needed to perform this action");
          }

  }
  @PutMapping("/update/businessowner")
  public Response<BusinessOwnerUpdateResponse> updateBusinessOwner(@AuthenticationPrincipal UserDetailsImp user, @RequestBody BusinessOwnerUpdateRequest request){
      try  {

          String currentUser= user.getUsername();
          if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();

          try  {

              String roleOfUser = servicePostService.roleOfUser(currentUser);
              if (roleOfUser.equalsIgnoreCase( Roles.BUSINESS_OWNER.toString())) {

                  int id = userService.findIdByUserEmail(user.getUsername());
                  boolean isUserUpdated =  userService.updateUser(id,request.getUsername(),request.getProfile_image(),request.getEmail_address(),request.getTelephone(),request.getBio(),request.getGender(),request.getAddress(),request.getNationality());

                 //Update social media
                  // Delete the remaining social media in the table
                  boolean isDeleteSocialLinks = userService.deleteSocialLinkOfUser(currentUser);
                  // adding the social media in
                  List<String> allLinks = request.getSocial_media();
                  String maxUserSocialMedia = userService.maxUserSocialMedia();
                  int availableUserSocialMedia=0 ;
                  if (maxUserSocialMedia!=null) availableUserSocialMedia=Integer.valueOf(maxUserSocialMedia);

                  if (allLinks.size()>0)
                      for(int i=0; i<allLinks.size(); i++){
                          userService.addSkills(currentUser,availableUserSocialMedia+i+1, allLinks.get(i));
                      }

                  boolean isBusinessOwnerUpdated = userService.updateBusinessOwner(id,request.getCompany_name(),request.getCompany_contact(),request.getCompany_location(),request.getCompany_logo(),request.getCompany_description());

                  if (isBusinessOwnerUpdated && isUserUpdated) {

                      BusinessOwnerUpdateResponse response = new BusinessOwnerUpdateResponse();

                      response.setUser_id(id);
                      response.setUsername(request.getUsername());
                      response.setTelephone(request.getTelephone());
                      response.setSocial_media(request.getSocial_media());
                      response.setNationality(request.getNationality());
                      response.setProfile_image(request.getProfile_image());
                      response.setGender(request.getGender());
                      response.setEmail_address(request.getEmail_address());
                      response.setCompany_name(request.getCompany_name());
                      response.setCompany_logo(request.getCompany_logo());
                      response.setCompany_location(request.getCompany_location());
                      response.setCompany_description(request.getCompany_description());
                      response.setCompany_contact(request.getCompany_contact());
                      response.setBio(request.getBio());
                      response.setAddress(request.getAddress());

                      return Response.<BusinessOwnerUpdateResponse>updateSucess().setPayload(response).setError("You have successfully updated your account.");
                  }else {
                      return Response.<BusinessOwnerUpdateResponse>exception().setError("Failed to update the profile, exception occur..");
                  }
              }
              else  return Response.<BusinessOwnerUpdateResponse>badRequest().setError(currentUser + "is not a Business Owner. Only Business Owner can perform this action.");
          }catch (Exception sqlException){
              System.out.println("SQL Exception : " + sqlException.getMessage());
              return  Response.<BusinessOwnerUpdateResponse>exception().setError("SQL Exception. Failed to update the profile");
          }
      }catch (Exception authException){
          System.out.println("Authentication Error : "+ authException.getMessage());
          return Response.<BusinessOwnerUpdateResponse>exception().setError("Authentication Error.Authentication is needed to perform this action.");
      }
  }
  @PutMapping("/resetpassword")
  Response<AccountUpdateDto> resetPassword (@AuthenticationPrincipal UserDetailsImp user,  @RequestBody ResetPasswordRequest request){
      try{
          String currentUser =user.getUsername();
          if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();

          try{
              String previousPassword = userService.previousPassword(currentUser);
              BCryptPasswordEncoder decoder = new BCryptPasswordEncoder();
              if (decoder.matches(request.getOldPassword(),previousPassword)) {
                  if (request.getNewPassword().equals(request.getConfirmPassword())){
                      try {
                          BCryptPasswordEncoder encoder  = new BCryptPasswordEncoder();
                          userService.updatePassword(encoder.encode(request.getNewPassword()),user.getUsername());

                          AccountUpdateDto response  = new AccountUpdateDto() ;
                          response.setMessage("You have successfully reset your password ...");

                          return Response.<AccountUpdateDto>ok().setPayload(response);
                      }catch (Exception ex){
                          return Response.<AccountUpdateDto>exception().setError("Failed to reset the password ...");
                      }

                  }else {
                      return Response.<AccountUpdateDto>badRequest().setError(" NewPassword and Confirm doesnt match up.");
                  }
              }else {

                  return Response.<AccountUpdateDto>badRequest().setError("Wrong Credential... Check your previous password again...");
              }


          }catch (Exception ex ){

              System.out.println("SQL Exception : "+ ex.getMessage());
              return Response.<AccountUpdateDto>exception().setError("SQL Exception occurs. Failed to reset the password...");
          }
      }catch(Exception authException){

          System.out.println("Authentication Exception : " + authException.getMessage());
          return Response.<AccountUpdateDto>exception().setError("Authentication error.Authentication is needed to perform this action.");
      }





  }
  @GetMapping("/{id}/findBusinessOwner")
  public Response<FindBusinessOwnerDto> findBusinessOwnerById(@AuthenticationPrincipal UserDetailsImp user, @PathVariable int id ,@RequestParam(defaultValue = "10") int limit,@RequestParam(defaultValue = "1") int page){
      try{
          String currentUser = "default";
          boolean accountOwner = false;
          User resultUser = userService.findUserById(id);
          if (user!= null ){
              currentUser = user.getUsername();
                if (currentUser.equals(resultUser.getEmail()))
                    accountOwner = true;
          }
          if (resultUser==null) return  Response.<FindBusinessOwnerDto>notFound().setError("User with the provided Id doesn't exist ");


          String role = servicePostService.roleOfUser(resultUser.getEmail());
          System.out.println("role of this user is : " + role   );
          if (role.equalsIgnoreCase(Roles.FREELANCER.toString()) || role.equalsIgnoreCase(Roles.ADMIN.toString()))
              return Response.<FindBusinessOwnerDto>badRequest().setError("User with provided id is not a business owner.You can only find business owner profile.");

          BusinessOwner resultBusinessOwner = userService.businessUser(resultUser.getUser_id());
          List<String> socialMediaLinks=postService.linksUser(resultUser.getUser_id());

           FindBusinessOwner author = new FindBusinessOwner();
           author.setGender(resultUser.getGender());
               author.setUser_id(resultUser.getUser_id());
               author.setUsername(resultUser.getUsername());
               author.setTelephone(resultUser.getTelephone());
               author.setProfile_image(resultUser.getProfile_image());
               author.setLocation(resultUser.getAddress());
               author.setBio(resultUser.getBio());
               author.setOfficial_email(resultUser.getEmail_address());
               author.setNationality(resultUser.getNationality());

           // Business Information
                 author.setCompany_name(resultBusinessOwner.getCompany_name());
                 author.setCompany_location(resultBusinessOwner.getCompany_location());
                 author.setCompany_logo(resultBusinessOwner.getCompany_logo());
                 author.setCompany_contact(resultBusinessOwner.getCompany_contact());
                 author.setBusiness_description(resultBusinessOwner.getBusiness_description());
                 author.setSocial_media(socialMediaLinks);


                 // Pagination

          int totalCount = postService.countOwnerNormalProjectPosts(resultUser.getEmail());
          Paging paging = new Paging();
          paging.setLimit(limit);
          paging.setPage(page);
          paging.setTotalCount(totalCount);

                 int offset = (page -1) * limit;
             List<AllProjectPostDto> allProjectPost = postService.findOwnerProjectPosts(resultUser.getEmail(),limit,offset,"normal");
            FindBusinessOwnerDto response = new FindBusinessOwnerDto();
                response.setOwnerProfile(accountOwner);
                 response.setUser(author);
                 response.setProjectPost(allProjectPost);
          return Response.<FindBusinessOwnerDto>ok().setPayload(response).setMetadata(paging);
      }catch (Exception ex){
          System.out.printf("SQL Exception : "+ ex.getMessage());
          return Response.<FindBusinessOwnerDto>exception().setError("Failed to retrieve the post.Exception occurs.");
      }
  }
  @GetMapping("/{id}/findFreelancer")
  public Response<FindFreelancerDto> findFreelancerById(@AuthenticationPrincipal UserDetailsImp user, @PathVariable int id,@RequestParam(defaultValue = "10") int limit,@RequestParam(defaultValue = "1") int page ){
      try  {
          String currentUser = "default";
          boolean accountOwner = false;
          User resultUser = userService.findUserById(id);
          if (user!= null ){
              currentUser = user.getUsername();
              if (currentUser.equals(resultUser.getEmail()))
                  accountOwner = true;
          }
          if (resultUser==null) return  Response.<FindFreelancerDto>notFound().setError("User with the provided Id doesn't exist ");

          String role = servicePostService.roleOfUser(resultUser.getEmail());
          System.out.println("role of this user is : " + role   );
          if (role.equalsIgnoreCase(Roles.BUSINESS_OWNER.toString()) || role.equalsIgnoreCase(Roles.ADMIN.toString()))
              return Response.<FindFreelancerDto>badRequest().setError("User with provided id is not a freelancer.You can only find freelancer profile.");


          Freelancer resultFreelancer = userService.freelancerUser(resultUser.getUser_id());
          List<String> socialMediaLinks=postService.linksUser(resultUser.getUser_id());


          FindFreelancer author = new FindFreelancer();
          author.setGender(resultUser.getGender());
          author.setUser_id(resultUser.getUser_id());
          author.setUsername(resultUser.getUsername());
          author.setTelephone(resultUser.getTelephone());
          author.setProfile_image(resultUser.getProfile_image());
          author.setLocation(resultUser.getAddress());
          author.setBio(resultUser.getBio());
          author.setOfficial_email(resultUser.getEmail_address());
          author.setNationality(resultUser.getNationality());
          author.setSocial_media(socialMediaLinks);

          // Business Information
          author.setEducational_background(resultFreelancer.getEducational_background());
          author.setAchievement(resultFreelancer.getAchievement());
          author.setLanguages(resultFreelancer.getLanguages());
          author.setWork_experience(resultFreelancer.getWork_experience());
          author.setSkills(resultFreelancer.getSkills());

          Paging paging = new Paging();
          int totalCount = servicePostService.countAllOwnerServicePost(resultUser.getEmail(),"normal");
          paging.setLimit(limit);
          paging.setPage(page);
          paging.setTotalCount(totalCount);
          int offset = (page -1) * limit;

          List<AllServicePostDto> allServicePost = servicePostService.allOwnerServicePost(resultUser.getEmail(),limit,offset,"normal");

          FindFreelancerDto response = new FindFreelancerDto();
          response.setUser(author);
          response.setServicePost(allServicePost);
          response.setOwnerProfile( accountOwner);

          return Response.<FindFreelancerDto>ok().setPayload(response).setMetadata(paging);

      }catch (Exception sqlException){
          System.out.println("SQL Exception : "+sqlException.getMessage());
          return Response.<FindFreelancerDto>exception().setError("Failed to find freelancer.Exception occurs ");
      }

  }
  @DeleteMapping("/deactivate")
  public Response<String> deactivateAccount(@AuthenticationPrincipal UserDetailsImp user ,@RequestParam String password){
      try{
          String currentUser = user.getUsername();
          if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();
          // with the currentUser id we have the email of the user.
          try {
              String previousPassword = userService.previousPassword(currentUser);
              BCryptPasswordEncoder decoder = new BCryptPasswordEncoder();

              if (decoder.matches(password, previousPassword)) {
                  // Delete User
                    boolean isUserDeleted = userService.deleteUser(currentUser);
                return   (isUserDeleted)? Response.<String>successDelete().setError(currentUser + " has been deactivated successfully."): Response.<String>notFound().setError("Failed to deactivate." + currentUser+ " doesn't exist.");
              } else
                  return Response.<String>badRequest().setError("Wrong password!. Failed to deactivate the account.");

          }catch (Exception sqlException){
              System.out.println("SQL Exception : "+sqlException.getMessage());
              return Response.<String>exception().setError("SQL Exception Occurs. Failed to delete this account.");
          }

      }catch (Exception authEx){
          System.out.println("Exception Error : "+authEx.getMessage());
          return Response.<String>exception().setError("Failed to deactivate this account.Authentication is needed to complete this process");
      }
  }

  @PostMapping("/forgetPassword")
  public Response<String> forgetPassword(@RequestParam String email){
      try{
// 1. Check email of the user first. To see if it exists in the database or not

          String confirmedEmail= userService.verifyEmail(email);
          if (confirmedEmail==null) return Response.<String>badRequest().setError(" Provided email address doesn't exist.");
         // String token = "http://103.29.70.46:15001/swagger-ui.html#/";
          String token = RandomString.make(30);

          if (confirmedEmail==null) return Response.<String>notFound().setError(email+ "  doesn't exist.Check your email again.");
          // Adding the token to the database
          Boolean resetPasswordToken = userService.setResetTokenPassword(token,confirmedEmail);
          if (!resetPasswordToken) return Response.<String>exception().setError("Failed to reset the password.");
          sendMail(email,token);

          return Response.<String>ok().setError("We have sent token to your email.Please check.");
      }catch (Exception exception){
          System.out.println("Exception Occurs : "+ exception.getMessage());
          return Response.<String>exception().setError("Exception Occurs. Failed to perform this action.");


      }
  }

  @PostMapping("/resetForgetPassword")
  public  Response<String> resetForgottenPassword(@RequestParam String token ,@RequestParam String email,@RequestParam String newPassword,@RequestParam String confirmPassword){

     try   {
         //verify the token

         int verifyToken = userService.verifyTokenOfUser(email,token);
         if (verifyToken==0) return Response.<String>notFound().setError("Invalid Token.");
         if (newPassword.equals(confirmPassword)){
             BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

             Boolean updateForgottenPassword = userService.updateForgottenPassword(email,encoder.encode(confirmPassword),token);

           return   (updateForgottenPassword)? Response.<String>updateSucess().setError("Update password successfully"): Response.<String>exception().setError("Failed to update new password.");


         }else return Response.<String>badRequest().setError("New password and confirmed password doesn't match up");



     }catch (Exception ex){
         System.out.println("SQL Exception : "+ex.getMessage());
         return Response.<String>exception().setError("Failed to reset the password.Exception occurs.");
     }
  }

    public   void sendMail (String recipient ,String token) throws Exception {
        System.out.println("Preparing to send email");
        Properties properties = new Properties();

        //Enable authentication
        properties.put("mail.smtp.auth", "true");
        //Set TLS encryption enabled
        properties.put("mail.smtp.starttls.enable", "true");
        //Set SMTP host
        properties.put("mail.smtp.host", "smtp.gmail.com");
        //Set smtp port
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.ssl.trust", "*");




        //Your gmail address
        String myAccountEmail = "sahakka006@gmail.com";
        //Your gmail password

        String password = "Sahakka$$006";

        //Create a session with account credentials
        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(myAccountEmail, password);
            }
        });

        //Prepare email message
        Message message = prepareMessage(session, myAccountEmail, recipient,token);

        //Send mail
        Transport.send(message);
        System.out.println("Message sent successfully");
    }
    private static Message prepareMessage(Session session, String myAccountEmail, String recipient ,String token) {
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(myAccountEmail) );
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
            message.setSubject("Here the token for you to reset your password.");
//<a href="http://103.29.70.46:1000/"></a>



            String content="<html>\n" +

                    "<body style=\"font-family:verdana\">\n" +
                    "\n" +
                    "<p>Greeting from <b> Sahakka </b></p>\n"

                    + "<p>You are seeing this because you have requested to reset your password.</p>"
                    + "<p>What to do from now on is used provided token to request access to set your password with this email : </p>"
                    + "<p>Here is our token: <b> "+token +"</b> </p>"
                    + "<br>"
                    + "<p>Please kindly ignore this email if you already remember your password, "
                    + "or you have not made the request. Thank you and have a wonderful day.</p>"
                    +"<p>Best Regard From Sahakka </p> "+
                    "</body>\n" +
                    "</html>";


            message.setContent(content, "text/html");
            return message;
        } catch (Exception ex) {
            System.out.println("Prepare message exception : "+ex.getMessage());
        }
        return null;
    }




}
