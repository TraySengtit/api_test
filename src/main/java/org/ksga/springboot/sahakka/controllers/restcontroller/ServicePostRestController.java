package org.ksga.springboot.sahakka.controllers.restcontroller;


import org.ksga.springboot.sahakka.dto.postDto.AllProjectPostDto;
import org.ksga.springboot.sahakka.dto.servicepostdto.AllServicePostDto;
import org.ksga.springboot.sahakka.dto.servicepostdto.FindServicePostDto;
import org.ksga.springboot.sahakka.dto.servicepostdto.ReviewServicePostDto;
import org.ksga.springboot.sahakka.dto.servicepostdto.ServicePostDto;
import org.ksga.springboot.sahakka.model.post.Post;
import org.ksga.springboot.sahakka.model.postService.*;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.payload.request.ServicePostRequest.PostServiceReviewRequest;
import org.ksga.springboot.sahakka.payload.request.ServicePostRequest.ServicePostRequest;
import org.ksga.springboot.sahakka.payload.response.Response;
import org.ksga.springboot.sahakka.security.UserDetailsImp;
import org.ksga.springboot.sahakka.service.PostService;
import org.ksga.springboot.sahakka.service.ServicePostService;
import org.ksga.springboot.sahakka.service.UserService;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.swing.event.InternalFrameListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/v1/servicePost")

public class ServicePostRestController {
    private final ServicePostService servicePostService;
    private  final UserService userService;
    private final PostService postService;
    @Autowired
    public ServicePostRestController(PostService postService , ServicePostService servicePostService  , UserService userService) {
        this.servicePostService = servicePostService;
        this.userService = userService;
        this.postService = postService;
    }
    enum Roles{
        FREELANCER,BUSINESS_OWNER
    }
    @PostMapping("/create")
    public  Response<ServicePostDto> createServicePost(@AuthenticationPrincipal UserDetailsImp user, @RequestBody ServicePostRequest request){

         try  {

             String currentUser = user.getUsername();
             if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();

             try{
                 String roleOfUser = servicePostService.roleOfUser(currentUser);
                 if (roleOfUser.equalsIgnoreCase(Roles.FREELANCER.toString())){

                                 List<Integer> allServicePostId = servicePostService.allServicePost();
                                 int maxId = 0;
                                 if (allServicePostId.size()>0){
                                     maxId = allServicePostId.stream()
                                             .mapToInt(element-> element)
                                             .max().orElseThrow(NoSuchElementException::new);

                                 }

                                 // From the request
                                 List<String> categories = request.getCategories();
                                 // From the database
                                 List<Category> allCategories = servicePostService.allCategories();
                                 // String all Categories(db) as a string
                                 List<String> allStCategories = allCategories.stream()
                                         .map(element->element.getCategory_type())
                                         .collect(Collectors.toList());
                                 // Identifies all invalid categories
                                 List<String> invalidCategories = categories.stream()
                                         .filter(element->!allStCategories.contains(element.trim()))
                                         .collect(Collectors.toList());

                                 boolean isNoInvalidCategories = false;

                                 // In case we adding the wrong categories
                                 if (invalidCategories.size()>0){
                                     String allInvalidCategories =  invalidCategories.stream()
                                             .map(String::valueOf)
                                             .reduce("",(first,second)-> second + "  "+ first);

                                     return Response.<ServicePostDto>notFound().setError(allInvalidCategories+ "not existed . Try changing it or adding new category.");
                                     // is there is no error
                                 }else {
                                     isNoInvalidCategories= true;
                                 }
                                 try{
                                     ServicePostDto response = new ServicePostDto();
                                     // once there are some value in request categories
                                     if (categories.size()>0 && isNoInvalidCategories){
                                         //To avoid duplicate value
                                         HashSet<String> requestCategories = new HashSet<>();
                                         categories.stream()
                                                 .map(requestCategories::add)
                                                 .collect(Collectors.toList());
                                         List<String>  validCategories =   requestCategories.stream()
                                                 .map(String::valueOf)
                                                 .collect(Collectors.toList());
                                         servicePostService.createServicePost(maxId+1,currentUser,request.getPostTitle(),request.getPostImage(),request.getPostStatus(),request.getPostDescription());
                                         // Adding categories to the service post
                                         for( int i=0; i<validCategories.size(); i++){
                                             for(int j =0 ;j<allCategories.size(); j++){

                                                 if (validCategories.get(i).trim().equalsIgnoreCase(allCategories.get(j).getCategory_type().trim())){
                                                     servicePostService.addCategoriesToServicePost(maxId+1,allCategories.get(j).getCategory_id());

                                                 }
                                             }
                                         }
                                         response.setCategories(request.getCategories());
                                     }
                                     else {
                                         // Just create normal post
                                         servicePostService.createServicePost(maxId+1,currentUser,request.getPostTitle(),request.getPostImage(),request.getPostStatus(),request.getPostDescription());
                                     }
                                     // response to user
                                     response.setId(maxId+1);
                                     response.setTitle(request.getPostTitle());
                                     response.setPost_image(request.getPostImage());
                                     response.setPost_description(request.getPostDescription());
                                     response.setPost_status(request.getPostStatus());
                                     response.setAuthor(currentUser);
                                     return Response.<ServicePostDto>successCreate().setPayload(response);

                                 }catch (Exception ex){
                                     System.out.println("Created Post exception: " + ex.getMessage());
                                     return Response.<ServicePostDto>exception().setError("Exception Occur! Failed to create new service post....");
                                 }
                 }else {
                     return Response.<ServicePostDto>badRequest().setError(currentUser + " is not a freelancer. Cannot perform this action");
                 }

             }catch (Exception sqlException){
                 System.out.println("Exception Error : " + sqlException.getMessage());
                 return Response.<ServicePostDto>exception().setError("Failed to create the service post. Exception occurs....");
             }

         }catch ( Exception ex){

             System.out.println("Something's wrong with the authentication "+ ex.getMessage());
             return Response.<ServicePostDto>exception().setError("Authentication Error.Authentication is needed to perform this action.");

         }

    }
    @PutMapping("{id}/update")
    public Response<ServicePostDto> updateServicePost(@AuthenticationPrincipal UserDetailsImp user,@PathVariable int id,@RequestBody ServicePostRequest request){

        try{
            String currentUser = user.getUsername();
            if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();

            try {
                 String roleOfUser = servicePostService.roleOfUser(currentUser);
                if (roleOfUser.equalsIgnoreCase(Roles.FREELANCER.toString())) {
                     int numberOfServicePost = servicePostService.allServicePostOfUser(currentUser);
                     if (numberOfServicePost>0){
                         // Verify the owner of the post
                         String verifyServicePostOwnership = servicePostService.verifyServicePost(currentUser,id);
                         if ( verifyServicePostOwnership!=null){
                             // update process:

                             // From the request
                             List<String> categories = request.getCategories();
                             // From the database
                             List<Category> allCategories = servicePostService.allCategories();
                             // String all Categories(db) as a string
                             List<String > allStCategories = allCategories.stream()
                                     .map(element->element.getCategory_type())
                                     .collect(Collectors.toList());
                             // Identifies all invalid categories
                             List<String> invalidCategories = categories.stream()
                                     .filter(element->!allStCategories.contains(element.trim()))
                                     .collect(Collectors.toList());

                             boolean isNoInvalidCategories = false;

                             // In case we adding the wrong categories
                             if (invalidCategories.size()>0){
                                 String allInvalidCategories =  invalidCategories.stream()
                                         .map(String::valueOf)
                                         .reduce("",(first,second)-> second + "  "+ first);

                                 return Response.<ServicePostDto>notFound().setError(allInvalidCategories+ "not existed . Try changing it or adding new category.");
                                 // is there is no error
                             }else {
                                 isNoInvalidCategories= true;
                             }
                             try{
                                 ServicePostDto response = new ServicePostDto();
                                 // once there are some value in request categories
                                 if (categories.size()>0 && isNoInvalidCategories){
                                     //To avoid duplicate value
                                     HashSet<String> requestCategories = new HashSet<>();
                                     categories.stream()
                                             .map(requestCategories::add)
                                             .collect(Collectors.toList());
                                     List<String>  validCategories =   requestCategories.stream()
                                             .map(String::valueOf)
                                             .collect(Collectors.toList());

                                     servicePostService.updateServicePost(id,request.getPostTitle(),request.getPostImage(),request.getPostStatus(),request.getPostDescription());

                                    //delete all category and add the new one.
                                     boolean isServicePostCategoryDeleted= servicePostService.deleteServicePostCategory(id);
                                     System.out.println("isServicePostCategory: "+ isServicePostCategoryDeleted);

                                     // Adding categories to the service post
                                     for( int i=0; i<validCategories.size(); i++){
                                         for(int j =0 ;j<allCategories.size(); j++){

                                             if (validCategories.get(i).trim().equalsIgnoreCase(allCategories.get(j).getCategory_type().trim())){
                                                 //servicePostService.updateCategoryServicePost(allCategories.get(j).getCategory_id(),id);
                                                 servicePostService.addCategoriesToServicePost(id,allCategories.get(j).getCategory_id());
                                             }
                                         }
                                     }
                                     response.setCategories(request.getCategories());
                                 }
                                 else {
                                     // Just update normal post
                                     servicePostService.updateServicePost(id,request.getPostTitle(),request.getPostImage(),request.getPostStatus(),request.getPostDescription());
                                 }
                                 // response to user
                                 response.setId(id);
                                 response.setTitle(request.getPostTitle());
                                 response.setPost_image(request.getPostImage());
                                 response.setPost_description(request.getPostDescription());
                                 response.setPost_status(request.getPostStatus());
                                 response.setAuthor(currentUser);
                                 return Response.<ServicePostDto>updateSucess().setPayload(response);

                             }catch (Exception ex){
                                 System.out.println("Update Service Post exception: " + ex.getMessage());
                                 return Response.<ServicePostDto>exception().setError("Exception Occur! Failed to update the service post....");
                             }


                         }else return Response.<ServicePostDto>notFound().setError("Service post with the provided ID doesn't exist in this user account.");
                      }else return Response.<ServicePostDto>notFound().setError("This account doesn't have any service posts.");

                }
                else  return Response.<ServicePostDto>exception().setError(currentUser + " is not a freelancer. Only a freelancer can perform this action.");
            }catch (Exception sqlException){
                return Response.<ServicePostDto>exception().setError("Failed to update the service post. SQL Exception.");
            }

        }catch (Exception authException){
            return Response.<ServicePostDto>exception().setError("Authentication Error. Authentication is needed to perform this action.");
        }


    }
    @DeleteMapping("/{id}/delete")
    public Response<ServicePostDto> deleteById(@AuthenticationPrincipal UserDetailsImp user, @PathVariable int id){

        try {

            String currentUser = user.getUsername();
            if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();

            try {
                String roleOfUser = servicePostService.roleOfUser(currentUser);
                if (roleOfUser.equalsIgnoreCase(Roles.FREELANCER.toString())){
                    // if there is no post then let user knows
                    int numberOfServicePost = servicePostService.allServicePostOfUser(currentUser);
                    if (numberOfServicePost>0)
                    { String verifyServicePost = servicePostService.verifyServicePost(currentUser,id);
                        if (verifyServicePost!=null){
                            // perform delete operation
                            String reviewId= servicePostService.getReviewId(id);
                            if (reviewId!=null){
                                boolean deleteReviewReply = servicePostService.deleteAllReviewReply(id,Integer.parseInt(reviewId));
                                System.out.println("DeleteReviewReply : "+ deleteReviewReply);
                            }
                            boolean deleteReview= servicePostService.deleteReview(id);
                            System.out.println("DeleteReview: "+ deleteReview);

                            boolean deleteCategoryOfServicePost= servicePostService.isCategoryServicePostDeleted(id);
                            boolean deleteServicePost=  servicePostService.isServicePostCategoryDeleted(currentUser,id);

                            return Response.<ServicePostDto>successDelete().setError("You have deleted this service post successfully.");
                        }else
                            return Response.<ServicePostDto>notFound().setError("Project Post with provided id doesn't exist in this account..");
                    }else
                    {
                        return Response.<ServicePostDto>notFound().setError("This account doesn't have any service post yet.");
                    }
                }
                else{
                    return Response.<ServicePostDto>badRequest().setError(currentUser+ " is not a freelancer.Only Freelancer can perform this action.");
                }
          }catch (Exception sqlException){
                System.out.println("Message : "  + sqlException.getMessage());
                return Response.<ServicePostDto>exception().setError("Failed to delete service post..... SQL Exception occurs.");
            }
        }catch (Exception e){
            System.out.println("Authentication Error: ");
            return Response.<ServicePostDto>exception().setError("Authentication Error ... Try Login again.");
        }

    }
    @PostMapping("/{id}/review")
    public Response<ReviewServicePostDto> reviewServicePost(@AuthenticationPrincipal UserDetailsImp user, @PathVariable int id , @RequestBody PostServiceReviewRequest request){

        try{

            String currentUser = user.getUsername();
            if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();
            try{
                String roleOfUser = servicePostService.roleOfUser(currentUser);
                if (roleOfUser.equalsIgnoreCase(Roles.BUSINESS_OWNER.toString())){

                    try{
                        Date date = Calendar.getInstance().getTime();
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        String currentDate = dateFormat.format(date);
                        int availableId=0;
                        String maxServiceId = servicePostService.maxReviewId();
                        if (maxServiceId!=null)  availableId= Integer.parseInt(maxServiceId);

                        servicePostService.addReviewToServicePost(availableId, currentUser,id,request.getReviews(),request.getNumberOfStar(),currentDate);

                        //find User BusinessOwner
                        User  reviewUser = userService.findUserByEmail(currentUser);

                        ReviewServicePostDto response = new ReviewServicePostDto();
                        response.setReviewText(request.getReviews());
                        response.setNumberOfStars(request.getNumberOfStar());
                        response.setPostedDate(currentDate);
                        response.setServicePostId(id);

                        response.setReviewId(availableId);
                        response.setReviewByUserId(reviewUser.getUser_id());
                        response.setReviewByUser(reviewUser.getUsername());
                        response.setReviewUserProfile(reviewUser.getProfile_image());

                        return Response.<ReviewServicePostDto>ok().setPayload(response);
                    }catch (Exception exception){
                        return Response.<ReviewServicePostDto>badRequest().setError("Provided ID of service post doesn't exist..");
                    }
                }else{
                    return Response.<ReviewServicePostDto>badRequest().setError(currentUser + " is not a business owner. Cannot perform this action");
                }
           }catch (Exception sqlException){
               // System.out.println("Exception OCCur : "+sqlException.getMessage());
                return Response.<ReviewServicePostDto>exception().setError("Exception Occurs.. Failed to review the service post.");

            }


        }catch (Exception ex){
            System.out.println("Authentication Error "  + ex.getMessage());
            return Response.<ReviewServicePostDto>exception().setError("Authentication is needed to complete this process..");
        }



    }
    @PostMapping("/{servicePostId}/reply")
    public Response<ReviewServicePostDto> replyReview(@AuthenticationPrincipal UserDetailsImp user, @PathVariable int servicePostId,@RequestParam int reviewId,  @RequestBody PostServiceReviewRequest request){

            try{

                String currentUser = user.getUsername();
                if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();

                try{
                    String roleOfUser = servicePostService.roleOfUser(currentUser);
                    if (roleOfUser.equalsIgnoreCase(Roles.FREELANCER.toString())){
                        try{
                            // Check to see if there is any reviews or not

                            int numberOfReviews= servicePostService.contReview(servicePostId);
                            if (numberOfReviews==0) return Response.<ReviewServicePostDto>notFound().setError("There is no review to reply to.");


                            String userID =servicePostService.verifyServicePost(currentUser,servicePostId);

                            if (userID!=null){
                                System.out.println("userId: "+ userID);
                                    //now that the service post exist
                                    Date date = Calendar.getInstance().getTime();
                                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    String currentDate = dateFormat.format(date);

                                     boolean isReplyReview = servicePostService.replyReviewToServicePost(servicePostId,currentUser,request.getReviews(),currentDate, reviewId);
                                    //System.out.println("isReplyReviews Status : "+ isReplyReview);

                                    User replyUser = userService.findUserByEmail(currentUser);

                                    ReviewServicePostDto response = new ReviewServicePostDto();
                                    response.setServicePostId(servicePostId);
                                    response.setReviewId(reviewId);
                                    response.setPostedDate(currentDate);
                                    response.setReviewText(request.getReviews());
                                    response.setReviewByUser(replyUser.getUsername());
                                    response.setReviewByUserId(replyUser.getUser_id());
                                    response.setReviewUserProfile(replyUser.getProfile_image());

                                    return Response.<ReviewServicePostDto>ok().setPayload(response);
                                }else {
                                    return  Response.<ReviewServicePostDto>badRequest().setError("ServicePost with the provided Id doesn't exist. ");
                                }



                            }
                            catch (Exception exception){
                                System.out.println("Exception Error : "+ exception.getMessage());
                                return  Response.<ReviewServicePostDto>ok().setError("Provided ReviewId doesn't exist....");
                            }
                }
                    else
                    {

                        return  Response.<ReviewServicePostDto>exception().setError(currentUser + " is not a owner of this service post. Cannot reply..");
                    }



                }catch (Exception sqlException){

                    System.out.println("SQL Exception : " + sqlException.getMessage());
                    return Response.<ReviewServicePostDto>exception().setError("Exception Occur. Failed to reply the review ....");
                }



            }catch (Exception authException){
                return Response. <ReviewServicePostDto>exception().setError("Authentication Error. Failed to response to the review");
            }

    }
    @GetMapping("/{id}/findServicePostById")
    public Response<FindServicePostDto> findById(@AuthenticationPrincipal UserDetailsImp user, @PathVariable int id){

      try{
//Find the service post
          String currentUser="default";
          if (user !=null)
             currentUser = user.getUsername();
          boolean isPostOwner =false;
          String postOwnership=" ";
          if (currentUser!=null || currentUser.equals("default")){

              System.out.println("Hahaahaaaa");
              postOwnership = servicePostService.verifyServicePost(currentUser,id);
              if (postOwnership!=null) isPostOwner=true;

          }
       //   System.out.println("IsPostOwner : "+isPostOwner);

          ServiceCard resultServicePost = servicePostService.findServiceCardById(id);
        if (resultServicePost==null) return Response.<FindServicePostDto>notFound().setError("Service post with provided id doesn't exist");

       /* User freelancer = userService.findUserById(resultServicePost.getUser_id());
        List<String> socialMediaLinks=postService.linksUser(resultServicePost.getUser_id());
*/
        UserServicePost freelancer = userService.findFreelancerById(resultServicePost.getUser_id());
        List<String> servicePostCategories = servicePostService.categoryOfServicePostById(id);

        // Show all the review
        int numberOfReview= servicePostService.contReview(id);
        List<Review> servicePostReview = servicePostService.allServicePostReview(id);

          FindServicePostDto response = new FindServicePostDto( );
              response.setNumber_of_review(numberOfReview);
              response.setId(resultServicePost.getId());
              response.setPost_image(resultServicePost.getPost_image());
              response.setPost_description(resultServicePost.getPost_description());
              response.setTitle(resultServicePost.getTitle());
              response.setPost_status(resultServicePost.getPost_status());
              response.setService_post_owner(isPostOwner);

              //user
              response.setUser(freelancer);
              response.setService_post_categories(servicePostCategories);

              response.setServicePostReview(servicePostReview);

          return Response.<FindServicePostDto>ok().setPayload(response);
      }catch(Exception ex){
          System.out.println("SQL Exception: "+ex.getMessage());
          return Response.<FindServicePostDto>exception().setError("Failed to find the service post.SQL Exception");
      }

    }
    // Review the service post
    @GetMapping("/findAllServicePost")
    public Response<List<AllServicePostDto>> getAllServicePost (@RequestParam(required = false)  String category,@RequestParam(required = false) String username,   @RequestParam(defaultValue = "1") int page , @RequestParam(defaultValue = "10")  int limit){

        try{
            Paging paging = new Paging();
            paging.setPage(page);
            paging.setLimit(limit);
            int offset = (page -1) * limit;

            if ((category==null || category.trim().equals(""))&& username!=null && !username.trim().equals("") )
            {
                int totalCount=servicePostService.countServicePostOfUsername(username);
                paging.setTotalCount(totalCount);
                List<AllServicePostDto> response = servicePostService.getServicePostByUsername(username,limit,offset);
                return Response.<List<AllServicePostDto>>ok().setPayload(response).setMetadata(paging);
            }else if ( (username==null|| username.equals("")) && category!=null && !category.trim().equals("")   ){
                //Category is not NUll and the username is null
                int totalCount = servicePostService.countServicePostByCategory(category);
                paging.setTotalCount(totalCount);
                List<AllServicePostDto> response = servicePostService.getServicePostByCategory(category,limit,offset);
                return Response.<List<AllServicePostDto>>ok().setPayload(response).setMetadata(paging);
            } else if (category!=null && username!=null && !username.trim().equals("")  && !category.trim().equals("") ){
                // Category and username are not null
                int totalCount = servicePostService.countServicePostByUsernameAndCategory(username,category);
                paging.setTotalCount(totalCount);
                List<AllServicePostDto> response = servicePostService.getServicePostByUsernameAndCategory(username,category,limit,offset);
                 return Response.<List<AllServicePostDto>>ok().setPayload(response).setMetadata(paging);

            }else {
                // Category and username is null
                int totalCount = servicePostService.allAvailableServicePost();
                paging.setTotalCount(totalCount);

                System.out.println("Value of the offset : " + offset);
                List<AllServicePostDto> response = servicePostService.allNormalServicePost(limit,offset);

                return Response.<List<AllServicePostDto>>ok().setPayload(response).setMetadata(paging);
            }


        }catch (Exception ex){
            System.out.println("SQL Exception : "+ex.getMessage());
            return Response.<List<AllServicePostDto>>exception().setError("Failed to get all the service posts.Exception occurs .");

        }


    }

    enum ServicePostTypes{
        NORMAL,PENDING,DRAFT
    }
    @GetMapping("/ownerGetAllServicePost")
    public Response<List<AllServicePostDto>> ownerGetAllServicePosts(@AuthenticationPrincipal UserDetailsImp user , @RequestParam(defaultValue = "10") int limit , @RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "NORMAL") PostRestController.PostTypes postType){

        try {
            String currentUser= user.getUsername();
            if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();
            try{
                //check role cause this is for the business owner user only .
                String roleOfUser = servicePostService.roleOfUser(currentUser);
                if (roleOfUser.equalsIgnoreCase(Roles.FREELANCER.toString())) {
                    int offset = (page -1) * limit;
                    Paging paging = new Paging();
                    paging.setPage(page);
                    paging.setLimit(limit);

                 int totalCount =  servicePostService.countAllOwnerServicePost (currentUser,postType.toString().toLowerCase());
                    paging.setTotalCount(totalCount);
                    List<AllServicePostDto> response =servicePostService.allOwnerServicePost(currentUser,limit,offset,postType.toString().toLowerCase());
                    return Response.<List<AllServicePostDto>>ok().setPayload(response).setMetadata(paging);

                }else return Response.<List<AllServicePostDto>>badRequest().setError(currentUser+ " is a business owner. Only freelancer can perform this action.");

            }catch (Exception sqlException ){
                System.out.println("SQL Exception : "+sqlException.getMessage());
                return Response.<List<AllServicePostDto>>exception().setError("Failed to get the service post.Exception occurs.");
            }

        }catch (Exception authException){
            System.out.println("Authentication Error : "+ authException.getMessage());
            return Response.<List<AllServicePostDto>>exception().setError("Failed to get all service posts due to the authentication exception");

        }

    }

    @PostMapping("/{id}/reportServicePost")
    public  Response<String> reportServicePost(@AuthenticationPrincipal UserDetailsImp user,@PathVariable int id,@RequestParam String reason){

        try{
            String currentUser = user.getUsername();
            if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();
            try {
                String roleOfUser = servicePostService.roleOfUser(currentUser);
                if (roleOfUser.equalsIgnoreCase(Roles.BUSINESS_OWNER.toString())) {

                    int serviceId =0;
                    String availableId = servicePostService.maxServicePostId();
                    if (availableId!=null) serviceId = Integer.parseInt(availableId);

                    Boolean createReport = false;
                    try {
                        createReport=true;

                        Date date = Calendar.getInstance().getTime();
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        String currentDate = dateFormat.format(date);
                        createReport= servicePostService.createServicePostReport(currentUser,serviceId+1,id,reason,currentDate);
                    }catch (Exception ex){
                        createReport=false;
                        System.out.println("here is the value of the exception : "+ex.getMessage());
                    }


                return  createReport? Response.<String>successCreate().setError("Report has been added to this service post successfully."):Response.<String>notFound().setError("Provided Id doesn't exist .");
                }
                  else return Response.<String>badRequest().setError(currentUser + " is a Freelancer. Only Business Owner account can perform this action.");


            }catch (Exception sqlException){
                System.out.println("SQL Exception Occurs: "+sqlException.getMessage());
                return Response.<String>exception().setError("Failed to report this service post.SQL Exception occurs");
            }

        }catch (Exception authException){

            System.out.println("Authentication Exception: "+ authException.getMessage());
            return Response.<String>exception().setError("Authentication is needed to perform this actions.");
        }

    }


}
