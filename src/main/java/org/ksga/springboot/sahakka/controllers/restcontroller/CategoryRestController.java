package org.ksga.springboot.sahakka.controllers.restcontroller;

import org.apache.ibatis.annotations.Param;
import org.ksga.springboot.sahakka.dto.categoryDto.CategoryDto;
import org.ksga.springboot.sahakka.model.category.Category;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.payload.request.CategoryRequest.CategoryRequest;
import org.ksga.springboot.sahakka.payload.response.Response;
import org.ksga.springboot.sahakka.security.UserDetailsImp;
import org.ksga.springboot.sahakka.service.CategoryService;
import org.ksga.springboot.sahakka.service.ServicePostService;
import org.ksga.springboot.sahakka.service.UserService;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/v1/category")

public class CategoryRestController {
    private final CategoryService categoryService;
    private final UserService userService;
    private final ServicePostService servicePostService;

    @Autowired
    public CategoryRestController(ServicePostService servicePostService, CategoryService categoryService , UserService userService) {
        this.categoryService = categoryService;
        this.servicePostService = servicePostService;
        this.userService = userService;

    }
    private  static  final String ADMIN="ADMIN";
    @PostMapping("/create")
    public Response<CategoryDto> create(@AuthenticationPrincipal UserDetailsImp user, @RequestParam List<String> categoryType){

        try{
            String currentUser = user.getUsername();
            if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();

            try{
                String roleOfUser = servicePostService.roleOfUser(currentUser);
                if (roleOfUser.equalsIgnoreCase(ADMIN)){
                    List<org.ksga.springboot.sahakka.model.postService.Category> allCategories = servicePostService.allCategories();
                    // String all Categories(db) as a string
                    List<String> allStCategories = allCategories.stream()
                            .map(element->element.getCategory_type())
                            .collect(Collectors.toList());
                    List<String> invalidCategories = categoryType.stream()
                            .filter(element->allStCategories.contains(element.trim()))
                            .collect(Collectors.toList());

                    boolean isNoInvalidCategories = false;

                    // In case we adding the wrong categories
                    if (invalidCategories.size()>0){
                        String allInvalidCategories =  invalidCategories.stream()
                                .map(String::valueOf)
                                .reduce("",(first,second)-> second + "  "+ first);

                        return Response.<CategoryDto>notFound().setError(allInvalidCategories+ "already existed. Try adding different one.");
                        // is there is no error
                    }else {
                        isNoInvalidCategories= true;
                    }

                    try {

                        if (categoryType.size()>0 && isNoInvalidCategories){
                            // Add new categories
                            int availableId=0;

                            String maxCategory = categoryService.maxCategory();
                            if (maxCategory != null) {
                                availableId = Integer.parseInt(maxCategory);
                            }

                            for( int i= 0 ; i<categoryType.size() ; i++){
                                categoryService.addNewCategory(availableId+1+i,categoryType.get(i));
                            }

                            return Response.<CategoryDto>ok().setError("New Categories successfully added ");
                        }else
                        return Response.<CategoryDto>badRequest().setError("Failed to create categories.Empty values...");

                    }catch (Exception ex){
                        System.out.println("SQL exception when creating the new categories"+ ex.getMessage());
                        return Response.<CategoryDto>exception( ).setError("Failed to create new categories. SQL Exception.");

                    }



                }else return   Response.<CategoryDto>badRequest().setError(currentUser + " is not the admin.Only admin can perform this action.");



            }catch (Exception sqlException){
                System.out.println("SQL Exception : "+sqlException.getMessage());
                return Response.<CategoryDto>exception().setError("SQL Exception. Failed to create a new category");
            }
        }catch (Exception authException){

            System.out.println("Authentication Error : " + authException.getMessage());
            return Response.<CategoryDto>exception().setError("Authentication Error.Authentication is needed to perform this action.");


        }



    }
    @GetMapping("/{id}/find")
    public Response<CategoryDto>findById(@AuthenticationPrincipal UserDetailsImp user, @PathVariable int id){

        // First version
          try{
            String currentUser = user.getUsername();
             if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();
             try{
                 Category category =categoryService.findById(id);
                  if (category!=null){
                      CategoryDto response = new CategoryDto();
                          response.setCategory_id(category.getCategory_id());
                          response.setCategory_type(category.getCategory_type());
                      return Response.<CategoryDto>ok().setPayload(response);
                  }else
                      return Response.<CategoryDto>notFound().setError("Category with the provided id.Doesn't exist.");

             }catch (Exception sqlException){
                 System.out.println("Sql Exception : "+sqlException.getMessage());
                 return Response.<CategoryDto>exception().setError("SQL Exception. Fail to retrieve the category.");
             }
        }catch (Exception authException){
              System.out.println("Auth Exception : " +authException.getMessage());
              return Response.<CategoryDto>exception().setError("Authentication Error. Authentication is needed to perform this action.");
          }
    }
    @DeleteMapping("/{id}/delete")
    public Response<CategoryDto>deleteById(@AuthenticationPrincipal UserDetailsImp user, @PathVariable int id){

        try{
            String currentUser = user.getUsername();
            if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();

            try{

                String roleOfUser = servicePostService.roleOfUser(currentUser);
                if (roleOfUser.equalsIgnoreCase(ADMIN)){
                    int numberOfCategories = categoryService.countCategories();
                         if (numberOfCategories==0) return Response.<CategoryDto>notFound().setError("There is no categories yet");


                    boolean isCategoryDeleted =  categoryService.delete(id);

                        if (isCategoryDeleted) return Response.<CategoryDto>successDelete().setError("You have successfully deleted the category");
                        else return Response.<CategoryDto>notFound().setError("Provided id of the category doesn't exist.");

                }else return Response.<CategoryDto>badRequest().setError(currentUser+"  is not an admin.Only Admin can perform this action.");
            }catch (Exception sqlException){
                System.out.println("SQL Exception : " + sqlException.getMessage());
                return Response.<CategoryDto>exception().setError("SQL Exception. Failed to delete the project post.");
            }
        }catch (Exception authException){
            System.out.println("Authentication Error: "+authException.getMessage());
            return Response.<CategoryDto>exception().setError("Authentication Error. Authentication is needed to perform this action.");
        }
    }
    enum ApprovalType{ APPROVE,DISAPPROVE}
    @PutMapping("/{id}/approval")
    public Response<CategoryDto> approval(@AuthenticationPrincipal UserDetailsImp user,@PathVariable int id ,@RequestParam ApprovalType type){
        try{
            String currentUser = user.getUsername();
            if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();

            try{
                String roleOfUser = servicePostService.roleOfUser(currentUser);
                if (roleOfUser.equalsIgnoreCase(ADMIN)){
                    // verify the provided id is true or not
                    String suggestedCategory=categoryService.getSuggestedCategoryById(id);
                    if (suggestedCategory==null) return Response.<CategoryDto>notFound().setError("Suggested Category with the provided id doesn't exist");
                     if (type.equals(ApprovalType.APPROVE)){
                         // adding suggested category to the database
                        boolean isSuggestedCategoryUpdated = categoryService.suggestedCategoryUpdate(id);

                     if (isSuggestedCategoryUpdated)   return Response.<CategoryDto>updateSucess().setError(suggestedCategory + " is successfully added as a new category.");
                        else return Response.<CategoryDto>exception().setError("Exception Occurs. Failed to approve this category suggestion.");
                     }else {
                        // delete the suggested category from the database
                         boolean isSuggestedCategoryRemove = categoryService.suggestedCategoryRemove(id);
                       if (isSuggestedCategoryRemove) return Response.<CategoryDto>successDelete().setError(suggestedCategory + " is successfully removed.");
                       else return Response.<CategoryDto>exception().setError("Exception Occurs. Failed to remove category suggestion.");
                     }
                } else return Response.<CategoryDto>badRequest().setError(currentUser + " is not an admin.Only admin can perform this action.");
            }catch (Exception sqlException){
                System.out.println("SQL Exception : "+ sqlException.getMessage());
                return Response.<CategoryDto>exception().setError("SQL Exception.Failed to approve this suggestion");
            }
        }catch (Exception authException){
            System.out.println("Authentication Error : "+authException.getMessage());
            return Response.<CategoryDto>exception().setError("Authentication Error. Authentication is needed to perform this action.");
        }
    }
    @PutMapping("/{id}/update")
    public Response<CategoryDto> updateCategory (@AuthenticationPrincipal UserDetailsImp user, @PathVariable int id, @RequestParam String updatedCategory) {
        try{
             String currentUser = user.getUsername();
             if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();
             try {
                 String roleOfUser = servicePostService.roleOfUser(currentUser);
                 if (roleOfUser.equalsIgnoreCase(ADMIN)) {

                     boolean isCategoryUpdated = categoryService.update(id,updatedCategory);
                     if (isCategoryUpdated) return Response.<CategoryDto>updateSucess().setError("Category has now been updated successfully.");
                     else return Response.<CategoryDto>notFound().setError("Category with the provided id doesn't exist.");

                 } else {
                     return Response.<CategoryDto>badRequest().setError(currentUser + " is not an admin.Only admin can perform this action");
                 }
             }catch (Exception sqlException){
                 return Response.<CategoryDto>exception().setError("Failed to update the category.Exception Occurs");

             }
   }catch (Exception authException){
             System.out.println("Authentication Error: ");
         return Response.<CategoryDto>exception().setError("Authentication Error. Authentication is needed to perform this action. ");
         }
    }


    @GetMapping("/getAllCategories")
    public Response<List<String>> getAllCategories (@RequestParam(defaultValue = "10")  int limit ,@RequestParam(defaultValue = "1") int page){

        try{
            Boolean suggestedState = false;

        int totalRecords = categoryService.countAllCategories(suggestedState);




         Paging paging = new Paging();
            paging.setLimit(limit);
            paging.setPage(page);
         paging.setTotalCount(totalRecords);

            int offset = (page -1) * limit;
            List<String> allCategories = categoryService.getAllCategories(false,limit,offset);

            if (allCategories.size()==0) return Response.<List<String>>notFound().setError("There is no categories to show");
            return Response.<List<String>>ok(). setPayload(allCategories) .setMetadata(paging);

        }catch (Exception sqlException){
            System.out.println("SQL Exception : Failed to get all categories " + sqlException.getMessage());
            return Response.<List<String>>exception().setError("Failed to get all the categories. Exception occurs ");
        }

    }





}
