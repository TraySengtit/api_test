package org.ksga.springboot.sahakka.controllers.restcontroller;

import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Result;
import org.ksga.springboot.sahakka.controllers.restcontroller.ServicePostRestController;
import org.ksga.springboot.sahakka.dto.postDto.AllProjectPostDto;
import org.ksga.springboot.sahakka.dto.postDto.FindPostDto;
import org.ksga.springboot.sahakka.dto.postDto.PostDto;
import org.ksga.springboot.sahakka.dto.postDto.UserPost;
import org.ksga.springboot.sahakka.model.post.Comment;
import org.ksga.springboot.sahakka.model.post.Post;
import org.ksga.springboot.sahakka.model.post.PostDeadline;
import org.ksga.springboot.sahakka.model.postService.Category;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.payload.request.ServicePostRequest.PostRequest;
import org.ksga.springboot.sahakka.payload.response.Response;
import org.ksga.springboot.sahakka.security.UserDetailsImp;
import org.ksga.springboot.sahakka.service.PostService;
import org.ksga.springboot.sahakka.service.ServicePostService;
import org.ksga.springboot.sahakka.service.UserService;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.annotation.DeterminableImports;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/v1/post")
public class PostRestController {
    private final PostService postService;
    private  final ServicePostService servicePostService;
    private final UserService userService;
    @Autowired
    public PostRestController(PostService postService , ServicePostService servicePostService, UserService userService) {
        this.postService = postService;
        this.servicePostService= servicePostService;
        this.userService = userService;
    }
    @PostMapping("/createPost")
    public Response<PostDto> creatPost(@AuthenticationPrincipal UserDetailsImp user , @RequestBody PostRequest request){

        try{
            String currentUser= user.getUsername();
            if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();
            try{
                String roleOfUser = servicePostService.roleOfUser(currentUser);
                if (roleOfUser.equalsIgnoreCase(ServicePostRestController.Roles.BUSINESS_OWNER.toString())) {
                    // Here is where we create a new project post
                    Date date = Calendar.getInstance().getTime();
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    String currentDate = dateFormat.format(date);
                    // Get the available
                    String maxPostId = postService.maxPostId();
                    int id =0 ;
                    if (maxPostId!=null){
                        id=Integer.parseInt(maxPostId);
                    }
                    List<String> categories = request.getCategories();
                    // From the database
                    List<Category> allCategories = servicePostService.allCategories();
                    // String all Categories(db) as a string
                    List<String > allStCategories = allCategories.stream()
                            .map(element->element.getCategory_type())
                            .collect(Collectors.toList());
                    // Identifies all invalid categories
                    List<String> invalidCategories = categories.stream()
                            .filter(element->!allStCategories.contains(element.trim()))
                            .collect(Collectors.toList());

                    boolean isNoInvalidCategories = false;

                    // In case we adding the wrong categories
                    if (invalidCategories.size()>0){
                        String allInvalidCategories =  invalidCategories.stream()
                                .map(String::valueOf)
                                .reduce("",(first,second)-> second + "  "+ first);

                         return Response.<PostDto>notFound().setError(allInvalidCategories +"  not exist. Try suggesting or using different one.");
                        // is there is no error
                    }else {
                        isNoInvalidCategories= true;
                    }
                    // posting project
                    try{
                        // adding state to normal and pending automatically
                        if (request.getPostStatus().equalsIgnoreCase("string")) request.setPostStatus("normal");
                        long differentInTime=0;
                        try{
                            differentInTime = findDifference(currentDate,request.getPostDeadline()) ;
                        }catch (Exception ex){
                            System.out.println("Different in Time exception : " + ex.getMessage());
                        }
                        if (request.getPostStatus().equals("normal") && differentInTime<=0 ) request.setPostStatus("pending");

                      PostDto response  = new PostDto();
                        if (categories.size()>0 && isNoInvalidCategories){

                            HashSet<String> requestCategories = new HashSet<>();
                            categories.stream()
                                    .map(requestCategories::add)
                                    .collect(Collectors.toList());
                            List<String>  validCategories =   requestCategories.stream()
                                    .map(String::valueOf)
                                    .collect(Collectors.toList());


                            // Create  Project Post
                            boolean isPostCreated = postService.createProjectPost(currentUser,id+1,request.getTitle(),request.getDescription(),request.getPostImage(),request.getPostDeadline(),currentDate,request.getPostStatus());
                            // Adding categories to the service post
                            for( int i=0; i<validCategories.size(); i++){
                                for(int j =0 ;j<allCategories.size(); j++){

                                    if (validCategories.get(i).trim().equalsIgnoreCase(allCategories.get(j).getCategory_type().trim())){
                                        //Add categories to the post
                                        postService.addCategoryToPost(id+1,allCategories.get(j).getCategory_id());
                                    }
                                }
                            }
                            response.setCategories(request.getCategories());
                        }
                        else {
                            // Just create normal post
                            boolean isPostCreated = postService.createProjectPost(currentUser,id+1,request.getTitle(),request.getDescription(),request.getPostImage(),request.getPostDeadline(),currentDate,request.getPostStatus());
                        }
                        // response to user
                        User author = userService.findUserByEmail(currentUser);


                        response.setPostId(id+1);
                        response.setTitle(request.getTitle());
                        response.setDescription(request.getDescription());
                        response.setPostImage(request.getPostImage());
                        response.setPostDeadline(request.getPostDeadline());
                        response.setPostStatus(request.getPostStatus());
                        response.setLikes(0);
                        response.setCreateDate(currentDate);

                        // Author
                        response.setUserId(author.getUser_id());
                        response.setAuthor(author.getUsername());
                        response.setProfileImage(author.getProfile_image());

                        return Response.<PostDto>successCreate().setPayload(response);
                    }catch (Exception ex){

                        System.out.println("Created Post exception: " + ex.getMessage());
                        return Response.<PostDto>exception().setError("Exception Occur! Failed to create new service post....");
                    }
               }else {
                    return Response.<PostDto>badRequest().setError(currentUser+"is a freelancer. Cannot perform this action");
                }
            }catch (Exception sqlException){
                System.out.println("sql Exception : "+ sqlException.getMessage());
                return Response.<PostDto>exception().setError("Failed to create project post. Exception occurs...");
            }

        }catch (Exception authException){
            return Response.<PostDto>exception().setError("Authentication Error.Authentication is needed to complete this process. ");

        }
    }
    @PutMapping("{id}/updatePost")
    public Response<PostDto> updateProjectPost(@AuthenticationPrincipal UserDetailsImp user , @PathVariable int id , @RequestBody PostRequest request ){

        try{
            String currentUser = user.getUsername();
            if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();

            try {
                String roleOfUser = servicePostService.roleOfUser(currentUser);
                System.out.println("Here is the role of the user : "+roleOfUser);
                if (roleOfUser.equalsIgnoreCase(ServicePostRestController.Roles.BUSINESS_OWNER.toString())) {
                    int numberOfOwnerPost= postService.countUserPost(currentUser);
                    if (numberOfOwnerPost>0){
                        // Do the updating process.

                        String postOwnerId = postService.verifyPostOwnership(currentUser,id);
                        if (postOwnerId!=null){
                            // Proceed the update process
                            // Here is where we create a new project post
                            Date date = Calendar.getInstance().getTime();
                            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            String currentDate = dateFormat.format(date);

                            List<String> categories = request.getCategories();
                            // From the database
                            List<Category> allCategories = servicePostService.allCategories();
                            // String all Categories(db) as a string
                            List<String > allStCategories = allCategories.stream()
                                    .map(element->element.getCategory_type())
                                    .collect(Collectors.toList());
                            // Identifies all invalid categories
                            List<String> invalidCategories = categories.stream()
                                    .filter(element->!allStCategories.contains(element.trim()))
                                    .collect(Collectors.toList());
                            boolean isNoInvalidCategories = false;
                            // In case we adding the wrong categories
                            if (invalidCategories.size()>0){
                                String allInvalidCategories =  invalidCategories.stream()
                                        .map(String::valueOf)
                                        .reduce("",(first,second)-> second + "  "+ first);

                                return Response.<PostDto>badRequest().setError(allInvalidCategories +"  not exist. Try suggesting or using different one.");
                                // is there is no error
                            }else {
                                isNoInvalidCategories= true;
                            }
                            // Updating project post
                           try{
                            // adding state to normal and pending automatically
                               if (request.getPostStatus().equalsIgnoreCase("string")) request.setPostStatus("normal");
                               long differentInTime=0;
                               try{
                                   differentInTime = findDifference(currentDate,request.getPostDeadline()) ;
                               }catch (Exception ex){
                                   System.out.println("Different in Time exception : " + ex.getMessage());
                               }
                               if (request.getPostStatus().equals("normal") && differentInTime<=0 ) request.setPostStatus("pending");



                                PostDto response  = new PostDto();
                                boolean isPostUpdated=false;
                                if (categories.size()>0 && isNoInvalidCategories){

                                    HashSet<String> requestCategories = new HashSet<>();
                                    categories.stream()
                                            .map(requestCategories::add)
                                            .collect(Collectors.toList());
                                    List<String>  validCategories = requestCategories.stream()
                                            .map(String::valueOf)
                                            .collect(Collectors.toList());
                                    //Check the created date :

                                    isPostUpdated= postService.updateProject(id,request.getTitle(),request.getDescription(),currentUser,request.getPostImage(),request.getPostDeadline(),request.getPostStatus());

                                    //delete all category and add the new one
                                    boolean isDeletePostCategory = postService.deletePostCategory(id);
                                    System.out.println("Value of isDeletePostCategory : "+isDeletePostCategory);

                                    // Update categories to the service post
                                    for( int i=0; i<validCategories.size(); i++){
                                        for(int j =0 ;j<allCategories.size(); j++){
                                            if (validCategories.get(i).trim().equalsIgnoreCase(allCategories.get(j).getCategory_type().trim())){
                                                //update categories to the post
                                                //postService.updateCategoryPost(allCategories.get(j).getCategory_id(),id);
                                                postService.addCategoryToPost(id,allCategories.get(j).getCategory_id());
                                            }
                                        }
                                    }
                                    response.setCategories(request.getCategories());
                                }
                                else {
                                    // Update  Project Post
                                    isPostUpdated= postService.updateProject(id,request.getTitle(),request.getDescription(),currentUser,request.getPostImage(),request.getPostDeadline(),request.getPostStatus());

                                }
                                // response to user
                                User author = userService.findUserByEmail(currentUser);
                                String numberOfLike = postService.likeOfPost(id);

                                int likeOfPost = 0;
                                if (numberOfLike!=null){
                                    likeOfPost = Integer.parseInt(numberOfLike);
                                }
                                // Double Check on this again.
                                response.setPostId(id);
                                response.setTitle(request.getTitle());
                                response.setDescription(request.getDescription());
                                response.setPostImage(request.getPostImage());
                                response.setPostDeadline(request.getPostDeadline());
                                response.setPostStatus(request.getPostStatus());
                                response.setLikes(likeOfPost);
                                response.setCreateDate(currentDate);
                                // Author
                                response.setUserId(author.getUser_id());
                                response.setAuthor(author.getUsername());
                                response.setProfileImage(author.getProfile_image());

                                return Response.<PostDto>updateSucess().setPayload(response);

                            }catch (Exception ex){
                                System.out.println("Update Post exception: " + ex.getMessage());
                                return Response.<PostDto>exception().setError("Exception Occur! Failed to create new service post....");
                            }
                        }else {
                            return Response.<PostDto>notFound().setError("Provided ID doesn't exist for this account.");
                        }

                    }else return Response.<PostDto>notFound().setError("This account doesn't have any posts yet..");
               }else
                    return Response.<PostDto>badRequest().setError(currentUser + " is a freelancer.Only Business Owner can perform this action");
         }catch (Exception sqlException){
                System.out.println("Sql Exception "+ sqlException.getMessage());
                return Response.<PostDto>exception().setError("Failed to update the project post. Exception occurs...");
            }
        }catch (Exception authException){
            System.out.println("Authentication Error : " + authException.getMessage());
            return Response.<PostDto>exception().setError("Authentication Error. Authentication is needed in order to complete this process.");
        }
    }
    @DeleteMapping("/{id}/deletePost")
    public Response<PostDto>deleteById(@AuthenticationPrincipal UserDetailsImp user, @PathVariable int id){
        try {
            String currentUser = user.getUsername();
            if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();
                        try {
                            String roleOfUser = servicePostService.roleOfUser(currentUser);
                            if (roleOfUser.equalsIgnoreCase(ServicePostRestController.Roles.BUSINESS_OWNER.toString())) {
                                // Let user know if there is no post available
                                int numberOfOwnerPost= postService.countUserPost(currentUser);
                                if (numberOfOwnerPost > 0){
                                    String postOwnerId=postService.verifyPostOwnership(currentUser,id);
                                    if (postOwnerId!=null){
                                        // Delete all the comments like and reply
                                        boolean isLikeDeleted= postService.deletePostLike(id);
                                        boolean isReplyDeleted = postService.deleteReplies(id);
                                        boolean isCommentDeleted=postService.deleteComments(id);
                                        boolean isPostCategoryDeleted = postService.deletePostCategory(id);
                                        boolean isPostDeleted= postService.deletePost(id);
                                        return Response.<PostDto>successDelete().setError("Project Post has been deleted successfully .");
                                    }else
                                    {
                                        return Response.<PostDto>notFound().setError("Project Post with provided id doesn't exist in this account..");
                                    }
                                }else {
                                    return Response.<PostDto>badRequest().setError("This account doesn't have any posts yet.");
                                }
                            }else
                                return Response.<PostDto>badRequest().setError(currentUser + " is not a business owner.Only Business Owner user can perform this action.");

                        }catch (Exception exception){
                            System.out.println("Exception Occurs: "+ exception.getMessage());
                            return Response.<PostDto>exception().setError("Exception Occur. Failed to delete the project post.");
                        }
                }catch (Exception authException){
                      System.out.println("Authentication Error");
                    return Response.<PostDto>exception().setError("Authentication Error. Authentication is needed to complete this process.");
        }
    }
    @PostMapping("/{id}/comment")
    public Response<PostDto> comment(@AuthenticationPrincipal UserDetailsImp user, @PathVariable int id, @RequestParam String comments){

        try {
            String currentUser = user.getUsername();
            if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();
         try{
                int commentId = 0;
                String maxCommentId = postService.maxCommentId();
                if (maxCommentId!=null){
                    commentId = Integer.parseInt(maxCommentId);
                }

                 Date date = Calendar.getInstance().getTime();
                 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                 String currentDate = dateFormat.format(date);


                 boolean isCommentAdded = postService.comment(commentId+1,currentUser,id,comments,currentDate);

                if (isCommentAdded){
                    return Response.<PostDto>successCreate().setError("Comment Added successfully.");

                }else return Response.<PostDto>badRequest().setError("Provided Post Id doesn't exist. Try different one.");


            }catch (Exception sqlException){
                System.out.println("SQL Exception : "+ sqlException.getMessage());
                return Response.<PostDto>exception().setError("Failed to comment on this post. Exception occurs...");
            }

        }catch (Exception authException){
            System.out.println("Authentication Error : " + authException.getMessage());
            return Response.<PostDto>exception().setError("Authentication Error. ");
        }
    }
    @PostMapping("/{postId}/reply")
    public Response<PostDto>reply(@AuthenticationPrincipal UserDetailsImp user, @PathVariable int postId,@RequestParam int commentId, @RequestParam String comments){
        try{

            String currentUser = user.getUsername();
            if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();
                try {
                        // try postId to see if it exist or not
                        int verifyPost = postService.verifyPostId(postId);
                        if (verifyPost>0){
                            //Verify Comments of the post
                            int verifyComment = postService.countComment(postId);
                            if (verifyComment>0){
                                // verityComment id
                                String verifyCommentId = postService.verifyCommentId(commentId);
                                if (verifyCommentId!=null){
                                    int availableId = 0;
                                    String maxCommentId = postService.maxCommentId();
                                    if (maxCommentId!=null){
                                        availableId = Integer.parseInt(maxCommentId);
                                    }
                                    boolean isReplyAdded = postService.replyComment(availableId+1,currentUser,postId,commentId,comments);
                                    if (isReplyAdded)
                                        return Response.<PostDto>successCreate().setError( "Reply added successfully.");
                                    else return Response.<PostDto>exception().setError("Failed to create a reply on this comment.");
                                }else{
                                  return Response.<PostDto>notFound().setError("Comment with provided id doesn't exist .");
                                }
                            }else return Response.<PostDto>notFound().setError("There is no comment for this post yet.");
                        }else
                            return Response.<PostDto>notFound().setError("Project post with the provided id doesn't exit.Cannot reply.");

              }catch (Exception sqlException){
                        System.out.println("sqlException Message : "+sqlException.getMessage());
                        return Response.<PostDto> exception().setError("Failed to reply on the comment... SQL Exception Occurs");
                    }

        }catch (Exception authException){
            System.out.println("Authentication Error ");
            return  Response.<PostDto>exception().setError("Authentication Error. Authentication is needed to perform this action.");
        }
    }
    @PostMapping("/{postId}/react")
    public Response<Integer> react(@AuthenticationPrincipal UserDetailsImp user, @PathVariable int postId){

        try{
            String currentUser = user.getUsername();
            if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();
            try{
                //check to see it the post exist or not
                int verifyPostId = postService.verifyPostId(postId);
                if (verifyPostId==0) return Response.<Integer>notFound().setError("Post with the provided id doesn't exist.");
                // check to see if this user like the post or not
                String reactId = postService.reactId(currentUser,postId);

             //   System.out.println("Here is the value of reactId : "+reactId);

                if (reactId!=null){
                        // unlike the post
                    boolean isUnliked=postService.unlikePost(currentUser,Integer.parseInt(reactId));

                    if (isUnliked)
                    {
                        int countLikes= postService.countLikes(postId);
                        boolean isUpdatedLike= postService.updatePostLike(countLikes,postId);

                        return Response.<Integer>updateSucess().setPayload(countLikes).setError("Unlike the project post successfully!");
                    }
                    else
                        return  Response.<Integer>exception().setError("Failed to unlike the project post.Exception occurs.");
                }else {
                    // like the post
                    String availableId = postService.maxLikeId();
                    int id=0;
                    if (availableId!=null) id=Integer.parseInt(availableId);
                    boolean isLiked= postService.likePost(currentUser,id+1,postId);
                    if (isLiked){
                            // adding like to the project post
                               int countLikes= postService.countLikes(postId);
                                System.out.println("When the post is like the count like value : "+ countLikes);
                               boolean isUpdatedLike= postService.updatePostLike(countLikes,postId);

                             //Integer numberOfLikes =postService.countLikeOfServicePost(postId);



                        return Response.<Integer>updateSucess().setPayload(countLikes).setError("Like the project post successfully.");
                    }
                    else return Response.<Integer>exception().setError("Failed to like the post.");
                }

            }catch(Exception sqlException){
                System.out.println("SQL Exception : " + sqlException.getMessage());
                return Response.<Integer>exception().setError("Failed to react on the project post.Exception Occurs.. ");
            }
        }catch (Exception authException){
            System.out.println( "Authentication Error : " + authException.getMessage());
            return Response.<Integer>exception().setError("Authentication Error. Authentication is needed to perform this action.");
        }

    }
    @GetMapping("/{postId}/findPostById")
    public Response<FindPostDto>findById(@AuthenticationPrincipal UserDetailsImp user, @PathVariable int postId){

        try{

            String currentUser = "default";
            if (user!=null)
                    currentUser = user.getUsername();

            boolean isPostOwner =false;
           String postOwnership=" ";
            if (currentUser!=null  ){

               postOwnership = postService.verifyPostOwnership(currentUser,postId);
               if (postOwnership!=null) isPostOwner=true;
            }
            //System.out.println("IsPostOwner : "+isPostOwner);

            // need to check to see if he the owner of the post
            Post resultedPost= postService.findPostById(postId);
            if (resultedPost==null) return Response.<FindPostDto>exception().setError("Post with the provided id doesn't exist.");

            User author = userService.findUserById(resultedPost.getUser_id());
            List<Comment> allComment=   postService.allCommentOfPost(postId);

            List<String> socialMediaLinks=postService.linksUser(resultedPost.getUser_id());
            List<org.ksga.springboot.sahakka.model.category.Category> postCategory = postService.categoryOfPost(postId);

           List<String> stringPostCategory =  postCategory.stream()
                    .map(e->e.getCategory_type())
                  .collect(Collectors.toList());

           UserPost postAuthor = new UserPost();
                   postAuthor.setSocial_media(socialMediaLinks);
                    postAuthor.setUsername(author.getUsername());
                    postAuthor.setProfile_image(author.getProfile_image());
                    postAuthor.setBio(author.getBio());
                    postAuthor.setUser_id(author.getUser_id());



            FindPostDto response = new FindPostDto();
            //Post
            response.setPostId(resultedPost.getPost_id());
            response.setPostImage(resultedPost.getPost_image());
            response.setDescription(resultedPost.getDescription());
            response.setPostDeadline(resultedPost.getPost_deadline());
            response.setPostStatus(resultedPost.getPost_status());
            response.setCreateDate(resultedPost.getCreated_date());
            response.setLikes(resultedPost.getLikes());
            response.setTitle(resultedPost.getTitle());

            response.setPostOwner(isPostOwner);

            response.setCategories(stringPostCategory);
            response.setComments(allComment);
            response.setUser(postAuthor);

            return Response.<FindPostDto>ok().setPayload(response) ;
        }catch (Exception sqlException){
            System.out.println(" SQL Exception Occurs : " + sqlException.getMessage());
            return Response.<FindPostDto>exception().setError("Failed to find post. SQL Exception occurs ");
        }

    }
    enum PostTypes{
        NORMAL,PENDING,DRAFT
    }
    @GetMapping("/ownerGetAllPost")
    public Response<List<AllProjectPostDto>> ownerGetAllPosts(@AuthenticationPrincipal UserDetailsImp user ,@RequestParam(defaultValue = "10") int limit ,@RequestParam(defaultValue = "1") int page,@RequestParam(defaultValue = "NORMAL") PostTypes postType){

        try {
            String currentUser= user.getUsername();
            if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();
            try{
                //check role cause this is for the business owner user only .
                String roleOfUser = servicePostService.roleOfUser(currentUser);
                if (roleOfUser.equalsIgnoreCase(ServicePostRestController.Roles.BUSINESS_OWNER.toString())) {
                    int offset = (page -1) * limit;
                    Paging paging = new Paging();
                    paging.setPage(page);
                    paging.setLimit(limit);

                    int totalCount =  postService.countOwnerProjectPosts(currentUser,postType.toString().toLowerCase());
                    paging.setTotalCount(totalCount);
                    List<AllProjectPostDto> response = postService.findOwnerProjectPosts(currentUser,limit,offset,postType.toString().toLowerCase());
                    return Response.<List<AllProjectPostDto>>ok().setPayload(response).setMetadata(paging);

                }else return Response.<List<AllProjectPostDto>>badRequest().setError(currentUser+ " is a freelancer. Only business owner can perform this action.");

            }catch (Exception sqlException ){
                System.out.println("SQL Exception : "+sqlException.getMessage());
                return Response.<List<AllProjectPostDto>>exception().setError("Failed to get the project post.Exception occurs.");
            }

        }catch (Exception authException){

            System.out.println("Authentication Error : "+ authException.getMessage());
            return Response.<List<AllProjectPostDto>>exception().setError("Failed to get all posts due to the authentication exception");

        }

    }
    @GetMapping("/getAllPosts")
    public Response<List<AllProjectPostDto>>getAllPosts(@RequestParam(required = false) String category,@RequestParam(required = false) String username, @RequestParam(required = false,defaultValue = "10") int limit , @RequestParam(required = false,defaultValue = "1") int page ){

        try{
            Paging paging = new Paging();
            paging.setPage(page);
            paging.setLimit(limit);
            int offset = (page -1) * limit;

            if ((category==null || category.trim().equals(""))&& username!=null && !username.trim().equals("") )
            {
                System.out.println("There is a value for the username: "+username);

                int totalCount= postService.countProjectsByUsername(username);
                paging  .setTotalCount(totalCount);
                List<AllProjectPostDto>response = postService.findProjectPostByUsername(username,limit ,offset);

                return Response.<List<AllProjectPostDto>>ok().setPayload(response).setMetadata(paging);

            }else if ( (username==null|| username.equals("")) && category!=null && !category.trim().equals("")   ){
                    //Category is not NUll and the username is null

                int totalCount=postService.countProjectPostByCategory(category);
                paging.setTotalCount(totalCount);
                List<AllProjectPostDto> response = postService.findProjectByCategory(category,limit,offset);

                 return Response.<List<AllProjectPostDto>>ok().setPayload(response).setMetadata(paging);
            } else if (category!=null && username!=null && !username.trim().equals("")  && !category.trim().equals("") ){
                    // Category and username are not null
                int totalCount=postService.countPostByUsernameAndCategory(username,category);
                paging.setTotalCount(totalCount);
                List<AllProjectPostDto> response = postService.findProjectPostByUsernameAndCategory(username,category,limit,offset);

                return Response.<List<AllProjectPostDto>>ok().setPayload(response).setMetadata(paging);

            }else {
              // Category and username is null

                int totalCount =postService.countNormalProjectPosts();

                List<AllProjectPostDto> response = postService.findAllNormalProjectPosts(limit ,offset) ;
                paging.setTotalCount(totalCount);
                return Response.<List<AllProjectPostDto>>ok().setPayload(response).setMetadata(paging);
            }

        }catch (Exception sqlException){
            System.out.println("SQL Exception : "+sqlException.getMessage());
            return Response.<List<AllProjectPostDto>>exception().setError("Failed to get project post. SQL Exception occurs ");
        }
    }
    @PostMapping("/{id}/reportPost")
    public Response<String> reportPost(@AuthenticationPrincipal UserDetailsImp user,@RequestParam String reason , @PathVariable int id){
        try{

            String currentUser= user.getUsername();
            if (currentUser.equals("anonymousUser") || currentUser.equals("") || currentUser== null) throw new Exception();
            try{
                String roleOfUser = servicePostService.roleOfUser(currentUser);
                if (roleOfUser.equalsIgnoreCase(ServicePostRestController.Roles.FREELANCER.toString())) {
                    int availableReportId=0;
                    String maxReportId = postService.maxReportId();
                    if (maxReportId!=null) availableReportId = Integer.parseInt(maxReportId) ;
                    Boolean addReport = false;

                    try {
                        addReport=true;

                        Date date = Calendar.getInstance().getTime();
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        String currentDate = dateFormat.format(date);

                        addReport= postService.createReport(availableReportId+1,id,currentUser,reason,currentDate);
                    }catch (Exception ex){
                        addReport=false;
                        System.out.println("Here is the value of exceptoin : "+ex .getMessage());
                    }
                   return addReport? Response.<String>successCreate().setError("Successfully report this project post."): Response.<String >notFound().setError("Provided Id doesn't exist.");
                }else return Response.<String>badRequest().setError(currentUser+ " is a Business Owner account.Only Freelancer account can perform this operation.");
            }catch (Exception sqlException){
                System.out.println("SQL Exception Occurs: "+sqlException.getMessage());
                return Response.<String>exception().setError("Failed to report this service post . SQL Exception occurs.");
            }
        }catch (Exception authException ){
            System.out.println("Authentication Error : "+ authException.getMessage());
            return Response.<String>exception().setError("Authentication is needed in order to complete this process.");
        }


    }

    @Scheduled(fixedRate = 5000L)
    void updateProjectPost(){
        // Here we will change the status of the post to pending
        try{

            // first we need to get the date from the database
            Date date = Calendar.getInstance().getTime();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String currentDate = dateFormat.format(date);
       /*     String startDate="2021-08-11 08:32:56";

            System.out.println(" Here is the current Date : "+currentDate);
            long differentInTime = findDifference(startDate,currentDate);
            System.out.println(" Here the different Date : "+ differentInTime);
*/
            // Done
            try{
                List<PostDeadline> allDeadline = postService.allProjectPostDeadline();

          List<PostDeadline> updatablePost=  allDeadline.stream()
                        .filter(e-> findDifference(e.getPost_deadline(),currentDate)>0)
                        .collect(Collectors.toList());

          List<Integer> updatableDeadlineId = updatablePost.stream().map( e -> e.getPost_id()).collect(Collectors.toList());

          for (int i =0; i<updatableDeadlineId.size(); i++){
                    postService.updatePostStatus(updatableDeadlineId.get(i));
          }
            }catch (Exception sqlException){
                System.out.println("SQL Exception on automatically pending the project : "+sqlException);

            }

        }catch (Exception ex){
            System.out.println("Here is the error message on auto pending : "+ex.getMessage());
        }


    }

    static long findDifference(String startDate , String endDate){
        //HH:mm:ss
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try{
            Date firstDate = dateFormat.parse(startDate);
            Date secondDate = dateFormat.parse(endDate) ;
            // calculate the the different
            // in milliseconds
            long  differentInTime = secondDate.getTime() - firstDate.getTime();
          long differentInSecond=(differentInTime/1000) ;

            return differentInSecond;

        }catch (ParseException e){
            System.out.println("Here is the error message on: "+e.getMessage());
            return 0;
        }
    }

    @EnableScheduling
    @Configuration
    @ConditionalOnProperty(name = "scheduling.enabled",matchIfMissing = true)
    class ScheduleConfiguration{


    }
}
