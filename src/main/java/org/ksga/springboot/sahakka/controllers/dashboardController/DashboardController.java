package org.ksga.springboot.sahakka.controllers.dashboardController;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.security.UserDetailsImp;
import org.ksga.springboot.sahakka.service.UserService;
import org.ksga.springboot.sahakka.service.serviceImp.adminDashboardService.DashboardService;
import org.mapstruct.control.MappingControl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DashboardController {


    @Autowired
    private DashboardService dashboardService;

    @Autowired
    private UserService userService;


    @GetMapping("/")
    public String dashboardController(Model model , @AuthenticationPrincipal UserDetailsImp user ){
        Long countFreeLancer = dashboardService.CountFreelancer();
        Long countOwner = dashboardService.CountOwner();
        Long total = countFreeLancer + countOwner;

        if (user !=null){
            String currentUser = user.getUsername();
            if (!user.getUsername().equalsIgnoreCase("anonymousUser") )
            {
                User loginUser = userService.findUserByEmail(currentUser);
                model.addAttribute("loginUser",loginUser);
            }
        }

        model.addAttribute("countFreelancer", countFreeLancer);
        model.addAttribute("countOwner", countOwner);
        model.addAttribute("totalUser", total);
        return "dashboard/index";
    }

    @GetMapping("/login")
    public String loginDashboardController(Model model){

        return "form_login/formLogin";
    }



}
