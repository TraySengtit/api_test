package org.ksga.springboot.sahakka.controllers.dashboardController;

import ch.qos.logback.core.net.SyslogOutputStream;
import org.apache.ibatis.annotations.Delete;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.security.UserDetailsImp;
import org.ksga.springboot.sahakka.service.UserService;
import org.ksga.springboot.sahakka.utilities.Paging;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.swing.*;
import javax.swing.text.DateFormatter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.zip.DataFormatException;

@Controller
@RequestMapping("/admin")
public class AdminController {
    UserService userService;
    public  AdminController(UserService userService){
        this.userService = userService ;
    }
    @GetMapping("/allAdmin")
    public String allAdmins(@AuthenticationPrincipal UserDetailsImp user, Model model, String search_query, Paging paging){

        try{
            long countAdmins = userService.countAllAdmin();
            List<User> admins = userService.allAdmins(search_query,paging);
            int totalCount =(int) countAdmins;
            if (search_query != null)
            totalCount = (int)  userService.countSearchedAdmin(search_query);


            if (user !=null){
                String currentUser = user.getUsername();
                if (!user.getUsername().equalsIgnoreCase("anonymousUser") )
                {
                    User loginUser = userService.findUserByEmail(currentUser);
                    model.addAttribute("loginUser",loginUser);
                }
            }

            /*System.out.println(" Here is the value of total count : "+totalCount);
            System.out.println(" Page to show :  "+paging.getPagesToShow());*/

            //got the totalCount;
                paging.setTotalCount(totalCount);
                model.addAttribute("message",model.asMap().get("message"));
                model.addAttribute("countAdmins", countAdmins);
                model.addAttribute("admins", admins);
                model.addAttribute("paging",paging);
                model.addAttribute("search_query", search_query);

        }catch (Exception ex){
            System.out.println("All Admin Error : "+ ex.getMessage());
        }
        return "admin/admin";
    }

    @GetMapping("/{id}/viewAdmin")
    public String viewAdmin(@AuthenticationPrincipal UserDetailsImp author,   Model model,@PathVariable int id){
        // Get the user and pass it to the model
        User user = userService.findUserById(id);

        if (author !=null){
            String currentUser = author.getUsername();
            if (!currentUser.equalsIgnoreCase("anonymousUser") )
            {
                User loginUser = userService.findUserByEmail(currentUser);
                model.addAttribute("loginUser",loginUser);
            }
        }

        model.addAttribute("message",model.asMap().get("message"));
        model.addAttribute("password", "");
        model.addAttribute("admin",user);


        return "/admin/viewAdmin";
    }
    /*@ModelAttribute*/
    @GetMapping("/{id}/blockAdmin")
    public String blockAdmin( @AuthenticationPrincipal UserDetailsImp user,@PathVariable int id,   String password , RedirectAttributes redirectAttributes ) {

        String message = "";
        if (user !=null){
            String currentUser = user.getUsername();
            if (!currentUser.equalsIgnoreCase("anonymousUser") )
            {
                User loginUser = userService.findUserByEmail(currentUser);
                BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
                if (encoder.matches(password,loginUser.getPasswords())){
                    //Perform Delete action :
                    //Verify the admin:

                    String adminEmail = userService.adminEmail(id);
                    Boolean adminDeleted =  userService.deleteAdmin(id);
                    if (adminEmail.equals(currentUser)){
                        // Delete his down account
                        System.out.println("Admin Delete his own account.");
                        message = "Successfully deleted this admin";
                        redirectAttributes.addFlashAttribute("message",message);
                     return adminDeleted? "redirect:/logout" : "redirect:/admin/allAdmin"   ;
                    }else {
                        System.out.println("Admin delete other account .");
                        message="Successfully deleted this admin";
                        redirectAttributes.addFlashAttribute("message",message);
                        return   "redirect:/admin/allAdmin" ;
                    }

                }else {
                    System.out.println("password : "+password);
                    System.out.println("Failed to delete  : Wrong password ");
                    message="Wrong Password.Failed to delete . ";
                    redirectAttributes.addFlashAttribute("message",message);
                    return "redirect:/admin/allAdmin";
                }

            }else return "redirect:/logout";
        }else return "redirect:/logout";


    }

    @GetMapping("/{id}/resetAdminPassword")
    public String resetPassword(@AuthenticationPrincipal UserDetailsImp user, @PathVariable int id ,String oldPassword, String newPassword,String confirmPassword ,RedirectAttributes redirectAttributes){
        String message ="";
        if (user!=null){
              String currentUser = user.getUsername();
            if (!currentUser.equalsIgnoreCase("anonymousUser") )
            {
                User loginUser = userService.findUserByEmail(currentUser);
                    BCryptPasswordEncoder decoder = new BCryptPasswordEncoder();
                        if (decoder.matches(oldPassword,loginUser.getPasswords()))
                        {
                            if (newPassword.equals(confirmPassword)){
                                // reset the password
                                Boolean updateAdminPassword =  userService.updateAdminPassword(decoder.encode(newPassword),id);
                                String adminEmail = userService.adminEmail(id);

                                if (updateAdminPassword)
                               return (adminEmail.equals(currentUser)) ? "redirect:/logout":"redirect:/admin/allAdmin";
                                else
                                {
                                    System.out.println("Failed to update the admin credential");
                                    message="Failed to update the admin credential.Exception occurs";
                                    redirectAttributes.addFlashAttribute("message",message);
                                    return "redirect:/admin/allAdmin";
                                }

                            }else
                            {
                                System.out.println("Your new password and confirm password don't match up .");
                                message="Your new password and confirm password don't match up .";
                                redirectAttributes.addFlashAttribute("message",message);
                                return "redirect:/admin/"+loginUser.getUser_id()+"/viewAdmin";
                            }
                        }else {
                            System.out.println("Wrong password");
                            message="Wrong Credential. Failed to reset the password ";
                            redirectAttributes.addFlashAttribute("message",message);
                            return "redirect:/admin/"+loginUser.getUser_id()+"/viewAdmin";
                        }
            }else return "redirect:/logout";
        }
        else    return "redirect:/logout";
    }

    // Send data to newAdminHandler through the form
    @GetMapping("/addNewAdmin")
    public String addAdminPage(@AuthenticationPrincipal UserDetailsImp user,Model model){

        if (user !=null){
            String currentUser = user.getUsername();
            if (!user.getUsername().equalsIgnoreCase("anonymousUser") )
            {
                User loginUser = userService.findUserByEmail(currentUser);
                model.addAttribute("loginUser",loginUser);
            }
        }
        model.addAttribute("newAdmin",new User());
        return "admin/addAdmin";
    }
    @GetMapping("/addNewAdminHandler")
    public String addNewAdmin(@AuthenticationPrincipal UserDetailsImp user , User newAdmin , String verifyPassword ,RedirectAttributes redirectAttributes){
        String message = "";
  if (user!=null){
            String currentUser = user.getUsername();
            if (!currentUser.equalsIgnoreCase("anonymousUser") ) {

                User loginUser = userService.findUserByEmail(currentUser) ;
                BCryptPasswordEncoder encoder = new BCryptPasswordEncoder( );
                if ( encoder.matches(verifyPassword,loginUser.getPasswords())){

                    // We have to check if the user is already existed;
                    // Check to see if th email is already existed

                   User existedUser = userService.findUserByEmail(newAdmin.getEmail());

                   if (existedUser!=null) {

                       System.out.println("This email is already taken. Try different one.");
                       message="this email is already taken.Failed to create an admin.";
                        redirectAttributes.addFlashAttribute("message", message);
                       return "redirect:/admin/allAdmin" ;
                   }

                    Date date = Calendar.getInstance().getTime();
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    String currentDate = dateFormat.format(date);
                    // Start adding new admin
                    int availableId = 0;
                    String maxUserId = userService.maxUserId();

                    newAdmin.setGender(newAdmin.getGender().equals("1")?  "Male"  :  "Female");

                    if(newAdmin.getProfile_image().equals(""))
                       // newAdmin.setProfile_image("http://103.29.70.46:15001/files/184cbe17-e453-4e29-8dc1-5c0718ec9491.png");
                        newAdmin.setProfile_image("https://www.pngkey.com/png/full/73-730477_first-name-profile-image-placeholder-png.png");


                    if (maxUserId!= null) availableId = Integer.parseInt(maxUserId);
                    Boolean isUserCreated=userService.addNewUser(availableId+1,newAdmin.getUsername(),newAdmin.getEmail(),encoder.encode(newAdmin.getPasswords()),"NORMAL",false,newAdmin.getProfile_image(),newAdmin.getEmail_address(),newAdmin.getTelephone(),currentDate,newAdmin.getBio(),newAdmin.getGender(),newAdmin.getAddress(),newAdmin.getNationality());

                    //Find available role id:
                    int userRoleId = 0;
                    String availableRoleId = userService.maxUserRoleId();

                    if (availableRoleId!=null) userRoleId= Integer.parseInt(availableRoleId);
                     Boolean addRoleAdmin=userService.addRoleToUser(userRoleId+1,availableId+1,3);
                    if (!isUserCreated){

                        System.out.println("Failed To Created User : ");
                        message= "Failed to create the admin. Exception occurs";
                        redirectAttributes.addFlashAttribute("message", message);
                    }
                    message = "New Admin has been created successfully";
                    redirectAttributes.addFlashAttribute("message", message);

                    return "redirect:/admin/allAdmin";
                    }else {
                    System.out.println("Wrong Credential! Failed to create a new admin account.");
                    message="Wrong Credential! Failed to create a new admin account.";
                    redirectAttributes.addFlashAttribute("message", message);

                    return "redirect:/admin/allAdmin";
                }
               }else {
                 return "redirect:/logout";
                }
             }else  return "redirect:/logout";
       }

    @GetMapping("/{id}/updateAdmin")
    public String updateAdminPage(@AuthenticationPrincipal UserDetailsImp user,Model model,@PathVariable int id){
        // Get the user and pass it to the model
        User updateUser = userService.findUserById(id);
        updateUser.setGender(updateUser.getGender().equalsIgnoreCase("Male")?  "1"  :  "2");
        if (user  !=null){
            String currentUser = user.getUsername();
            if (!currentUser.equalsIgnoreCase("anonymousUser") )
            {
                User loginUser = userService.findUserByEmail(currentUser);
                model.addAttribute("loginUser",loginUser);
            }
        }
        model.addAttribute("updateAdmin",updateUser);
        return "admin/updateAdmin";
    }

    @GetMapping("/updateAdminHandler")
    public String updateAdmin(@AuthenticationPrincipal UserDetailsImp user,User updateAdmin ,String password ,RedirectAttributes redirectAttributes)
    {
        String message = "";
        if (user!=null){
            String currentUser = user.getUsername();
            if (!currentUser.equalsIgnoreCase("anonymousUser") ) {
                User loginUser = userService.findUserByEmail(currentUser) ;
                BCryptPasswordEncoder encoder = new BCryptPasswordEncoder( );
                if ( encoder.matches(password,loginUser.getPasswords())){

                    updateAdmin.setGender(updateAdmin.getGender().equals("1")?  "Male"  :  "Female");

                    if(updateAdmin.getProfile_image().equals("")|| updateAdmin.getEmail_address()== null)
                        updateAdmin.setProfile_image("http://103.29.70.46:15001/files/184cbe17-e453-4e29-8dc1-5c0718ec9491.png");

             Boolean isUserUpdated = userService.updateUser(updateAdmin.getUser_id(),updateAdmin.getUsername(),updateAdmin.getProfile_image(),updateAdmin.getEmail_address(),updateAdmin.getTelephone(),updateAdmin.getBio(),updateAdmin.getGender(),updateAdmin.getAddress(),updateAdmin.getNationality());
                  //  Boolean isUserCreated=  userService.addNewUser(availableId+1,newAdmin.getUsername(),newAdmin.getEmail(),encoder.encode(newAdmin.getPasswords()),"NORMAL",false,newAdmin.getProfile_image(),newAdmin.getEmail_address(),newAdmin.getTelephone(),currentDate,newAdmin.getBio(),newAdmin.getGender(),newAdmin.getAddress(),newAdmin.getNationality());

                    if (!isUserUpdated){
                        System.out.println("Failed To Update User : ");
                        message="Failed  to update user! Exception occurs.";
                        redirectAttributes.addFlashAttribute("message",message);
                    }


                    System.out.println("Updated this admin account successfully!");
                    message="Updated this admin account successfully!";

                    redirectAttributes.addFlashAttribute("message",message);
                    return "redirect:/admin/allAdmin";
                }else {
                    System.out.println("Authorization Failed : ");
                    message="Wrong credential! Failed to update this admin account.";

                    redirectAttributes.addFlashAttribute("message",message);
                    return "redirect:/admin/allAdmin";
                }
            }else {
                System.out.println("Authorization Failed.");
                message = "Authorization Failed!" ;
                redirectAttributes.addFlashAttribute("message",message);
                return "redirect:/logout";
            }
        }else
        {
            System.out.println("Authorization Failed.");
            message = "Authorization Failed!" ;
            redirectAttributes.addFlashAttribute("message",message);
            return "redirect:/logout";
        }



    }




}
