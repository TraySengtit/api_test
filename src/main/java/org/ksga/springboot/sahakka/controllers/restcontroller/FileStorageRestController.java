package org.ksga.springboot.sahakka.controllers.restcontroller;

import org.ksga.springboot.sahakka.payload.response.Response;
import org.ksga.springboot.sahakka.service.FileStorageService;
import org.ksga.springboot.sahakka.service.serviceImp.FileStorageServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/v1/file")
public class FileStorageRestController {
    private FileStorageService fileStorageService;

    @Value("${file.upload.full.url}")
    String fullUrl;
    @Autowired
    public  FileStorageRestController(FileStorageServiceImp fileStorageService){
        this.fileStorageService = fileStorageService;
    }
    @PostMapping("/uploadFile")
    public Response<String>uploadFile(@RequestParam(name = "file")MultipartFile file){

        try {
            String response = fullUrl + fileStorageService.saveFile(file);

            return Response.<String>ok().setPayload(response);
        }catch (Exception fileExp){

            System.out.println("File Exception ; " + fileExp.getMessage());
            return Response.<String>exception().setError("Failed to upload exception occurs");
        }
    }

}





        // ======================================


     /*   try{
            String response = "/files/"+ fileStorageService.saveFile(file);
            System.out.println("Value of response : "+ response);
            if (response!=null){

                return Response.<String>ok().setPayload(response).setError("Image Uploaded successfully.");

            }else return Response.<String>badRequest().setError("Failed to upload the image. Bad image format");
        }catch (Exception exception){
            System.out.println("Exception Occur. Failed to update image "+exception.getMessage());
            return Response.<String>exception().setError("Failed to upload the image.Exception occurs.");
        }*/



