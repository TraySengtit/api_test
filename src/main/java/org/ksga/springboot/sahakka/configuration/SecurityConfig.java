package org.ksga.springboot.sahakka.configuration;

import org.ksga.springboot.sahakka.security.UserDetailsServiceImp;
import org.ksga.springboot.sahakka.security.handler.CustomAuthenticationEntryPoint;
import org.ksga.springboot.sahakka.security.handler.CustomAuthenticationSuccessHandler;
import org.ksga.springboot.sahakka.security.handler.CustomLogoutSuccessHandler;
import org.ksga.springboot.sahakka.security.jwt.EntryPointJwt;
import org.ksga.springboot.sahakka.security.jwt.JwtTokenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends GlobalAuthenticationConfigurerAdapter{

    @Autowired
    public EntryPointJwt unauthorizedHandler ;
    @Autowired
    public UserDetailsServiceImp userDetailsService;

    @Order(1)
    @Configuration
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    public class ApiWebSecurityConfigurationAdapter extends   WebSecurityConfigurerAdapter {

        @Autowired
        PasswordEncoder passwordEncoder;
        @Bean
        public PasswordEncoder passwordEncoder(){ return new BCryptPasswordEncoder();
        }
        // Use this in order to verify the user in provided url
        @Bean
        public AuthenticationManager authenticationManager() throws Exception{
            return super.authenticationManager();
        }
        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth
                    .userDetailsService(userDetailsService)
                    .passwordEncoder(passwordEncoder);
        }
        @Override
        public void configure(WebSecurity web) throws Exception {
            super.configure(web);
            web.ignoring()
                    .antMatchers("/h2-console/**",
                            "/swagger-ui.html", "/resources/**", "/static/**", "/css/**", "/js/**", "/images/**",
                            "/resources/static/**", "/css/**", "/js/**", "/img/**", "/fonts/**",
                            "/images/**", "/scss/**", "/vendor/**", "/favicon.ico", "/auth/**", "/favicon.png",
                            "/v2/api-docs", "/configuration/ui", "/configuration/security",
                            "/webjars/**", "/swagger-resources/**", "/actuator", "/swagger-ui/**",
                            "/actuator/**", "/swagger-ui/index.html", "/swagger-ui/");
        }

        @Bean
        public JwtTokenFilter jwtTokenFilter(){
            return  new JwtTokenFilter();
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            // super.configure(http);
            http
                    .cors().configurationSource(corsConfigurationSource()).and()
                    . csrf().disable()
                    .exceptionHandling()
                    .authenticationEntryPoint(unauthorizedHandler)
                    .and()
                    .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                    .antMatcher("/api/**")
                    .authorizeRequests()
                    .antMatchers("/api/v1/auth/signup/freelancer","/api/v1/auth/signup/businessOwner","/api/v1/account/{id}/findBusinessOwner"
                            ,"/api/v1/auth/login","/api/v1/auth/loginWithProvider","/api/v1/servicePost/","/api/v1/servicePost","/api/v1/servicePost/findAllServicePost","/api/v1/category/getAllCategories",
                            "/api/v1/post/getAll","/api/v1/post/{postId}/findPostById","/api/v1/post/getAllPosts","/api/v1/servicePost/{id}/findServicePostById","/api/v1/account/{id}/findFreelancer"
                            ,"/api/v1/file/uploadFile"
                            ,"/files/***", "/api/v1/account/forgetPassword","/api/v1/account/resetForgetPassword"
                    )
                    .permitAll()
                    .anyRequest()
                    .authenticated() ;
            http.addFilterBefore(jwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
        }
        @Bean
        CorsConfigurationSource corsConfigurationSource() {
            CorsConfiguration configuration = new CorsConfiguration();
            configuration.setAllowedOrigins(Collections.singletonList("*"));
            configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "OPTIONS", "DELETE", "PATCH"));
            UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
            source.registerCorsConfiguration("/**", configuration.applyPermitDefaultValues());
            return source;
        }
    }

    @Order(2)
    @Configuration
    public class FormLoginWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter{

        @Autowired
        private PasswordEncoder passwordEncoder;
        @Autowired
        private CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;
        @Autowired
        private CustomAuthenticationEntryPoint entryPointJwt;


        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
          //  super.configure(auth);
            auth
                    .userDetailsService(userDetailsService)
                    .passwordEncoder(passwordEncoder);

        }
        @Override
        protected void configure(HttpSecurity http) throws Exception {
          //  super.configure(http);
            http
           .csrf()
                    .disable()
                    .authorizeRequests()
                    .antMatchers("/login","/files/***").permitAll()
                   // .antMatchers("/signup").permitAll()
                    .antMatchers("/dashboard/**").hasAuthority("ADMIN")
                    .anyRequest()
                    .fullyAuthenticated()
                    .and()
                    .formLogin()
                    .loginProcessingUrl("/login")
                    .failureUrl("/login?error=true")
                    .loginPage("/login")
                    .successHandler(customAuthenticationSuccessHandler)// add success handler
                    .usernameParameter("email")
                    .passwordParameter("password")
                    .permitAll()
                    .and()
                    .logout()
                    .logoutRequestMatcher(new AntPathRequestMatcher("/logout")) // Add success logout
                    .logoutSuccessHandler(new CustomLogoutSuccessHandler())
                    .deleteCookies("JSESSIONID")
                    .logoutSuccessUrl("/")
                    .and()
                    .exceptionHandling()
                    .authenticationEntryPoint(entryPointJwt);
        }
        @Override
        public void configure(WebSecurity web) throws Exception {
         //   super.configure(web);
            web.ignoring()
                    .antMatchers("/h2-console/**",
                            "/swagger-ui.html", "/resources/**", "/static/**", "/css/**", "/js/**", "/images/**",
                            "/resources/static/**", "/css/**", "/js/**", "/img/**", "/fonts/**",
                            "/images/**", "/scss/**", "/vendor/**", "/favicon.ico", "/auth/**", "/favicon.png",
                            "/v2/api-docs", "/configuration/ui", "/configuration/security",
                            "/webjars/**", "/swagger-resources/**", "/actuator", "/swagger-ui/**",
                            "/actuator/**", "/swagger-ui/index.html", "/swagger-ui/");

        }

    }




}



