package org.ksga.springboot.sahakka.model.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.ksga.springboot.sahakka.model.postService.UserServicePost;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class FindFreelancer  extends UserServicePost {

String skills;
String languages;
String achievement;
String educational_background;
String work_experience ;

}
