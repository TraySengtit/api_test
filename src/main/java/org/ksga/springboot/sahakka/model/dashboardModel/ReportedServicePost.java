package org.ksga.springboot.sahakka.model.dashboardModel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.ksga.springboot.sahakka.model.user.FindFreelancer;
import org.ksga.springboot.sahakka.model.user.User;



@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class ReportedServicePost {

    int id;
    int user_id;
    String post_image;
    String title;
    String post_description;

    FindFreelancer user;
}
