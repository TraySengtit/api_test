package org.ksga.springboot.sahakka.model.post;

import lombok.*;
import lombok.experimental.Accessors;
import org.ksga.springboot.sahakka.dto.postDto.UserPost;
import org.ksga.springboot.sahakka.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@ToString
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class Post {
    private int post_id;
    private int user_id;
    private String title;
    private String description;
    private String post_image;
    private String likes;
    private String post_deadline;
    private String created_date;
    private String post_status;

}
