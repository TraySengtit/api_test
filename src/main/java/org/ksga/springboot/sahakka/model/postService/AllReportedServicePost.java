package org.ksga.springboot.sahakka.model.postService;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.ksga.springboot.sahakka.model.user.FindFreelancer;
import org.ksga.springboot.sahakka.model.user.User;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class AllReportedServicePost{

    private int id;
    private int service_post_id;
    private int user_id;
    private String reason;
    private String created_date;
  //  FindFreelancer user;
    User user;

}
