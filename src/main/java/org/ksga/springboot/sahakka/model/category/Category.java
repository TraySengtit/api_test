package org.ksga.springboot.sahakka.model.category;

import lombok.*;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class Category {
    private int category_id;
    private String category_type;

}
