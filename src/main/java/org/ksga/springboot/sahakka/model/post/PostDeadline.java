package org.ksga.springboot.sahakka.model.post;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class PostDeadline {

int post_id;
String post_deadline ;
}
