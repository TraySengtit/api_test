package org.ksga.springboot.sahakka.model.post;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ProjectPost {


    private int post_id;
    private String title;
    private String description;
    private String post_image;
    private String likes;
    private String post_deadline;
    private String created_date;
    private String post_status;
    List<String> categories;

    //
    private int user_id;
    private String username;
    private String profile_image;
}
