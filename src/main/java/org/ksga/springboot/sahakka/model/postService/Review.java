package org.ksga.springboot.sahakka.model.postService;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.ksga.springboot.sahakka.dto.postDto.UserPost;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class Review {

    private int id;
    private int service_post_id;
   // private int user_id;
    private String review_text;
    private String review_date;
    private int number_of_stars;
    private int parent_review_id;

    int user_id;
    String username;
    String profile_image;




}
