package org.ksga.springboot.sahakka.model.postService;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class UserServicePost {

    int user_id;
    String username;
    String bio;
    String profile_image;
    String telephone;
    String official_email;
    String location;
    String nationality ;
    // What I just update
    String gender;
    String skills;
    List<String> social_media;
}
