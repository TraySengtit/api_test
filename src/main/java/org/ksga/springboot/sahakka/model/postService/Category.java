package org.ksga.springboot.sahakka.model.postService;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class Category {

    int category_id ; // our id is also auto increment
    String category_type;
    boolean suggested_state;


}
