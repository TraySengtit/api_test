package org.ksga.springboot.sahakka.model.dashboardModel;

import lombok.*;
import org.ksga.springboot.sahakka.model.user.User;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class FreelancerPost {
    private int id;
    private String title;
    private String post_image;
    private String post_description;

   // private
    User user;
    String created_date;
    String email;
    String telephone;
    private String username;
 //   private String user_id;
    private int user_id;
    private String profile_image;
    private String educational_background;
    private String work_experience;
    private String skills;
    private String languages;
    private String achievement;
    private String nationaliry;
    private String address;
}
