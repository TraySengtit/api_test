package org.ksga.springboot.sahakka.model.dashboardModel;

import lombok.*;
import org.ksga.springboot.sahakka.model.user.User;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class OwnerPost {
    private int post_id;
    private String title;
    private String description;
    private String post_image;
    private String likes;
    private String created_date;

    private String username;
    private int user_id;
    private String telephone;
    private String address;
    private String nationality;
    private String email;
    private String profile_image;
    private String company_name;

    @Override
    public String toString() {
        return "OwnerPost{" +
                "post_id=" + post_id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", post_image='" + post_image + '\'' +
                ", likes='" + likes + '\'' +
                ", created_date='" + created_date + '\'' +
                ", username='" + username + '\'' +
                ", user_id=" + user_id +
                ", telephone='" + telephone + '\'' +
                ", address='" + address + '\'' +
                ", nationality='" + nationality + '\'' +
                ", email='" + email + '\'' +
                ", profile_image='" + profile_image + '\'' +
                ", company_name='" + company_name + '\'' +
                ", company_contact='" + company_contact + '\'' +
                ", company_location='" + company_location + '\'' +
                '}';
    }

    private String company_contact;
    private String company_location;
}
