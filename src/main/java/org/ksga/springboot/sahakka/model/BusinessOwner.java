package org.ksga.springboot.sahakka.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.ksga.springboot.sahakka.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class BusinessOwner {
    private int bo_id;
    private String company_name;
    private String company_contact;
    private String company_location;
    private String business_description;
    private String company_logo;
}
