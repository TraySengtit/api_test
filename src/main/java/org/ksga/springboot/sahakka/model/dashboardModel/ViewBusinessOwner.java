package org.ksga.springboot.sahakka.model.dashboardModel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.ksga.springboot.sahakka.model.BusinessOwner;
import org.ksga.springboot.sahakka.model.user.User;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor

public class ViewBusinessOwner  {

    private User user ;
    private BusinessOwner businessOwner;

    public void setObject(User user,BusinessOwner businessOwner){

        this.user = user ;
        this.businessOwner=businessOwner;
    }

}
