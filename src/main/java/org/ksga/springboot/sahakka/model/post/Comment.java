package org.ksga.springboot.sahakka.model.post;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.ksga.springboot.sahakka.model.postService.ServiceCardUser;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Comment {
//private int user_id;
private int comment_id;
private String comment_text;
private String created_date;
private ServiceCardUser user;

}
