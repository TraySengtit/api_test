package org.ksga.springboot.sahakka.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.ksga.springboot.sahakka.model.postService.UserServicePost;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class FindBusinessOwner extends UserServicePost {

    String company_name;
    String company_contact;
    String company_location;
    String company_logo;
    String business_description;

}
