package org.ksga.springboot.sahakka.model.postService;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ServiceCard {

private int id;
private int user_id;
private String title;
private String post_image;
private String post_status;
private String post_description;

}
