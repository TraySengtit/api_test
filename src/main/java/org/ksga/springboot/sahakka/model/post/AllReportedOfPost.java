package org.ksga.springboot.sahakka.model.post;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.ksga.springboot.sahakka.model.user.User;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AllReportedOfPost {
    private int id;
    private int  post_id;
    private int user_id;
    private String reason;
    private String created_date;
    //  FindFreelancer user;
    User user;
}
