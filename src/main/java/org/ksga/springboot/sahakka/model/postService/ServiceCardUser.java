package org.ksga.springboot.sahakka.model.postService;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class ServiceCardUser {
    int user_id;
    String username;
    String profile_image;
    String skills;

}
