package org.ksga.springboot.sahakka.model.post;

import lombok.*;
import org.ksga.springboot.sahakka.model.user.User;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AllReportedProjectPost {
    private int post_id;
    private int user_id;
    private String title;
    private String description;
    private String post_image;
    private String likes;
    private String post_deadline;
    private String created_date;
    private String post_status;

    User user;


}
