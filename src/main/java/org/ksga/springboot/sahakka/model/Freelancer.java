package org.ksga.springboot.sahakka.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.ksga.springboot.sahakka.model.user.User;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class Freelancer {
    private int freelancer_id;
    private String educational_background;
    private String work_experience;
    private String skills;
    private String languages;
    private String achievement;

    private User user;
}
