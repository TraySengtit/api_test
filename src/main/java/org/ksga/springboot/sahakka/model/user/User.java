package org.ksga.springboot.sahakka.model.user;

import lombok.*;
import org.ksga.springboot.sahakka.model.BusinessOwner;
import org.ksga.springboot.sahakka.model.Freelancer;
import org.springframework.beans.factory.annotation.Autowired;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class User {

    private int user_id;
    private String username;
    private String email;
    private String passwords;
    private  String provider_type;
    private boolean is_active;
    private String profile_image;
    private String email_address;
    private String  telephone;
    private String created_date;
    private String nationality ;
    private String bio;
    private String gender;
    private String address;


}
