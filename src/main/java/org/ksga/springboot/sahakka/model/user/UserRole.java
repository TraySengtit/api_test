package org.ksga.springboot.sahakka.model.user;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class UserRole {
    private int id;
    private int user_id;
    private int role_id;
}
