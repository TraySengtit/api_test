package org.ksga.springboot.sahakka.model.user;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@Accessors(chain = true)



public class AuthUser {
    private Long id;
    private String email;
    private String password;
    private Set<AuthRole> roles = new HashSet<>();
    public AuthUser(String email , String password){

        this.email = email;
        this.password = password;
    }
}
