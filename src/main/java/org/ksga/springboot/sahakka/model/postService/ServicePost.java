package org.ksga.springboot.sahakka.model.postService;

import lombok.*;
import lombok.experimental.Accessors;
import org.ksga.springboot.sahakka.model.user.User;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
@Accessors(chain = true)

public class ServicePost {
    private int id;
    private String title;
    private String post_image;
    private String post_status;
    private String post_description;
    private User user;

}
