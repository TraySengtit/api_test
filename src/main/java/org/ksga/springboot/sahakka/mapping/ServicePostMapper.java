package org.ksga.springboot.sahakka.mapping;

import org.ksga.springboot.sahakka.dto.servicepostdto.ServicePostDto;
import org.ksga.springboot.sahakka.model.postService.ServicePost;
import org.ksga.springboot.sahakka.payload.request.ServicePostRequest.ServicePostRequest;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;


@Component
@Mapper(componentModel = "spring")
public interface ServicePostMapper {
    ServicePostDto mapToDTo(ServicePost servicePostService);

    ServicePost mapRequestToModel(ServicePostRequest request);
}
