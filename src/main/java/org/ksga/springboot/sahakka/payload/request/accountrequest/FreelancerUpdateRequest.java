package org.ksga.springboot.sahakka.payload.request.accountrequest;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class FreelancerUpdateRequest extends UserUpdateRequest {
    private String education_background;
    private String work_experience;
    private String skills ;
    private String languages;
    private String achievement;
    private String national_id;
}
