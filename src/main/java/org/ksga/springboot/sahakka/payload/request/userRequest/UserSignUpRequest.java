package org.ksga.springboot.sahakka.payload.request.userRequest;

import lombok.*;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
public class UserSignUpRequest {
   // private int user_id;
    private String username;
    private String email;
    private String passwords;
   //  private  String provider_type;
   // private boolean is_active;
    private String profile_image;
    private String email_address;
    private String  telephone;
    private List<String > social_media;
    //private String created_date;
    private String bio;
    private String gender;
    private String address;
    private String nationality ; 
}
