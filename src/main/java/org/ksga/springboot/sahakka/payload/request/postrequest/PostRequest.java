package org.ksga.springboot.sahakka.payload.request.postrequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class PostRequest {


    private String title ;
    private String description ;
    private String postImage;
    private String postStatus;
    private String createDate;
    private String deadline;



}
