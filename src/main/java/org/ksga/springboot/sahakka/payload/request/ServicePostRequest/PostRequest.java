package org.ksga.springboot.sahakka.payload.request.ServicePostRequest;
import lombok.*;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
public class PostRequest {

    private String title;
    private String description;
    private String postImage;
    private String postDeadline;
    private String postStatus;

    List<String > categories;

//    @JsonIgnore
//    public int getId() {
//        return id;
//    }

}
