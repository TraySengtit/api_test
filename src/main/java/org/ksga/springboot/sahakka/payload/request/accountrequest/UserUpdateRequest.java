package org.ksga.springboot.sahakka.payload.request.accountrequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class UserUpdateRequest {

   // private int user_id;
    private String username;
    //private String email;
   // private String passwords;
  //  private  String provider_type;
  //  private boolean is_active;
    private String profile_image;
    private String email_address;
    private String  telephone;

  //  private String created_date;
    private String bio;
    private String gender;
    private String address ;
    private String nationality ;

    List<String> social_media;
}
