package org.ksga.springboot.sahakka.payload.request.userRequest;


import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter

public class BusinessOwnerSignUpRequest extends  UserSignUpRequest{
    // private int bo_id;
    private String company_name;
    private String company_contact;
    private String company_location ;
    private String company_description ;
    private String company_logo;

}
