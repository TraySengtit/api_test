package org.ksga.springboot.sahakka.payload.request.accountrequest;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class ResetPasswordRequest {

    String oldPassword;
    String newPassword;
    String confirmPassword;
}
