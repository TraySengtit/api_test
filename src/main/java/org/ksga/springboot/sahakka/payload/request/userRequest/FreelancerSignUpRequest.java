package org.ksga.springboot.sahakka.payload.request.userRequest;


import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class FreelancerSignUpRequest extends UserSignUpRequest{

   // private int freelancer_id ;
    private String education_background;
    private String work_experience;
    private String skills ;
    private String languages;
    private String achievement;
    private String national_id;


}
