package org.ksga.springboot.sahakka.payload.request.userRequest;


import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class UserLoginRequest {

    String email;
    String password;



}
