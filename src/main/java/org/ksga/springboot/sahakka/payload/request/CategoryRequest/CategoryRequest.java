package org.ksga.springboot.sahakka.payload.request.CategoryRequest;


import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class CategoryRequest {
    private int category_id;
    private String category_type;
}
