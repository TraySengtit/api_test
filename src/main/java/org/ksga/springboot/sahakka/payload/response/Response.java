package org.ksga.springboot.sahakka.payload.response;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.lang.annotation.Repeatable;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Getter
@Setter
@Accessors ( chain = true)

@NoArgsConstructor
public class Response<T> {


   private Status status;
   private T payload ;
   private Object error;
   private boolean success = false;
   private Object metadata ;

   // For bad request

    public  static <T> Response<T> badRequest (){
        Response<T> response = new Response<>();
        response.setStatus(Status.BAD_REQUEST);

        return response;
    }

    // OK

    public  static  <T> Response<T> ok(){

        Response<T> response = new Response<>();
        response.setStatus(Status.OK);
        response.setSuccess(true);
        return response;

    }

    public  static  <T> Response<T> successDelete(){

        Response<T> response = new Response<>();
        response.setStatus(Status.SUCCESS_DELETE);
        response.setSuccess(true);
        return response;

    }

    // Duplicate Entry

    public static <T> Response<T> duplicateEntry (){
        Response<T> response = new Response<>();
        response.setStatus(Status.DUPLICATED_ENTRY);
        return response;


    }

    // not found

    public static <T> Response<T> notFound(){

        Response<T> response   = new Response<>();
        response.setStatus(Status.NOT_FOUND);
        return response ;
    }

    // SUCCESS

    public static <T> Response<T> successCreate(){

        Response<T> response = new Response<>();
        response.setStatus(Status.CREATE_SUCCESS);
        return response ;
    }

    // update

    public static <T> Response<T> updateSucess(){

        Response<T> response = new Response<>();
        response.setStatus(Status.UPDATE_SUCCESS);
        return response ;
    }

    // Exception Error

    public  static <T> Response<T> exception(){
        Response<T> response   = new Response<>();
        response.setStatus(Status.EXCEPTION);
        return response ;

    }


    // Get the current date using data utils

    public  void setErrorMsgToResponse ( String  errorMsg , Exception ex ){

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");


        ResponseError error = new ResponseError()
                .setDetails(errorMsg)
                .setMessage(ex.getMessage())
                .setTimestamp(new Date());

    }

    public enum Status{
        OK,BAD_REQUEST,UNAUTHORIZED,VALIDATION_EXCEPTION
        ,EXCEPTION,
        WRONG_CREDENTIAL,
        ACCESS_DENIED,
        NOT_FOUND,
        DUPLICATED_ENTRY,
        SUCCESS_DELETE,
        UPDATE_SUCCESS,
        CREATE_SUCCESS
    }


    // Class Meta : for the pagination purposes


    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class PageMetaData {
        private final int size;
        private final long totalElement;
        private final int totalPage;
        private final int number;

        public PageMetaData(int size, long totalElement , int totalPage, int number){

            this.size = size;
            this.totalElement = totalElement;
            this.totalPage=totalPage;
            this.number = number;
        }

    }

}
