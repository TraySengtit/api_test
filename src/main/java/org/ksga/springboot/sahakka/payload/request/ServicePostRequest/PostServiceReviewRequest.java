package org.ksga.springboot.sahakka.payload.request.ServicePostRequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class PostServiceReviewRequest {
    String reviews;
    int numberOfStar;
}
