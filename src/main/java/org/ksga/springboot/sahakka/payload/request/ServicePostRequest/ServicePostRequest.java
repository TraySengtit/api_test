package org.ksga.springboot.sahakka.payload.request.ServicePostRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.ksga.springboot.sahakka.model.user.User;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ServicePostRequest {
   /* private int id;
    private int user_id;*/

    private String postTitle;
    private String postImage;
    private String postStatus;
    private String postDescription;
    List<String > categories ;



}
