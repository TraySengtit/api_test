package org.ksga.springboot.sahakka.payload.request.accountrequest;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class BusinessOwnerUpdateRequest extends UserUpdateRequest{

    // private int bo_id;
    private String company_name;
    private String company_contact;
    private String company_location ;
    private String company_description ;
    private String company_logo;

}
