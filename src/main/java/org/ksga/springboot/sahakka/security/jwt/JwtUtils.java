package org.ksga.springboot.sahakka.security.jwt;

import io.jsonwebtoken.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.ksga.springboot.sahakka.security.UserDetailsImp;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

// Here is the class that will used to verify , generate token.
@Component
@Slf4j

public class JwtUtils {

    private String jwtSecret = "ThisIsTheKey";
    private long jwtExpiration = 86400000;

    public String generateJwtToken(Authentication authentication){

        UserDetailsImp userPrinciple  = (UserDetailsImp) authentication.getPrincipal();

        return Jwts
                .builder()
                .setSubject(userPrinciple.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpiration))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    // Get user name by the provided toden

    public String getUserNameFromJwtToken ( String token) {

        if(validateJwtToken(token)){

            return Jwts
                    .parser()
                    .setSigningKey(jwtSecret)
                    .parseClaimsJws(token)
                    .getBody()
                    .getSubject();
        }
        else    return null;


    }

    // method in order to validate the JwtToken

    public boolean validateJwtToken ( String authToken){

        try{
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return  true ;

        }catch (SignatureException e){ log.error("Invalid JWT signature : {}", e.getMessage());}
        catch (MalformedJwtException e ){ log.error("Invalid JWT token : {}",e.getMessage());}
        catch (ExpiredJwtException e){ log.error("JWT token is expired: {}",e.getMessage());}
        catch (UnsupportedJwtException e){ log.error("JWT token is unsupported : {}",e.getMessage());}
        catch (IllegalArgumentException e){log.error("JWT claims string is empty : {}",e.getMessage());}
        catch (Exception e){ log.error("Exception : {}"+ e.getMessage());}

        return  false;


    }

}
