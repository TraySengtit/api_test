package org.ksga.springboot.sahakka.security;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.ksga.springboot.sahakka.model.user.AuthUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserDetailsImp implements UserDetails {

    private Long id;
    String email;
    String password;
    private Collection<? extends GrantedAuthority> authorities;
    // User ( username , password and role )
    public  static UserDetailsImp build(AuthUser user){

        List<GrantedAuthority> authorities = user.getRoles()
                .stream()
                .map(e-> new SimpleGrantedAuthority(e.getRole().name()))
                .collect(Collectors.toList());

        System.out.println("----- Here are the value fo the authorities :---");
        authorities.stream().map(String::valueOf).forEach(System.out::println);

        return  new UserDetailsImp(
                user.getId(),
                user.getEmail(),
                user.getPassword(),
                authorities
        );
    }
    // Get Role of the user
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


}
