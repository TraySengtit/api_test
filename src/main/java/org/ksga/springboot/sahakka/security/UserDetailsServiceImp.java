package org.ksga.springboot.sahakka.security;

import org.ksga.springboot.sahakka.model.user.AuthRole;
import org.ksga.springboot.sahakka.model.user.AuthUser;
import org.ksga.springboot.sahakka.model.user.User;
import org.ksga.springboot.sahakka.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserDetailsServiceImp implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        // only enter username and the password: Next up is to try to using email and password
       AuthUser selectedUser = userRepository.findUserByUsername(email);

       if( selectedUser == null){
          // System.out.println("Shit there we go again .");
           return null; }
       else
       {
          // return UserDetailsImp.build(selectedUser);
           UserDetailsImp userDetails = new UserDetailsImp();


                   userDetails.setEmail(selectedUser.getEmail());
           userDetails.setPassword(selectedUser.getPassword());

           // Set authority. Not let's see if it still null
           List<GrantedAuthority> authorities = selectedUser.getRoles()
                   .stream()
                   .map(e-> new SimpleGrantedAuthority(e.getRole().name()))
                   .collect(Collectors.toList());

           userDetails.setAuthorities(authorities);


           return  userDetails;
         /*  // Error on the authorities
            UserDetailsImp userDetails = new UserDetailsImp();
            userDetails.setEmail(selectedUser.getUsername());
            userDetails.setPassword(selectedUser.getPassword());

            //userDetails.setAuthorities(selectedUser.getRoles());
            // these are the values that we get it from the database
            return userDetails;
*/
       }



    }
}
