create table shk_users
(
    user_id       integer not null
        constraint users_tb_pk
            primary key,
    username      varchar,
    email         varchar,
    passwords     text,
    provider_type varchar,
    is_active     boolean,
    profile_image varchar,
    email_address varchar,
    telephone     varchar,
    social_media  text,
    created_date  text,
    bio           text,
    gender        varchar
);

alter table shk_users
    owner to postgres;

create table shk_freelancers
(
    freelancer_id          integer
        constraint freelancer_tb_users_tb__fk
            references shk_users,
    educational_background text,
    work_experience        text,
    skills                 varchar,
    languages              varchar,
    achievement            varchar
);

alter table shk_freelancers
    owner to postgres;

create table shk_business_owners
(
    bo_id                integer
        constraint shk_business_owners_tb_users_tb__fk
            references shk_users,
    company_name         varchar,
    company_contact      varchar,
    company_location     varchar,
    business_description text,
    company_logo         varchar
);

alter table shk_business_owners
    owner to postgres;

create table shk_roles
(
    role_id integer not null
        constraint roles_tb_pk
            primary key,
    role    varchar
);

alter table shk_roles
    owner to postgres;

create table shk_user_role
(
    id      integer not null
        constraint user_role_tb_pk
            primary key,
    user_id integer
        constraint user_role_tb_users_tb__fk
            references shk_users,
    role_id integer
        constraint user_role_tb_roles_tb__fk
            references shk_roles
);

alter table shk_user_role
    owner to postgres;

create table shk_posts
(
    post_id       integer not null
        constraint posts_tb_pk
            primary key,
    title         text,
    description   text,
    user_id       integer
        constraint posts_tb_users_tb__fk
            references shk_users,
    post_image    text,
    likes         integer,
    post_deadline text,
    created_date  text,
    post_status   text
);

alter table shk_posts
    owner to postgres;

create table shk_user_like_post
(
    id      integer not null
        constraint user_like_post_pk
            primary key,
    post_id integer
        constraint user_like_post_posts_tb__fk
            references shk_posts,
    user_id integer
        constraint user_like_post_users_tb__fk
            references shk_users
);

alter table shk_user_like_post
    owner to postgres;

create table shk_comments
(
    comment_id        integer not null
        constraint comments_tb_pk
            primary key,
    user_id           integer
        constraint comments_tb_users_tb__fk
            references shk_users,
    post_id           integer
        constraint comments_tb_posts_tb__fk
            references shk_posts,
    parent_comment_id integer
        constraint comments_tb_comments_tb__fk
            references shk_comments,
    comment_text      text
);

alter table shk_comments
    owner to postgres;

create table shk_categories
(
    category_id     serial not null
        constraint categories_tb_pk
            primary key,
    category_type   varchar,
    suggested_state boolean
);

alter table shk_categories
    owner to postgres;

create table shk_post_category
(
    id          serial not null
        constraint shk_post_category_tb_pk
            primary key,
    post_id     integer
        constraint shk_post_category_tb_posts_tb__fk
            references shk_posts,
    category_id integer
        constraint shk_post_category_tb_categories_tb__fk
            references shk_categories
);

alter table shk_post_category
    owner to postgres;

create table shk_service_posts
(
    id               integer not null
        constraint shk_service_posts_tb_pk
            primary key,
    user_id          integer
        constraint shk_service_posts_tb_users_tb__fk
            references shk_users,
    title            text,
    post_image       text,
    post_status      varchar,
    post_description text
);

alter table shk_service_posts
    owner to postgres;

create table shk_service_post_category
(
    id              serial not null
        constraint shk_service_post_category_pk
            primary key,
    service_post_id integer
        constraint shk_service_post_category_shk_service_posts_tb__fk
            references shk_service_posts,
    category_id     integer
        constraint shk_service_post_category_categories_tb__fk
            references shk_categories
);

alter table shk_service_post_category
    owner to postgres;

create table shk_reviews
(
    id               serial not null
        constraint reviews_tb_pk
            primary key,
    service_post_id  integer
        constraint reviews_tb_shk_service_posts_tb__fk
            references shk_service_posts,
    user_id          integer
        constraint reviews_tb_users_tb__fk
            references shk_users,
    review_text      text,
    number_of_stars  integer,
    parent_review_id integer
        constraint reviews_tb_reviews_tb__fk
            references shk_reviews,
    review_date      text
);

alter table shk_reviews
    owner to postgres;

create table shk_message
(
    id           integer not null
        constraint shk_message_tb_pk
            primary key,
    msg          text,
    create_date  date,
    recipient_id integer
        constraint shk_message_tb_users_tb__fk
            references shk_users,
    is_seen      boolean,
    sender_id    integer
        constraint shk_message_tb_users_tb__fk_2
            references shk_users
);

alter table shk_message
    owner to postgres;

